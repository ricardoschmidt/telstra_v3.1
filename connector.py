#!/usr/bin/env python

import socket
import time
import sys

_tcp_ip = "127.0.0.1"
_tcp_port = 5000
_buffer_size = 1024
_message = "StatReq"


_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
_socket.connect((_tcp_ip, _tcp_port))

_msg_counter = 0

# Send a Login Message
print "Sending Login:"
_msg_counter += 1
_message = "RTA|LOGIN|" + str(_msg_counter) + "|" + str(time.strftime('%s') + "|1,IEX,")
print _message
_socket.send(_message)
_data = _socket.recv(_buffer_size)
print "received data:"
print str(_data)



try:
  while 1:
    # Send a Agent Status Request message
    print "Sending Agent Status Request:"
    _msg_counter += 1
    _message = "RTA|StatReq|" + str(_msg_counter) + "|" + str(time.strftime('%s') + "|1,All")
    _socket.send(_message)
    _data = ""
    while 1:
      _part = _socket.recv(_buffer_size)
      _data += _part
      if len(_part) < _buffer_size:
        break
    print "received data:"
    print str(_data)

    time.sleep(10)

except KeyboardInterrupt:
  pass



## Send a Agent Status Request message
#print "Sending Agent Status Request:"
#_msg_counter += 1
#_message = "RTA|StatReq|" + str(_msg_counter) + "|" + str(time.strftime('%s') + "|1,All")
#_socket.send(_message)
#_data = ""
#while 1:
#  _part = _socket.recv(_buffer_size)
#  _data += _part
#  if len(_part) < _buffer_size:
#    break
#print "received data:"
#print str(_data)
#
#
## Give it some time and then proceed
#time.sleep(5)


# Send a Session Termination Message and close the socket
print "Sending Termination:"
_msg_counter += 1
_message = "RTA|Term|" + str(_msg_counter) + "|" + time.strftime('%s') + "|0,"
_socket.send(_message)


# Close connection
_socket.close()


