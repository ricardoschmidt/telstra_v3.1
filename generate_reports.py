#!/usr/bin/env python
#
# Author:
#   Ricardo de O. Schmidt
#     NAVOMI.com Cloud Solutions
#
# Date:
#   February 14, 2018
#
# Contact:
#   ricardo.schmidt@navomi.com
#
# Description:
#   This is a Python script that collects data from Live Engage through Live
#   Person APIs and generates reports based on specifications of the NICE WFM
#   Data Connector (document version 1.10). Data is collected from Live Person
#   using Operational Realtime API and Engagement History API.
#
# Input arguments (all mandatory):
#   -a : LP API account number
#   -k : LP API key
#   -s : LP API secret
#   -t : LP API token
#   -T : LP API token secret
#   -m : LP API signature method
#   -o : output path (path/to/folder/) to store XML reports
#   -l : output path (path/to/folder/) to store log and error files
#   -al : output path (path/to/folder/) to store application log files
#   -f : LP API request timeframe
#   -b1ts : OR API timestamp for first 5-min bucket
#   -b2ts : OR API timestamp for second 5-min bucket
#   -b3ts : OR API timestamp for third 5-min bucket
#   -b1hr : OR API human-readable time for first 5-min bucket
#   -b2hr : OR API human-readable time for second 5-min bucket
#   -b3hr : OR API human-readable time for third 5-min bucket
#   -i : LP API request interval
#   -v : LP API request version
#   -A : LP API request agents IDs (comma separated if more than one)
#   -S : LP API request skill IDs (comma separated if more than one)
#   -e : input file with agent-to-employee IDs mapping
#   -d : start timestamp used to control which time intervals to process
#   -F : end timestamp used to control which time intervals to process
#   -V : debug level (0:off, 1:basic, 2:verbose)
#   -n : report name for storing JSON files in case _debug_level==2
#   -j : JSON location (path/to/folder/) to store files
#   -es : Email server, used to send out email notifications
#   -ep : Email port, used to send out email notifications
#   -ew : Email password, used to access email account in the given server
#   -ef : Email source address/account name (only one address possible)
#   -et : Email destination address (only one address possible)
#   -ej : Pre-defined subject text for email (only a tag)
#	-ehFrom : EH API "from" time for request interval
#	-ehTo   : EH API "to" time for request interval
#	-cf  : concurrency factor file
#	-dcf : default concurrency factor value
#	-cts : call timestamp (in seconds)
#	-postday : True/False for generation of 24-hour postday reports
#
# Running:
#   This script is meant to be called from another automatized (Bash/Python)
#   script since all arguments listed above are mandatory and some have to be
#   calculated on the run/call time. Calling this script manually is not advised
#   since errors/inconsistencies might occur due to non-expected values passed
#   as input line argument.
#
#   The Bash script loop.sh available in the same software package is used to
#   call this script. For more information, please refer to loop.sh.
#

import imports_lib
import requests
from requests_oauthlib import OAuth1
import json
import cgi
import sys
import time
import datetime
import os
import xml.etree.cElementTree as et
import collections
import smtplib

# Python library containing general variable definitions
import general_definitions as GenDefinitions

# Python library with general functions
import general_functions as GenFunctions

# Python library with functions to collect data from LP APIs
import le_api_lib as LE

# Python library with functions related to email messages
import email_lib as Email


###
### Macros
###

# IMPORTANT: watch for consistency with file extension defined in ftp_connect.py
_M_REPORT_FILE_EXT = '.xml'

# Service name for LP APIs
_M_SERV_NAME_USERS_API      = 'accountConfigReadOnly'   # Service name for LP Users API
_M_SERV_NAME_SKILLS_API     = 'accountConfigReadOnly'   # Service name for LP Skills API
_M_SERV_NAME_REAL_TIME_API  = 'leDataReporting'         # Service name for LP Realtime Operational API
_M_SERV_NAME_ENG_HIST_API   = 'engHistDomain'           # Service name for LP Engagement History API


# File where to log the application messages
_app_log_file = ''


# Call parseArgs defined in general_functions.py and store arguments in the dictionary _myargs
_myargs = {}
_myargs = GenFunctions.parseArgs(sys.argv)

# Save the debug level
_debug_level = int(_myargs['-V'])

# Check for type of reports: intraday or postday
if _myargs['-postday'] == 'True':
	_postday_operation = True
	# Print debug message
	if _debug_level == GenDefinitions._DEBUG_VERBOSE:
		print "< POSTDAY reports >"
elif _myargs['-postday'] == 'False':
	_postday_operation = False
	# Print debug message
	if _debug_level == GenDefinitions._DEBUG_VERBOSE:
		print "< INTRADAY report >"


# Define application log file name
_app_log_file = str(_myargs['-al']) + str(_myargs['-n'][0:6]) + ".log"

# Write to app log: report generation started
_log_text = "==================================================\n"\
			+ "> Starting generation of reports for " \
			+ str(_myargs['-n'][1:]) \
			+ "\n> Time: " \
			+ str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")) \
			+ "\n"
GenFunctions.writeToAppLog(_app_log_file, _log_text)

# Save the email information
_email_server   = _myargs['-es']
_email_port     = _myargs['-ep']
_email_pw       = _myargs['-ew']
_email_from     = _myargs['-ef']
_email_to       = _myargs['-et']
_email_tag		= _myargs['-ej']

# Build basic email information template
_email_info_template = { 'server': _myargs['-es'],
						 'port': _myargs['-ep'],
						 'pw': _myargs['-ew'],
						 'from': _myargs['-ef'],
						 'to': _myargs['-et'] }

# Save LP account information
_lp_account_number  = _myargs['-a']
_lp_key             = _myargs['-k']
_lp_secret          = _myargs['-s']
_lp_token           = _myargs['-t']
_lp_token_secret    = _myargs['-T']


# Create the base name for output report files to be generated
_report_file_date = datetime.datetime.fromtimestamp(int(_myargs['-d'])-300).strftime("%m%d%y.%H%M")

# Name for Agent System Data report file
_report_AS_file_name = _myargs['-o'] + \
					"AS_" + \
					_myargs['-n'] + \
					_M_REPORT_FILE_EXT

# Name for Agent Queue Data report file
_report_AQ_file_name = _myargs['-o'] + \
					"AQ_" + \
					_myargs['-n'] + \
					_M_REPORT_FILE_EXT

# Name for Queue Data report file
_report_Q_file_name = _myargs['-o'] + \
					"Q_" + \
					_myargs['-n'] + \
					_M_REPORT_FILE_EXT


# Create the base name for the log files
_base_log_file_name = _myargs['-l'] + time.strftime('%Y%m%d%H%M')


# Build the authorization set of keys to be used from here on
_auth = OAuth1(str(_myargs['-k']), str(_myargs['-s']), str(_myargs['-t']), str(_myargs['-T']))



########## Retrieve Base URIs for all LP APIs that will be used ##########

# This block only runs for 15-min-based intraday reports; NOT for 24-hour-based postday reports
if not _postday_operation:

	# Retrieve the base URI for the Operational Realtime API
	if _debug_level == GenDefinitions._DEBUG_VERBOSE:
		print '=== Retrieving base URI for Operational Realtime API ==='
	_base_uri_OR_API, _notification_msg = LE.LEGetBaseURI(_M_SERV_NAME_REAL_TIME_API, _lp_account_number)

	# Check if there is a notification message to send via email, and if so quit the program due to error/failure
	if _notification_msg != '':
		# Attach the returned notification and a subject line to the email information
		_email_info = _email_info_template
		_email_info['message'] = _notification_msg
		_email_info['subject'] = _email_tag + ' System error notification'
		# Send email
		Email.sendNotification(_email_info)
		# Exit program because Base URI is critical information
		print "Exiting..."
		sys.exit()
# It's a postday report generation, just assumes a default URI value for the OR API
else:
	_base_uri_OR_API = '-1'


# Retrieve the base URI for the Engagement History API
if _debug_level == GenDefinitions._DEBUG_VERBOSE:
	print '=== Retrieving base URI for Engagement History API ==='
_base_uri_EH_API, _notification_msg = LE.LEGetBaseURI(_M_SERV_NAME_ENG_HIST_API, _lp_account_number)

# Check if there is a notification message to send via email, and if so quit the program due to error/failure
if _notification_msg != '':
	# Attach the returned notification and a subject line to the email information
	_email_info = _email_info_template
	_email_info['message'] = _notification_msg
	_email_info['subject'] = _email_tag + ' System error notification'
	# Send email
	Email.sendNotification(_email_info)
	# Exit program because Base URI is critical information
	print 'Exiting...'
	sys.exit()


# Retrieve the base URI for the Users API
if _debug_level == GenDefinitions._DEBUG_VERBOSE:
	print '=== Retrieving base URI for Users API ==='
_base_uri_USERS_API, _notification_msg = LE.LEGetBaseURI(_M_SERV_NAME_USERS_API, _lp_account_number)

# Check if there is a notification message to send via email, and if so quit the program due to error/failure
if _notification_msg != '':
	# Attach the returned notification and a subject line to the email information
	_email_info = _email_info_template
	_email_info['message'] = _notification_msg
	_email_info['subject'] = _email_tag + ' System error notification'
	# Send email
	Email.sendNotification(_email_info)
	# Exit program because Base URI is critical information
	print "Exiting..."
	sys.exit()


##########


########## Load agent ID to employee ID mapping ##########


if _debug_level == GenDefinitions._DEBUG_VERBOSE:
	print "=== Loading known agent-to-employee IP mapping ==="

# Start the dictionary containing the already known mapping
_agent_employee_id = {}

# Open the input file containing the mapping; this file is defined in the configuration file
with open(_myargs['-e'], 'r') as _agent_employee_map_file:
	# Reads every line and stores information in the dictionary _agent_employee_id
	for _line in _agent_employee_map_file:
		if len(_line) > 0:
			_agent_employee_id[str(_line.split(',')[0])] = str(_line.split(',')[1]).rstrip()
_agent_employee_map_file.close()

# Save the size of the file (number of lines) to later check if there are new lines to save
_agent_employee_id_file_size = len(_agent_employee_id)

# Print message with the number of lines loaded, if any
if _debug_level == GenDefinitions._DEBUG_VERBOSE:
	if len(_agent_employee_id) > 0:
		print '   ' + str(len(_agent_employee_id)) + ' agent-to-employee map loaded'


##########


########## Load concurrency factor values from file ##########


if _debug_level == GenDefinitions._DEBUG_VERBOSE:
	print "=== Loading concurrency factor values ==="

# Start the dictionary to store concurrency factor values per skill ID
_concurrency_factor = {}

# Open the input file containing the values; this file is defined in the configuration file
with open(_myargs['-cf'], 'r') as _concurrency_factor_file:
	# Reads every line and stores information in the dictionary _concurrency_factor
	for _line in _concurrency_factor_file:
		if len(_line) > 0:
			_concurrency_factor[str(_line.split(',')[0])] = str(_line.split(',')[1]).rstrip()
_concurrency_factor_file.close()

# Print message with the number of lines loaded, if any
if _debug_level == GenDefinitions._DEBUG_VERBOSE:
	if len(_concurrency_factor) > 0:
		print '   ' + str(len(_concurrency_factor)) + ' concurrency factor values loaded'


##########


########## Retrieve data from XML intraday Agent Queue (AQ_*) and Queue (Q_*) Data reports ##########

# This block only runs for 24-hour postday reports; NOT for 15-min-based intraday reports
if _postday_operation:

	##
	## Agent Queue Data report XML data
	##

	# Recover the XML file name
	_xml_aq_report = _myargs['-inxml'] + 'AQ_' + _myargs['-n'] + _M_REPORT_FILE_EXT

	# Initiate arrays to receive the data that has to be maintained from the intraday Agent Queue Data report
	_postday_aq_keys = {}
	_postday_aq_handled = {}
	_postday_aq_handle_time = {}

	# Report fields that will be set to zero
	#	For intraday reports, these fields are set in the block that computes the report values
	_report_hold_time = 0
	_report_work_time = 0
	_report_right_party_contacts = 0
	_report_right_party_talk_time = 0
	_report_wrong_party_contacts = 0
	_report_wrong_party_talk_time = 0

	# Walk through the XML file structure and save the information of interest in the arrays
	_xml_aq = et.parse(_xml_aq_report)
	_root = _xml_aq.getroot()
	for _node in _root.iter('AgentQueueData'):
		_xml_aq_data_skill_id = _node.find('QueueValue').text
		_xml_aq_data_agent_id = _node.find('AgentValue').text
		_key = str(_xml_aq_data_skill_id) + ',' + str(_xml_aq_data_agent_id)
		# Just keep a list of all key combinations
		_postday_aq_keys[str(_key)] = 0
		# Initialize array index of those fields to be recalculated for the postday report
		_postday_aq_handled[str(_key)] = 0
		_postday_aq_handle_time[str(_key)] = 0


	##
	## Queue Data report XML data
	##

	# Recover the XML file name
	_xml_q_report = _myargs['-inxml'] + 'Q_' + _myargs['-n'] + _M_REPORT_FILE_EXT

	# Initiate arrays to receive the data that has to be maintained from the intraday Queue Data report
	_postday_q_contacts_received = {}
	_postday_q_abandoned_long = {}
	_postday_q_handled_short = {}
	_postday_q_handled_long = {}
	_postday_q_handle_time = {}
	_postday_q_queue_delay_time = {}

	# Report fields that will be set to zero
	#	For intraday reports, these fields are set in the block that computes the report values
	_report_abandoned_short = 0
	_report_hold_time = 0
	_report_work_time = 0
	_report_svc_lvl_pct = 0
	_report_backlog = 0
	_report_backlog_not_expired = 0
	_report_backlog_expired = 0
	_report_right_party_contacts = 0
	_report_right_party_talk_time = 0
	_report_wrong_party_contacts = 0
	_report_wrong_party_talk_time = 0

	# Walk through the XML file structure and save the information of interest in the arrays
	_xml_q = et.parse(_xml_q_report)
	_root = _xml_q.getroot()
	for _node in _root.iter('QueueData'):
		_xml_q_data_skill_id = _node.find('QueueValue').text
		# Store those values generated using OR API for the intraday report, so that they are reported again
		_postday_q_contacts_received[str(_xml_q_data_skill_id)] = _node.find('ContactsReceived').find('count').text
		_postday_q_abandoned_long[str(_xml_q_data_skill_id)] = _node.find('AbandonedLong').find('count').text
		# Initialize array index of those fields to be recalculated for the postday report
		#_postday_q_contacts_received[str(_xml_q_data_skill_id)] = 0
		#_postday_q_abandoned_long[str(_xml_q_data_skill_id)] = 0
		_postday_q_handled_short[str(_xml_q_data_skill_id)] = 0
		_postday_q_handled_long[str(_xml_q_data_skill_id)] = 0
		_postday_q_handle_time[str(_xml_q_data_skill_id)] = 0
		_postday_q_queue_delay_time[str(_xml_q_data_skill_id)] = 0


##########


########## Collect data from LP APIs ##########


# Initial timeframe value as defined in the arguments
_start_bucket_min = int(str(datetime.datetime.fromtimestamp(int(_myargs['-d'])).strftime('%M')))
_timeframe_min = _myargs['-f']


##
## Send request to Operational Realtime API for the Agent Activity method
##

# This block only runs for 15-min-based intraday reports; NOT for 24-hour-based postday reports
if not _postday_operation:

	# Print debug message
	if _debug_level == GenDefinitions._DEBUG_VERBOSE:
		print '=== Agent Activity API request ==='

	# The request is going to be repeated until the obtained time interval minute is the one we want
	while True:

		# Define the params
		_params = { 'timeframe': _timeframe_min,
					'agentIds': str(_myargs['-A']),
					'interval': str(_myargs['-i']),
					'v': str(_myargs['-v']) }

		# Send the request to the API
		_response_AA, _notification_msg = LE.LEGetAgentActivity(_base_uri_OR_API, _lp_account_number, _auth, _params)

		# Check if there is a problem on the report generation, and if so quits the program
		if _notification_msg != '':
			# Attach the returned notification and a subject line to the email information
			_email_info = _email_info_template
			_email_info['message'] = _notification_msg
			_email_info['subject'] = _email_tag + ' System error notification'
			# Send email
			Email.sendNotification(_email_info)
			# Exit program because report data is critical information
			print 'Exiting...'
			sys.exit()

		# The request is successful, now checks if the interval is the correct one
		#	To do so, compares the timestamp (without milliseconds) from the first
		#	interval to that of the first bucket we are looking for
		for _interval in _response_AA['metricsByIntervals']:
			_interval_ts = int(_interval['timestamp']/1000)
			break

		# Interval is shorter than the one we want, redo the request with timeframe +5 minutes
		if _interval_ts > int(_myargs['-b1ts']):
			# Print debug message
			if _debug_level == GenDefinitions._DEBUG_VERBOSE:
				print '   Warning! We need to start with %s but the first interval returned is %s\n      We will increase the timeframe from %s to %s and try again' % (_myargs['-b1ts'], str(_interval_ts), str(_timeframe_min), str(int(_timeframe_min)+5))
			_timeframe_min = str(int(_timeframe_min) + 5)
		# Interval is beyond than the one we want, redo the request with timeframe -5 minutes
		elif _interval_ts < int(_myargs['-b1ts']):
			# Print debug message
			if _debug_level == GenDefinitions._DEBUG_VERBOSE:
				print '   Warning! We need to start with %s but the first interval returned is %s\n      We will increase the timeframe from %s to %s and try again' % (_myargs['-b1ts'], str(_interval_ts), str(_timeframe_min), str(int(_timeframe_min)-5))
			_timeframe_min = str(int(_timeframe_min) - 5)
		# Interval is the one we want, quit the while loop and proceed
		else:
			# Print debug message
			if _debug_level == GenDefinitions._DEBUG_VERBOSE:
				print '   OK, we are starting with %s (%s) what was the exactly minute requested' % (str(_interval_ts), _myargs['-b1hr'])
			_skip_interval = 0
			break


##
## Send request to Operational Realtime API for the Engagement Activity method
##

# This block only runs for 15-min-based intraday reports; NOT for 24-hour-based postday reports
if not _postday_operation:

	# Print debug message
	if _debug_level == GenDefinitions._DEBUG_VERBOSE:
		print '=== Engagement Activity API request ==='

	# The request is going to be repeated until the obtained time interval minute is the one we want
	#	It is very likely that the request will go through in the first attempt,
	#	because _timeframe_min has been calculated in the first request for the
	#	Agent Activity above.
	while True:

		# Define the params
		_params = { 'timeframe': _timeframe_min,
					'agentIds': str(_myargs['-A']),
					'skillIds': str(_myargs['-S']),
					'interval': str(_myargs['-i']),
					'v': str(_myargs['-v']) }

		# Send the request to the API
		_response_EA_reverse, _notification_msg = LE.LEGetEngActivity(_base_uri_OR_API, _lp_account_number, _auth, _params)

		# Check if there is a problem on the report generation, and if so quits the program
		if _notification_msg != '':
			# Attach the returned notification and a subject line to the email information
			_email_info = _email_info_template
			_email_info['message'] = _notification_msg
			_email_info['subject'] = _email_tag + ' System error notification'
			# Send email
			Email.sendNotification(_email_info)
			# Exit program because report data is critical information
			print 'Exiting...'
			sys.exit()

		# Reverse the order of the response, because intervals are returned in reversed order for Engagement Activity method
		_response_EA_temp = {}
		for _interval in _response_EA_reverse['metricsByIntervals']:
			_response_EA_temp[_interval['timestamp']] = _interval
		_response_EA = collections.OrderedDict(sorted(_response_EA_temp.items()))

		# The request is successful, now checks if the interval is the correct one
		#	To do so, compares the timestamp (withouth milliseconds) from the first
		#	interval to that of the first bucket we are looking for
		for _interval in _response_EA:
			_interval_ts = int(_interval/1000)
			#_interval_min = int(datetime.datetime.fromtimestamp(int(_interval/1000)).strftime('%M'))
			break

		# Interval is shorter than the one we want, redo the request with timeframe +5 minutes
		if _interval_ts > int(_myargs['-b1ts']):
			# Print debug message
			if _debug_level == GenDefinitions._DEBUG_VERBOSE:
				print '   Warning! We need to start with %s but the first interval returned is %s\n      We will increase the timeframe from %s to %s and try again' % (_myargs['-b1ts'], str(_interval_ts), str(_timeframe_min), str(int(_timeframe_min)+5))
			_timeframe_min = str(int(_timeframe_min) + 5)
		# Interval is beyond than the one we want, redo the request with timeframe -5 minutes
		elif _interval_ts < int(_myargs['-b1ts']):
			# Print debug message
			if _debug_level == GenDefinitions._DEBUG_VERBOSE:
				print '   Warning! We need to start with %s but the first interval returned is %s\n      We will increase the timeframe from %s to %s and try again' % (_myargs['-b1ts'], str(_interval_ts), str(_timeframe_min), str(int(_timeframe_min)-5))
			_timeframe_min = str(int(_timeframe_min) - 5)
		# Interval is the one we want, quit the while loop and proceed
		else:
			# Print debug message
			if _debug_level == GenDefinitions._DEBUG_VERBOSE:
				print '   OK, we are starting with %s (%s) what was the exactly minute requested' % (str(_interval_ts), _myargs['-b1hr'])
			_skip_interval = 0
			break


##
## Send request to Operational Realtime API for the Queue Health method
##

# This block only runs for 15-min-based intraday reports; NOT for 24-hour-based postday reports
if not _postday_operation:

	# Print debug message
	if _debug_level == GenDefinitions._DEBUG_VERBOSE:
		print '=== Queue Health API request ==='

	# The request is going to be repeated until the obtained time interval minute is the one we want
	#	It is very likely that the request will go through in the first attempt,
	#	because _timeframe_min has been calculated in the first request for the
	#	Agent Activity above.
	while True:

		# Define the params
		_params = { 'timeframe': _timeframe_min,
					'skillIds': str(_myargs['-S']),
					'interval': str(_myargs['-i']),
					'v': str(_myargs['-v']) }

		# Send the request to the API
		_response_QH, _notification_msg = LE.LEGetQueueHealth(_base_uri_OR_API, _lp_account_number, _auth, _params)

		# Check if there is a problem on the API response, and if so quits the program
		if _notification_msg != '':
			# Attach the returned notification and a subject line to the email information
			_email_info = _email_info_template
			_email_info['message'] = _notification_msg
			_email_info['subject'] = _email_tag + ' System error notification'
			# Send email
			Email.sendNotification(_email_info)
			# Exit program because report data is critical information
			print 'Exiting...'
			sys.exit()

		# The request is successful, now checks if the interval is the correct one
		#	To do so, compares the timestamp (without milliseconds) from the first
		#	interval to that of the first bucket we are looking for
		for _interval in _response_QH['metricsByIntervals']:
			_interval_ts = int(_interval['timestamp']/1000)
			break

		# Interval is shorter than the one we want, redo the request with timeframe +5 minutes
		if _interval_ts > int(_myargs['-b1ts']):
			# Print debug message
			if _debug_level == GenDefinitions._DEBUG_VERBOSE:
				print '   Warning! We need to start with %s but the first interval returned is %s\n      We will increase the timeframe from %s to %s and try again' % (_myargs['-b1ts'], str(_interval_ts), str(_timeframe_min), str(int(_timeframe_min)+5))
			_timeframe_min = str(int(_timeframe_min) + 5)
		# Interval is beyond than the one we want, redo the request with timeframe -5 minutes
		elif _interval_ts < int(_myargs['-b1ts']):
			# Print debug message
			if _debug_level == GenDefinitions._DEBUG_VERBOSE:
				print '   Warning! We need to start with %s but the first interval returned is %s\n      We will increase the timeframe from %s to %s and try again' % (_myargs['-b1ts'], str(_interval_ts), str(_timeframe_min), str(int(_timeframe_min)-5))
			_timeframe_min = str(int(_timeframe_min) - 5)
		# Interval is the one we want, quit the while loop and proceed
		else:
			# Print debug message
			if _debug_level == GenDefinitions._DEBUG_VERBOSE:
				print '   OK, we are starting with %s (%s) what was the exactly minute requested' % (str(_interval_ts), _myargs['-b1hr'])
			_skip_interval = 0
			break


##
## Send request to Skills API to retrieve list os all skills
##

# This block only runs for 15-min-based intraday reports; NOT for 24-hour-based postday reports
if not _postday_operation:

	# Print debug message
	if _debug_level == GenDefinitions._DEBUG_VERBOSE:
		print '=== Skills API request ==='

	# Retrieve the list of all skills
	_response_skills, _notification_msg = LE.LEGetAllSkills(_base_uri_USERS_API, _lp_account_number, _auth)

	# Check if there is a problem on the API response, and if so quits the program
	if _notification_msg != '':
		# Attach the returned notification and a subject line to the email information
		_email_info = _email_info_template
		_email_info['message'] = _notification_msg
		_email_info['subject'] = _email_tag + ' System error notification'
		# Send email
		Email.sendNotification(_email_info)
		# Exit program because list of skills is critical information
		print 'Exiting...'
		sys.exit()


##
## Send request to Engagement History API
##

# Print debug message
if _debug_level == GenDefinitions._DEBUG_VERBOSE:
	print '=== Engagement History API request ==='

# Add milliseconds to the from and to times for the request
_eh_from_ts	= str(_myargs['-ehFrom']) + '000'
_eh_to_ts	= str(_myargs['-ehTo']) + '000'

# Same as above, but converts to UTC time
# NOTE: there is a confusion with LP on whether the EH API request requires UTC or local time
_eh_from_ts_utc = str(datetime.datetime.utcfromtimestamp(float(int(_eh_from_ts)/1000)).strftime('%s')) + '000'
_eh_to_ts_utc = str(datetime.datetime.utcfromtimestamp(float(int(_eh_to_ts)/1000)).strftime('%s')) + '000'

# Define JSON body data for the request
_data = '{ "start": { "from": ' + _eh_from_ts + ', "to": ' + _eh_to_ts + ' } }'
#_data = '{ "start": { "from": ' + _eh_from_ts_utc + ', "to": ' + _eh_to_ts_utc + ' } }'

# Define the headers for the request
_headers = { 'Content-Type': 'application/json' }


# Requests to the Engagement History are limited to 100 chats per return
#	This while loop allows for as many requests as needed to retrieve all
#	the data from the desired period
_next = False

# Copy the base URI to a variable that will change depending on how many requests to the EH API will be made
_base_uri_eh = _base_uri_EH_API
# Start the array that will accumulate the information about all interactions
_interactions_eh = json.loads('{ "interactions": [] }')

while True:

	# Send the request to the API
	_response_eh, _notification_msg = LE.LEGetEngHistory(_next, _base_uri_eh, _lp_account_number, _auth, _data, _headers)

	# Check if there is a problem on the report generatino, and if so quits the program
	if _notification_msg != '':
		# attach the returned notification and a subject line to the email information
		_email_info = _email_info_template
		_email_info['message'] = _notification_msg
		_email_info['subject'] = _email_tag + ' System error notification'
		# Send email
		Email.sendNotification(_email_info)
		# Exit program because report data is critical information
		print 'Exiting...'
		sys.exit()

	# Accumulate the interactions to be processed later
	_interactions_eh['interactions'] += _response_eh['interactionHistoryRecords']

	# Print debug message
	if _debug_level == GenDefinitions._DEBUG_VERBOSE:
		print '   Fetched %d out of %d interactions' % (len(_interactions_eh['interactions']), _response_eh['_metadata']['count'])
		#print '   ' + _response_eh['_metadata']['self']['href'] + ' out of ' + str(_response_eh['_metadata']['count'])

	# Replace _base_uri_eh by the 'next' URL to request the next bucket of interactions, if any
	if 'next' in _response_eh['_metadata']:
		_base_uri_eh = _response_eh['_metadata']['next']['href']
		_next = True
	# If there are no further interactions to fetch, just quits the loop
	else:
		break


##########


########## Save JSON content to files for debugging purposes ##########

# Print debug message
if _debug_level == GenDefinitions._DEBUG_VERBOSE:
	print '=== Saving JSON responses into files ==='

# Save JSON returned from the Agent Activity stored in _response_AA
#	The file receives the name AS referring to the report name Agent System (AS_*)
# This block only runs for 15-min-based intraday reports; NOT for 24-hour-based postday reports
if not _postday_operation:
	_output_json_filename = _myargs['-j'] + 'AS_' + _myargs['-n'] + '.json'
	if _debug_level == GenDefinitions._DEBUG_VERBOSE:
		print '   Saving Agent Activity JSON reply to ' + _output_json_filename
	_output_json_file = open(_output_json_filename, 'w')
	_output_json_file.write(json.dumps(_response_AA))
	_output_json_file.close()

# Save JSON returned from the Engagement Activity stored in _response_EA
#	The file receives the name AQ referring to the report name Agent Queue (AQ_*)
# This block only runs for 15-min-based intraday reports; NOT for 24-hour-based postday reports
if not _postday_operation:
	_output_json_filename = _myargs['-j'] + 'AQ_' + _myargs['-n'] + '.json'
	if _debug_level == GenDefinitions._DEBUG_VERBOSE:
		print '   Saving Engagement Activity JSON reply to ' + _output_json_filename
	_output_json_file = open(_output_json_filename, 'w')
	_output_json_file.write(json.dumps(_response_EA))
	_output_json_file.close()

# Save JSON returned from the Agent Activity stored in _response_QH
#	The file receives the name AS referring to the report name Queue (Q_*)
# This block only runs for 15-min-based intraday reports; NOT for 24-hour-based postday reports
if not _postday_operation:
	_output_json_filename = _myargs['-j'] + 'Q_' + _myargs['-n'] + '.json'
	if _debug_level == GenDefinitions._DEBUG_VERBOSE:
		print '   Saving Queue Health JSON reply to ' + _output_json_filename
	_output_json_file = open(_output_json_filename, 'w')
	_output_json_file.write(json.dumps(_response_QH))
	_output_json_file.close()

# Save JSON containing all the interactions retrieved through EH API
#	The file receives the name EH referring to the Engagement History API
# Set the filename depending on the type of report (intra or postday)
if not _postday_operation:
	_output_json_filename = _myargs['-j'] + 'EH_' + _myargs['-n'] + '.json'
else:
	_output_json_filename = _myargs['-j'] + 'EH_' + _myargs['-n'] + '.refreshed.json'

if _debug_level == GenDefinitions._DEBUG_VERBOSE:
	print '   Saving Engagement History JSON content to ' + _output_json_filename
_output_json_file = open(_output_json_filename, 'w')
_output_json_file.write(json.dumps(_interactions_eh))
_output_json_file.close()


##########


########## Calculating Agent System report fields from OR API ##########

# Array initialization of missing Employee ID
_missing_employee_id = []

# This block only runs for 15-min-based intraday reports; NOT for 24-hour-based postday reports
if not _postday_operation:

	# Print debug message
	if _debug_level == GenDefinitions._DEBUG_VERBOSE:
		print '=== Calculating Agent System (AS_*) report fields from OR API ==='

	# Initialize the report fields
	_report_agent_id			    = 0     # Used to retrieve the employee ID
	_report_employee_id			    = 0     # Employee ID retrieved from Users API
	_report_internal_contacts	    = 0     # Report 0
	_report_internal_time           = 0     # Report 0
	_report_ready_time              = {}    # Not chatting in "online" state
	_report_not_ready_time          = {}    # (logged in time - online time)
	_report_outbound_contacts       = 0     # Report 0
	_report_outbound_handle_time    = 0     # Report 0
	_report_login_time              = {}    # logged in status time in this interval

	# Print debug message
	if _debug_level == GenDefinitions._DEBUG_VERBOSE:
		print '   Intervals, running from ' + _myargs['-b1hr'] + ' to ' + _myargs['-b3hr']

	# Control for which intervals to process
	_start_bucket = int(str(datetime.datetime.fromtimestamp(int(_myargs['-d'])).strftime('%M')))
	_end_bucket = int(str(datetime.datetime.fromtimestamp(int(_myargs['-F'])).strftime('%M')))

	# Control the number of intervals to process (should be 3)
	_count_interval = 0
	# Interact over time intervals
	for _interval in _response_AA['metricsByIntervals']:

		# Print debug message
		if _debug_level == GenDefinitions._DEBUG_VERBOSE:
			print '      -- Interval ' + datetime.datetime.fromtimestamp(int(_interval['timestamp']/1000)).strftime('%M') + ' --'

		# Save the current interval and removes the milliseconds
		_cur_interval_ts = int(_interval['timestamp']/1000)

		# Only processes intervals of interest
		if _cur_interval_ts == int(_myargs['-b1ts']) or _cur_interval_ts == int(_myargs['-b2ts']) or _cur_interval_ts == int(_myargs['-b3ts']):

			# Increment the interval control variable
			_count_interval += 1

			# Interact over each agent reported in the given time interval
			for _agent in _interval['metricsData']['agentsMetrics']['metricsPerAgent']:

				# Save agent ID to later use to find the employee ID
				_report_agent_id = _agent

				# If agent ID not in the known mapping to employee ID, find the employee ID
				if _report_agent_id not in _agent_employee_id:
					if _debug_level == GenDefinitions._DEBUG_VERBOSE:
						print '         Employee ID for agent ' + str(_report_agent_id) + ' is missing, finding it...'
					_employee_id_found, _notification_msg = LE.LEGetEmployeeID(_base_uri_USERS_API, _lp_account_number, _auth, _report_agent_id)

					# Check if request for employee ID was successful
					if _notification_msg != '':
						# Print debug message in case of fail
						#   Continue program run, since employee ID is not critical information
						#   Ignore this agent information in the report
						if _debug_level == GenDefinitions._DEBUG_VERBOSE:
							print '            * Could not retrieve an employee ID for agent ' + str(_report_agent_id)
						# Check if this key is already in the array of missing employee id
						#	If not, this entry WILL BE be inserted in the array
						if str(_report_agent_id) not in _missing_employee_id:
							_missing_employee_id.append(str(_report_agent_id))

					# Check if employee ID is '-1'
					elif _employee_id_found == '-1':
						# Print debug message in case employee ID was not found
						#   Continue program run, since employee ID is not critical information
						#   Ignore this agent information in the report
						if _debug_level == GenDefinitions._DEBUG_VERBOSE:
							print '            * There is no employee ID for agent ' + str(_report_agent_id)
						# It was sucessful, save the newly found employee ID
					else:
						# Save to mapping array, which later goes to the file
						_agent_employee_id[_report_agent_id] = _employee_id_found

				# Continue if we have a valid employee ID
				if _report_agent_id in _agent_employee_id:

					# Check if there is an entry for this agent in report data, if not create one
					if _report_agent_id not in _report_ready_time:
						_report_ready_time[_report_agent_id]        = int(_interval['metricsData']['agentsMetrics']['metricsPerAgent'][_agent][0]['value']['notChatting'])
						_report_not_ready_time[_report_agent_id]    = int(_interval['metricsData']['agentsMetrics']['metricsPerAgent'][_agent][3]['value']['total']) - \
																	int(_interval['metricsData']['agentsMetrics']['metricsPerAgent'][_agent][0]['value']['total'])
						_report_login_time[_report_agent_id]        = int(_interval['metricsData']['agentsMetrics']['metricsPerAgent'][_agent][3]['value']['total'])
					# There is an entry, just accumulate the information
					else:
						_report_ready_time[_report_agent_id]        += int(_interval['metricsData']['agentsMetrics']['metricsPerAgent'][_agent][0]['value']['notChatting'])
						_report_not_ready_time[_report_agent_id]    += int(_interval['metricsData']['agentsMetrics']['metricsPerAgent'][_agent][3]['value']['total']) - \
																	int(_interval['metricsData']['agentsMetrics']['metricsPerAgent'][_agent][0]['value']['total'])
						_report_login_time[_report_agent_id]        += int(_interval['metricsData']['agentsMetrics']['metricsPerAgent'][_agent][3]['value']['total'])

		# Quits the loop after processing 3 intervals
		#	or if current interval timestamp is higher than the last one to be processed
		if _count_interval == 3 or _cur_interval_ts > int(_myargs['-b3ts']):
			break

##########


########## Calculating Agent Queue report fields from OR API ##########

# NOTE: This entire section for Agent Queue is deprecated because the
#		only two fields of interest _report_handled and _report_handle_time
#		are now calculated using EH API data.

# This block only runs for 15-min-based intraday reports; NOT for 24-hour-based postday reports
if not _postday_operation:

	# Print debug message
	if _debug_level == GenDefinitions._DEBUG_VERBOSE:
		print '=== Calculating Agent Queue (AQ_*) report fields from OR API ==='

	# Initialize report fields
	_report_skill_id                = 0     # Skill ID also known as Queue ID
	_report_agent_id                = 0     # Used to find the employee ID
	_report_employee_id             = 0     # Retrieved from Users API
	_report_handled                 = {}    # Using EH API
	_report_handle_time             = {}    # Using EH API
	_report_hold_time               = 0     # Report 0
	_report_work_time               = 0     # Report 0
	_report_right_party_contacts    = 0     # Report 0
	_report_right_party_talk_time   = 0     # Report 0
	_report_wrong_party_contacs     = 0     # Report 0
	_report_wrong_party_talk_time   = 0     # Report 0

	# Dictionary to accumulate handle time for the Queue Data report
	# NOTE: Deprecated by the newest solution that computes this field from EH API data
	_queue_handle_time = {}

	# Print debug message
	if _debug_level == GenDefinitions._DEBUG_VERBOSE:
		print '   Intervals, running from ' + _myargs['-b1ts'] + ' to ' + _myargs['-b3ts']

	# NOTE: control variables _start_bucket and _end_bucket have been calculated above

	# Control the number of intervals to process (should be 3)
	_count_interval = 0
	# Interact over time intervals
	for _interval in _response_EA:

		# Print debug message
		if _debug_level == GenDefinitions._DEBUG_VERBOSE:
			print '      -- Interval ' + datetime.datetime.fromtimestamp(int(_interval/1000)).strftime('%M') + ' --'

		# Save the current interval and removes the milliseconds
		_cur_interval_ts = int(_response_EA[_interval]['timestamp']/1000)

		# Only processes intervals of interest
		if _cur_interval_ts == int(_myargs['-b1ts']) or _cur_interval_ts == int(_myargs['-b2ts']) or _cur_interval_ts == int(_myargs['-b3ts']):

			# Increment the interval control variable
			_count_interval += 1

			# Interact over each skill reported in the given time interval
			for _skill in _response_EA[_interval]['metricsData']['skillsMetricsPerAgent']['metricsPerSkill']:

				# Save the skill ID
				_report_skill_id = str(_skill)

				# Interact over each agent reported within the specific skill
				for _agent in _response_EA[_interval]['metricsData']['skillsMetricsPerAgent']['metricsPerSkill'][_skill]['metricsPerAgent']:

					# Save the agent ID
					_report_agent_id = str(_agent)

					# Retrieve the employee ID
					if str(_report_agent_id) not in _agent_employee_id:
						if _debug_level == GenDefinitions._DEBUG_VERBOSE:
							print '         Employee ID for agent ' + str(_report_agent_id) + ' is missing, finding it...'
						_employee_id_found, _notification_msg = LE.LEGetEmployeeID(_base_uri_USERS_API, _lp_account_number, _auth, _report_agent_id)

						# Check if request for employee ID was successful
						if _notification_msg != '':
							# Print debug message in case of fail
							#   Continue program run, since employee ID is not critical information
							#   Ignore this agent information in the report
							if _debug_level == GenDefinitions._DEBUG_VERBOSE:
								print '            * Could not retrieve an employee ID for agent ' + str(_report_agent_id)
							# Check if this key is already in the array of missing employee id
							#	If not, this entry WILL BE be inserted in the array
							if str(_report_agent_id) not in _missing_employee_id:
								_missing_employee_id.append(str(_report_agent_id))
						# Check if employee ID is '-1'
						elif _employee_id_found == '-1':
							# Print debug message in case employee ID was not found
							#   Continue program run, since employee ID is not critical information
							#   Ignore this agent information in the report
							if _debug_level == GenDefinitions._DEBUG_VERBOSE:
								print '            * There is no employee ID for agent ' + str(_report_agent_id)
						# It was sucessful, save the newly found employee ID
						else:
							# Save to mapping array so that this new agent-employee is considered in the data processing below
							_agent_employee_id[_report_agent_id] = _employee_id_found

					# Continue if we have a valid employee ID
					if _report_agent_id in _agent_employee_id:

						# Calculate fields - this is needed because of the accumulator _queue_handle_time
						_value_handled = _response_EA[_interval]['metricsData']['skillsMetricsPerAgent']['metricsPerSkill'][_skill]['metricsPerAgent'][_agent]['totalInteractiveChats'] + \
										_response_EA[_interval]['metricsData']['skillsMetricsPerAgent']['metricsPerSkill'][_skill]['metricsPerAgent'][_agent]['totalNonInteractiveChats']

						_value_handle_time = _response_EA[_interval]['metricsData']['skillsMetricsPerAgent']['metricsPerSkill'][_skill]['metricsPerAgent'][_agent]['totalHandlingTime'] + \
											_response_EA[_interval]['metricsData']['skillsMetricsPerAgent']['metricsPerSkill'][_skill]['metricsPerAgent'][_agent]['nonInteractiveTotalHandlingTime']

						# Create a tuple key (skill ID, agent ID) and accumulate data for the report
						_key = str(_report_skill_id) + ',' + str(_report_agent_id)
						# NOTE: Deprecated by the newest solution that find the values for the fields below using the EH API
						#if _key not in _report_handled:
						#	_report_handled[_key] = _value_handled
						#	_report_handle_time[_key] = _value_handle_time
						#else:
						#	_report_handled[_key] += _value_handled
						#	_report_handle_time[_key] += _value_handle_time

						# Accumulate the handle time for the Queue Data report
						# NOTE: Deprecated by the newest solution that uses EH API data to calculate this field
						if str(_report_skill_id) not in _queue_handle_time:
							_queue_handle_time[str(_report_skill_id)] = _value_handle_time
						else:
							_queue_handle_time[str(_report_skill_id)] += _value_handle_time

		# Quits the loop after processing 3 intervals
		#	or if current interval timestamp is higher than the last one to be processed
		if _count_interval == 3 or _cur_interval_ts > int(_myargs['-b3ts']):
			break

##########


########## Calculate Queue Health report fields from OR API ##########

# This block only runs for 15-min-based intraday reports; NOT for 24-hour-based postday reports
if not _postday_operation:

	# Print debug message
	if _debug_level == GenDefinitions._DEBUG_VERBOSE:
		print '=== Calculating Queue (Q_*) report fields from OR API ==='

	# Initialize report fields
	_report_skill_id                = 0     # Skill ID also known as Queue ID
	_report_contacts_received       = {}    # enteredQEng
	_report_abandoned_short         = 0     # Report 0
	_report_abandoned_long          = {}    # abandonedEng
	_report_handled_short           = {}    # Using EH API data
	_report_handled_long            = {}    # Using EH API data
	_report_queue_handle_time       = {}    # Using EH API data and concurrency factor defined for each skill
	_report_hold_time               = 0     # Report 0
	_report_work_time               = 0     # Report 0
	_report_queue_delay_time        = {}    # Using EH API data
	_report_svc_lvl_pct             = 0     # Report 0
	_report_backlog                 = 0     # Report 0
	_report_backlog_not_expired     = 0     # Report 0
	_report_backlog_expired         = 0     # Report 0
	_report_right_party_contacts    = 0     # Report 0
	_report_right_party_talk_time   = 0     # Report 0
	_report_wrong_party_contacts    = 0     # Report 0
	_report_wrong_party_talk_time   = 0     # Report 0

	# Initialize report variables for each skill in _response_skills
	for _skill_item in _response_skills:
		_report_contacts_received[str(_skill_item['id'])] = 0
		_report_abandoned_long[str(_skill_item['id'])] = 0
		_report_handled_short[str(_skill_item['id'])] = 0
		_report_handled_long[str(_skill_item['id'])] = 0
		_report_queue_delay_time[str(_skill_item['id'])] = 0
		_report_queue_handle_time[str(_skill_item['id'])] = 0

		# NOTE: the following is deprecated by current solution using EH API data
		# Also update _queue_handle_time for skills that are not there yet
		if str(_skill_item['id']) not in _queue_handle_time:
			_queue_handle_time[str(_skill_item['id'])] = 0

	# Print debug message
	if _debug_level == GenDefinitions._DEBUG_VERBOSE:
		print '      Interval, running from ' + _myargs['-b1hr'] + ' to ' + _myargs['-b3hr']

	# NOTE: control variables _start_bucket and _end_bucket have been calculated above

	# Control for the number of intervals to process
	_count_interval = 0
	# Computation of fields using data from Operational Realtime API
	#	Interact over time intervals
	for _interval in _response_QH['metricsByIntervals']:

		# Print debug message
		if _debug_level == GenDefinitions._DEBUG_VERBOSE:
			print '      -- Interval ' + datetime.datetime.fromtimestamp(int(_interval['timestamp']/1000)).strftime('%M') + '  --'

		# Save the current interval and removes the milliseconds
		_cur_interval_ts = int(_interval['timestamp']/1000)

		# Only processes intervals of interest
		if _cur_interval_ts == int(_myargs['-b1ts']) or _cur_interval_ts == int(_myargs['-b2ts']) or _cur_interval_ts == int(_myargs['-b3ts']):

			# Increment the interval control variable
			_count_interval += 1

			# Interact over all skills
			for _skill in _interval['metricsData']['skillsMetrics']:

				# Save skill ID for the report
				_report_skill_id = str(_skill)

				# Record accumulative data for the report
				if str(_report_skill_id) in _report_contacts_received:

					_report_contacts_received[str(_report_skill_id)] += _interval['metricsData']['skillsMetrics'][_skill]['enteredQEng']
					_report_abandoned_long[str(_report_skill_id)] += _interval['metricsData']['skillsMetrics'][_skill]['abandonedEng']
					#_report_handled_long[str(_report_skill_id)] += _interval['metricsData']['skillsMetrics'][_skill]['connectedEng']

				# Skill ID is not in the array, this is a "weird" situation that has to be reported
				else:

					# Print debug message
					if _debug_level == GenDefinitions._DEBUG_VERBOSE:
						print '         * Skill ID unknown, sending email notification...'

					# Send notification email
					_email_info = _email_info_template
					_email_info['message'] = 'Skill ID ' + str(_report_skill_id) + ' was reported by Engagement Activity method of the Operational Realtime API, but it was not reported in the list of skills from Skills API'
					_email_info['subject'] = _email_tag + ' System warning notification'
					Email.sendNotification(_email_info)
					# NOTE: do not quit because this is not a critical problem

		# Quits the loop after processing 3 intervals
		#	or if current interval timestamp is higher than the last one to be processed
		if _count_interval == 3 or _cur_interval_ts > int(_myargs['-b3ts']):
			break


##########

# Array initialization of missing Concurrency Factor
_missing_concurrency_factor = []

########## Calculate combined Agent Queue and Queue Health report fields from EH API ##########
########## INTRADAY REPORTS BLOCK													 ##########

# This block only runs for 15-min-based intraday reports; NOT for 24-hour-based postday reports
if not _postday_operation:

	# Print debug message
	if _debug_level == GenDefinitions._DEBUG_VERBOSE:
		print '=== Calculating Agent Queue (AQ_*) and Queue (Q_*) report fields from EH API ==='
		print '     - from %s to %s' % (datetime.datetime.fromtimestamp(int(_myargs['-ehFrom'])).strftime('%Y-%m-%d %H:%M:%S'), datetime.datetime.fromtimestamp(int(_myargs['-ehTo'])).strftime('%Y-%m-%d %H:%M:%S'))

	# Counters for debug purposes
	_in_range_count = 0
	_out_range_count = 0

	# Computation of fields using data from Engagement History API
	for _interaction in _interactions_eh['interactions']:

		# Only process interactions that actually started within the desired period
		if int(_interaction['info']['startTimeL']) >= int(_eh_from_ts) and int(_interaction['info']['startTimeL']) <= int(_eh_to_ts):

			# Update in range counter and print debug message
			_in_range_count += 1

			# Save skill ID
			_report_skill_id = _interaction['info']['skillId']


			##
			## Compute Agent Queue report fields
			##

			# Save agent ID
			_report_agent_id = _interaction['info']['agentId']

			# Find employee ID for this agent is unknown, try to find it
			if str(_report_agent_id) not in _agent_employee_id:

				# Print debug message
				if _debug_level == GenDefinitions._DEBUG_VERBOSE:
					print '         Employee ID for agent ' + str(_report_agent_id) + ' is missing, finding it...'
					_employee_id_found, _notification_msg = LE.LEGetEmployeeID(_base_uri_USERS_API, _lp_account_number, _auth, _report_agent_id)

				# Check if request for employee ID was successful
				if _notification_msg != '':
					# Print debug message in case of fail
					#	Continue program run, since employee ID is not critical information
					#	Ignore this agent information in the report
					if _debug_level == GenDefinitions._DEBUG_VERBOSE:
						print '            * Could not retrieve an employee ID for agent ' + str(_report_agent_id)
					# Check if this key is already in the array of missing employee id
					#	If not, this entry WILL BE be inserted in the array
					if str(_report_agent_id) not in _missing_employee_id:
						_missing_employee_id.append(str(_report_agent_id))
				# Check if employee ID is '-1'
				elif _employee_id_found == '-1':
					# Print debug message in case employee ID was not found
					#	Continue program run, since employee ID is not critical information
					#	Ignore this agent information in the report
					if _debug_level == GenDefinitions._DEBUG_VERBOSE:
						print '            * There is no employee ID for agent ' + str(_report_agent_id)
				# It was successful, save the newly found employee ID
				else:
					# Save to mapping array so that this new agent-employee is considered in the data procesing below
					_agent_employee_id[str(_report_agent_id)] = _employee_id_found

			# Continue if we have a valid employee ID
			if str(_report_agent_id) in _agent_employee_id:

				# Create key tuple (skill ID, agent ID)
				_aq_key = str(_report_skill_id) + ',' + str(_report_agent_id)

				# Check if this key is already in the accumulation arrays, and if not creates the entry for it
				if _aq_key not in _report_handled:
					_report_handled[_aq_key] = 0
					_report_handle_time[_aq_key] = 0

				# Increment the number of handled interactions
				_report_handled[_aq_key] += 1

				# Accumulate the total duration of this interaction
				#	To do so, multiply the duration by the concurrency factor
				#	First check if concurrency factor is in there
				if str(_report_skill_id) not in _concurrency_factor:

					# Print debug message
					#if _debug_level == GenDefinitions._DEBUG_VERBOSE:
					print '         * Concurrency factor value is not defined for skill ' + str(_report_skill_id)

					# Check if this key is already in the array of missing concurrency factor
					#	If not, this entry WILL BE be inserted in the array
					if str(_report_skill_id) not in _missing_concurrency_factor:
						_missing_concurrency_factor.append(str(_report_skill_id))

					#print '-- DF agent %s and skill %s multiplying duration %d by %f = %d' % (str(_report_agent_id), str(_report_skill_id), int(_interaction['info']['duration']), float(_myargs['-dcf']), int(int(_interaction['info']['duration']) * float(_myargs['-dcf'])))
					_report_handle_time[_aq_key] += int( int(_interaction['info']['duration']) * float(_myargs['-dcf']) )

				# There is a concurrency factor, use that for the multiplication
				else:
					#print '-- CC agent %s and skill %s multiplying duration %d by %f = %d' % (str(_report_agent_id), str(_report_skill_id), int(_interaction['info']['duration']), float(_concurrency_factor[str(_report_skill_id)]), int(int(_interaction['info']['duration']) * float(_concurrency_factor[str(_report_skill_id)])))
					_report_handle_time[_aq_key] += int( int(_interaction['info']['duration']) * float(_concurrency_factor[str(_report_skill_id)]) )


			##
			## Compute Queue Health report fields
			##

			# Check first if the agent information was also reported, by looking if there is a valid employee ID for the agent
			# NOTE: This check prevents that data related to bots are summed in the Queue Health report, but disregarded in the Agent Queue report
			if str(_report_agent_id) in _agent_employee_id:

				# Record accumulative data for the fields that use data from EH API
				if str(_report_skill_id) in _report_queue_delay_time:

					# Calculate wait time for the chat (in seconds)
					_wait_time = int(_interaction['info']['startTimeL'])/1000 - int(_interaction['info']['chatRequestedTimeL'])/1000

					# Accumulate the sum of difference between start time and chat request time (in seconds)
					_report_queue_delay_time[str(_report_skill_id)] += _wait_time

					# Increment counter for long chats that wait time >= 20 seconds, or short chats that wait time < 20 seconds
					if _wait_time < 20:
						_report_handled_short[str(_report_skill_id)] += 1
					else:
						_report_handled_long[str(_report_skill_id)] += 1

					# If the interaction has not ended, cap the duration time
					#	Otherwise, just get the duration as informed in the returned JSON
					#	Cap'ing formula: (report call time) - (chat start time)
					if str(_interaction['info']['endTimeL']) == '':
						_report_duration = int( int(_myargs['-cts']) - int(_interaction['info']['startTimeL'])/1000 )
					else:
						_report_duration = int(_interaction['info']['duration'])

					# Multiply the _report_duration by the concurrency factor for the given skill
					#	First, check if there is a concurrency factor value for the skill, if not, use the default one
					if str(_report_skill_id) not in _concurrency_factor:

						# Print debug message
						#if _debug_level == GenDefinitions._DEBUG_VERBOSE:
						print '         * Concurrency factor value is not defined for skill ' + str(_report_skill_id)

						# Check if this key is already in the array of missing concurrency factor
						#	If not, this entry WILL BE be inserted in the array
						if str(_report_skill_id) not in _missing_concurrency_factor:
							_missing_concurrency_factor.append(str(_report_skill_id))

						## Send notification email, but continue script run because lack of concurrency factor value is not critical
						#_email_info = _email_info_template
						#_email_info['message'] = 'No concurrency factor defined for skill ' + str(_report_skill_id) + '. Default concurrency factor value of ' + str(_myargs['-dcf']) + ' was used instead.'
						#_email_info['subject'] = _email_tag + ' System warning notification'
						##Email.sendNotification(_email_info)

						# Multiply by the default concurrency factor value informed in the configuration file
						_temp_sum_dur = _report_duration * float(_myargs['-dcf'])
						_report_queue_handle_time[str(_report_skill_id)] += int( _temp_sum_dur )
						#_report_queue_handle_time[str(_report_skill_id)] += int( _report_duration * float(_myargs['-dcf']) )
						#print '-- DF skill %s multiplying duration %d by %f = %d' % (str(_report_skill_id), int(_interaction['info']['duration']), float(_myargs['-dcf']), int(int(_interaction['info']['duration']) * float(_myargs['-dcf'])))

					# There is a concurrency factor, use that for the multiplication
					else:
						_temp_sum_dur = _report_duration * float(_concurrency_factor[str(_report_skill_id)])
						_report_queue_handle_time[str(_report_skill_id)] += int( _temp_sum_dur )
						#_report_queue_handle_time[str(_report_skill_id)] += int( _report_duration * float(_concurrency_factor[str(_report_skill_id)]) )
						#print '-- CC skill %s multiplying duration %d by %f = %d' % (str(_report_skill_id), int(_interaction['info']['duration']), float(_concurrency_factor[str(_report_skill_id)]), int(int(_interaction['info']['duration']) * float(_concurrency_factor[str(_report_skill_id)])))

				# Skill ID is not in the array, this is a "weird" situation that has to be reported
				else:

					# Print debug message
					if _debug_level == GenDefinitions._DEBUG_VERBOSE:
						print '         * Skill ID unknown, sending email notification...'

					# Send notification email
					_email_info = _email_info_template
					_email_info['message'] = 'Skill ID ' + str(_report_skill_id) + ' was reported by Engagement History API, but it was not reported in the list of skills from Skills API'
					_email_info['subject'] = _email_tag + ' System warning notification'
					Email.sendNotification(_email_info)
					# NOTE: do not quit because this is not a critical problem

		# The start of this interaction is out of the interval of interest
		else:

			# Update out range counter and print debug message
			_out_range_count += 1
			#if _debug_level == GenDefinitions._DEBUG_VERBOSE:
			#	print '         * Out of range: chat by agent %s on queue %s started at %s' % (str(_interaction['info']['agentId']), str(_interaction['info']['skillId']), str(_interaction['info']['startTime']))

	# Send notification email, but continue script run because lack of concurrency factor value is not critical
	if _missing_concurrency_factor:
		_email_info = _email_info_template
		_email_info['subject'] = _email_tag + ' System warning notification'
		_email_info['message'] = 'No concurrency factor defined for skill(s): \n'
		for _no_concurrency in _missing_concurrency_factor:
			_email_info['message'] +=  str(_no_concurrency) + '\n'

		_email_info['message'] += 'Default concurrency factor value of ' + str(_myargs['-dcf']) + ' was used instead.'
		Email.sendNotification(_email_info)

	# Print debug message
	if _debug_level == GenDefinitions._DEBUG_VERBOSE:
		print "In range	= %d" % _in_range_count
		print "Out range	= %d" % _out_range_count
		print "Total chats	= %d" % (_in_range_count + _out_range_count)


##########


########## Calculate combined Agent Queue and Queue Health report fields from EH API ##########
########## POSTDAY REPORTS BLOCK													 ##########

# This block only runs for 24-hour-based postday reports; NOT for 15-min-based intraday reports
if _postday_operation:

	# Print debug message
	if _debug_level == GenDefinitions._DEBUG_VERBOSE:
		print '=== Calculating Agent Queue (AQ_*) and Queue (Q_*) report fields from EH API ==='
		print '     - from %s to %s' % (datetime.datetime.fromtimestamp(int(_myargs['-ehFrom'])).strftime('%Y-%m-%d %H:%M:%S'), datetime.datetime.fromtimestamp(int(_myargs['-ehTo'])).strftime('%Y-%m-%d %H:%M:%S'))

	# Counters for debug purposes
	_in_range_count = 0
	_out_range_count = 0

	# Computation of fields using data from Engagement History API
	for _interaction in _interactions_eh['interactions']:

		# Only process interactions that actually started within the desired period
		if int(_interaction['info']['startTimeL']) >= int(_eh_from_ts) and int(_interaction['info']['startTimeL']) <= int(_eh_to_ts):

			# Update in range counter and print debug message
			_in_range_count += 1

			# Save skill ID
			_report_skill_id = _interaction['info']['skillId']


			##
			## Compute Agent Queue report fields
			##

			# Save agent ID
			_report_agent_id = _interaction['info']['agentId']

			# Find employee ID for this agent is unknown, try to find it
			if str(_report_agent_id) not in _agent_employee_id:

				# Print debug message
				if _debug_level == GenDefinitions._DEBUG_VERBOSE:
					print '         Employee ID for agent ' + str(_report_agent_id) + ' is missing, finding it...'
					_employee_id_found, _notification_msg = LE.LEGetEmployeeID(_base_uri_USERS_API, _lp_account_number, _auth, _report_agent_id)

				# Check if request for employee ID was successful
				if _notification_msg != '':
					# Print debug message in case of fail
					#	Continue program run, since employee ID is not critical information
					#	Ignore this agent information in the report
					if _debug_level == GenDefinitions._DEBUG_VERBOSE:
						print '            * Could not retrieve an employee ID for agent ' + str(_report_agent_id)
					# Check if this key is already in the array of missing employee id
					#	If not, this entry WILL BE be inserted in the array
					if str(_report_agent_id) not in _missing_employee_id:
						_missing_employee_id.append(str(_report_agent_id))
				# Check if employee ID is '-1'
				elif _employee_id_found == '-1':
					# Print debug message in case employee ID was not found
					#	Continue program run, since employee ID is not critical information
					#	Ignore this agent information in the report
					if _debug_level == GenDefinitions._DEBUG_VERBOSE:
						print '            * There is no employee ID for agent ' + str(_report_agent_id)
				# It was successful, save the newly found employee ID
				else:
					# Save to mapping array so that this new agent-employee is considered in the data procesing below
					_agent_employee_id[str(_report_agent_id)] = _employee_id_found

			# Continue if we have a valid employee ID
			if str(_report_agent_id) in _agent_employee_id:

				# Keys brought back from the XML file contain the combination Skill ID and Employee ID
				#	instead of the traditional Skill ID and Agent ID
				_report_employee_id = _agent_employee_id[str(_report_agent_id)]

				# Create key tuple (skill ID, employee ID) -- just as in the XML file of the intraday report
				_aq_key = str(_report_skill_id) + ',' + str(_report_employee_id)

				# Check if this key is already in the arrays of values brought back from the XML files
				#	If not, this entry WILL BE reported and has to be initialized in the array
				if _aq_key not in _postday_aq_keys:

					_postday_aq_handled[_aq_key] = 0
					_postday_aq_handle_time[_aq_key] = 0

				# Increment the number of handled interactions
				_postday_aq_handled[_aq_key] += 1

				# Accumulate the total duration of this interaction
				#	To do so, multiply the duration by the concurrency factor
				#	First check if concurrency factor is in there
				if str(_report_skill_id) not in _concurrency_factor:

					# Print debug message
					if _debug_level == GenDefinitions._DEBUG_VERBOSE:
						print '         * Concurrency factor value is not defined for skill ' + str(_report_skill_id)
						print '           Notification email has been sent on the intraday report generation'

					# Check if this key is already in the array of missing concurrency factor
					#	If not, this entry WILL BE be inserted in the array
					if str(_report_skill_id) not in _missing_concurrency_factor:
						_missing_concurrency_factor.append(str(_report_skill_id))

					#print '-- DF agent %s and skill %s multiplying duration %d by %f = %d' % (str(_report_agent_id), str(_report_skill_id), int(_interaction['info']['duration']), float(_myargs['-dcf']), int(int(_interaction['info']['duration']) * float(_myargs['-dcf'])))
					_postday_aq_handle_time[_aq_key] += int( int(_interaction['info']['duration']) * float(_myargs['-dcf']) )

				# There is a concurrency factor, use that for the multiplication
				else:
					#print '-- CC agent %s and skill %s multiplying duration %d by %f = %d' % (str(_report_agent_id), str(_report_skill_id), int(_interaction['info']['duration']), float(_concurrency_factor[str(_report_skill_id)]), int(int(_interaction['info']['duration']) * float(_concurrency_factor[str(_report_skill_id)])))
					_postday_aq_handle_time[_aq_key] += int( int(_interaction['info']['duration']) * float(_concurrency_factor[str(_report_skill_id)]) )


			##
			## Compute Queue Health report fields
			##

			# Check first if the agent information was also reported, by looking if there is a valid employee ID for the agent
			if str(_report_agent_id) in _agent_employee_id:

				# Record accumulative data for the fields that use data from EH API
				if str(_report_skill_id) in _postday_q_queue_delay_time:

					# Calculate wait time for the chat (in seconds)
					_wait_time = int(_interaction['info']['startTimeL'])/1000 - int(_interaction['info']['chatRequestedTimeL'])/1000

					# Accumulate the sum of difference between start time and chat request time (in seconds)
					_postday_q_queue_delay_time[str(_report_skill_id)] += _wait_time

					# Increment counter for long chats that wait time >= 20 seconds, or short chats that wait time < 20 seconds
					if _wait_time < 20:
						_postday_q_handled_short[str(_report_skill_id)] += 1
					else:
						_postday_q_handled_long[str(_report_skill_id)] += 1

					# If the interaction has not ended, cap the duration time
					#	Otherwise, just get the duration as informed in the returned JSON
					#	Cap'ing formula: (report call time) - (chat start time)
					if str(_interaction['info']['endTimeL']) == '':
						_report_duration = int( int(_myargs['-cts']) - int(_interaction['info']['startTimeL'])/1000 )
					else:
						_report_duration = int(_interaction['info']['duration'])

					# Multiply the _report_duration by the concurrency factor for the given skill
					#	First, check if there is a concurrency factor value for the skill, if not, use the default one
					if str(_report_skill_id) not in _concurrency_factor:

						# Print debug message
						if _debug_level == GenDefinitions._DEBUG_VERBOSE:
							print '         * Concurrency factor value is not defined for skill ' + str(_report_skill_id)
							print '           Notification email has been sent on the intraday report generation'

						# Check if this key is already in the array of missing concurrency factor
						#	If not, this entry WILL BE be inserted in the array
						if str(_report_skill_id) not in _missing_concurrency_factor:
							_missing_concurrency_factor.append(str(_report_skill_id))

						# Multiply by the default concurrency factor value informed in the configuration file
						_temp_sum_dur = _report_duration * float(_myargs['-dcf'])
						_postday_q_handle_time[str(_report_skill_id)] += int( _temp_sum_dur )

					# There is a concurrency factor, use that for the multiplication
					else:
						_temp_sum_dur = _report_duration * float(_concurrency_factor[str(_report_skill_id)])
						_postday_q_handle_time[str(_report_skill_id)] += int( _temp_sum_dur )

				# Skill ID is not in the array, this is a "weird" situation that has to be reported
				else:

					# Print debug message
					if _debug_level == GenDefinitions._DEBUG_VERBOSE:
						print '         * Skill ID was not in the XML intraday report'
						print '           Notification email has been sent on the intraday report generation'

		# The start of this interaction is out of the interval of interest
		else:

			# Update out range counter and print debug message
			_out_range_count += 1

	# Send notification email, but continue script run because lack of concurrency factor value is not critical
	if _missing_concurrency_factor:
		_email_info = _email_info_template
		_email_info['subject'] = _email_tag + ' System warning notification'
		_email_info['message'] = 'No concurrency factor defined for skill(s): \n'
		for _no_concurrency in _missing_concurrency_factor:
			_email_info['message'] +=  str(_no_concurrency) + '\n'

		_email_info['message'] += 'Default concurrency factor value of ' + str(_myargs['-dcf']) + ' was used instead.\n'
		Email.sendNotification(_email_info)

	# Print debug message
	if _debug_level == GenDefinitions._DEBUG_VERBOSE:
		print "In range	= %d" % _in_range_count
		print "Out range	= %d" % _out_range_count
		print "Total chats	= %d" % (_in_range_count + _out_range_count)

##########

# Send notification email, but continue script run because lack of concurrency factor value is not critical
if _missing_employee_id:
	_email_info = _email_info_template
	_email_info['subject'] = _email_tag + ' System warning notification'
	_email_info['message'] = 'Missing employee ID(s): \n'
	for _no_employee_id in _missing_employee_id:
		_email_info['message'] +=  str(_no_employee_id) + '\n'

	_email_info['message'] += 'Default Employee Id value of -1 was used instead.\n'
	Email.sendNotification(_email_info)


########## Create Agent System Data report (XML file) ##########

# This block only runs for 15-min-based intraday reports; NOT for 24-hour-based postday reports
if not _postday_operation:

	# Print debug message
	if _debug_level == GenDefinitions._DEBUG_VERBOSE:
		print '=== Creating Agent System (AS_*) Data report (XML file) ==='

	# Root HistPlugin node
	_xml_root = et.Element('HistPlugin')
	# XML DataSourceNode node
	_xml_data_source_node = et.SubElement(_xml_root, 'DataSourceNode')
	# XML Vendor node
	et.SubElement(_xml_data_source_node, 'Vendor').text = 'Telstra'

	# XML AgentSystemNode node
	_xml_agent_system_node = et.SubElement(_xml_data_source_node, 'AgentSystemNode')
	# XML - add time period for the report - format is %Y%m%dT%H%M - Note the T between %d and %H
	_xml_time_period = et.SubElement(_xml_agent_system_node, 'TimePeriod')
	# Time period DateTime node in specific format
	_report_date_time = datetime.datetime.fromtimestamp(int(_myargs['-d'])-300).strftime('%Y%m%dT%H%M')
	et.SubElement(_xml_time_period, 'DateTime').text = _report_date_time

	# Interact over each agent recorded within one of the _report_* dictionaries and record data to XML
	for _agent in _report_ready_time:

		# XML AgentSystemData node
		_xml_agent_system_data = et.SubElement(_xml_agent_system_node, 'AgentSystemData')

		# XML AgentValue node
		et.SubElement(_xml_agent_system_data, 'AgentValue').text = str(_agent_employee_id[_agent])

		# XML InternalContacts node, value fixed to 0
		_xml_internal_contacts = et.SubElement(_xml_agent_system_data, 'InternalContacts')
		et.SubElement(_xml_internal_contacts, 'count').text = str(_report_internal_contacts)

		# XML InternalHandleTime node, value fixed to 0
		_xml_internal_handle_time = et.SubElement(_xml_agent_system_data, 'InternalHandleTime')
		_xml_internal_handle_time_dur = et.SubElement(_xml_internal_handle_time, 'duration')
		et.SubElement(_xml_internal_handle_time_dur, 'totalseconds').text = str(_report_internal_time)

		# XML ReadyTime node, value from _report_ready_time, otherwise empty
		_xml_ready_time = et.SubElement(_xml_agent_system_data, 'ReadyTime')
		_xml_ready_time_dur = et.SubElement(_xml_ready_time, 'duration')
		if _agent in _report_ready_time:
			et.SubElement(_xml_ready_time_dur, 'totalseconds').text = str(_report_ready_time[_agent])
		else:
			et.SubElement(_xml_ready_time_dur, 'totalseconds').text = ""

		# XML NotReadyTime node, value from _report_not_ready_time, otherwise empty
		_xml_not_ready_time = et.SubElement(_xml_agent_system_data, 'NotReadyTime')
		_xml_not_ready_time_dur = et.SubElement(_xml_not_ready_time, 'duration')
		if _agent in _report_not_ready_time:
			et.SubElement(_xml_not_ready_time_dur, 'totalseconds').text = str(_report_not_ready_time[_agent])
		else:
			et.SubElement(_xml_not_ready_time_dur, 'totalseconds').text = ""

		# XML OutboundContacts node, value fixed to 0
		_xml_outbound_contacts = et.SubElement(_xml_agent_system_data, 'OutboundContacts')
		et.SubElement(_xml_outbound_contacts, 'count').text = str(_report_outbound_contacts)

		# XML OutboundHandleTime node, value fixed to 0
		_xml_outbound_handle_time = et.SubElement(_xml_agent_system_data, 'OutboundHandleTime')
		_xml_outbound_handle_time_dur = et.SubElement(_xml_outbound_handle_time, 'duration')
		et.SubElement(_xml_outbound_handle_time_dur, 'totalseconds').text = str(_report_outbound_handle_time)

		# XML LoginTime node, value from _report_login_time, otherwise empty
		_xml_login_time = et.SubElement(_xml_agent_system_data, 'LoginTime')
		_xml_login_time_dur = et.SubElement(_xml_login_time, 'duration')
		if _agent in _report_login_time:
			et.SubElement(_xml_login_time_dur, 'totalseconds').text = str(_report_login_time[_agent])
		else:
			et.SubElement(_xml_login_time_dur, 'totalseconds').text = ""

	# Record the XML tree in the report file and clear the XML tree for further use
	tree = et.ElementTree(_xml_root)
	tree.write(_report_AS_file_name)
	_xml_root.clear()

	# Write to app log
	_log_text = '> Agent System Data report successfully generated.\n'
	GenFunctions.writeToAppLog(_app_log_file, _log_text)

	# Print debug messages
	if _debug_level == GenDefinitions._DEBUG_VERBOSE:
		print '      Agent System (' + str(_report_AS_file_name) + ') Data report generation is successful'

	# Open ftp file and write content in it
	with open("ftp/files_to_upload.txt",'a') as _ftp_file:
		_ftp_file.write(str(_report_AS_file_name)+'\n')
	_ftp_file.close()

##########

########## Create Agent Queue Data report (XML file) ##########

# Print debug message
if _debug_level == GenDefinitions._DEBUG_VERBOSE:
	print '=== Creating Agent Queue (AQ_*) Data report (XML file) ==='

# Root element for the XML tree
_xml_root = et.Element('HistPlugin')
# DataSourceNode node
_xml_data_source_node = et.SubElement(_xml_root, 'DataSourceNode')
# Vendor node
et.SubElement(_xml_data_source_node, 'Vendor').text = 'Telstra'

# XML AgentQueueNode node
_xml_agent_queue_node = et.SubElement(_xml_data_source_node, 'AgentQueueNode')
# XML - add time period for the report - format is %Y%m%dT%H%M - Note the T between %d and %H
_xml_time_period = et.SubElement(_xml_agent_queue_node, 'TimePeriod')
# Time period DateTime node in specific format
_report_date_time = datetime.datetime.fromtimestamp(int(_myargs['-d'])-300).strftime('%Y%m%dT%H%M')
et.SubElement(_xml_time_period, 'DateTime').text = _report_date_time

# This block only runs for 15-min-based intraday reports; NOT for 24-hour-based postday reports
# INTRADAY XML REPORT
if not _postday_operation:

	# Interact over each key tuple (skill ID, agent ID) recorded within one of the _report_* dictionaries and record data to XML
	for _key in _report_handled:

		# XML AgentQueueData node
		_xml_agent_queue_data = et.SubElement(_xml_agent_queue_node, 'AgentQueueData')

		# Recover skill ID and agent ID from key tuple (skill ID, agent ID)
		_report_skill_id = str(_key).split(',')[0]
		_report_agent_id = str(_key).split(',')[1]

		# XML QueueValue node, skill ID recovered from _key
		et.SubElement(_xml_agent_queue_data, 'QueueValue').text = str(_report_skill_id)

		# XML AgentValue node, employee ID recovered from agent ID recovered from _key
		et.SubElement(_xml_agent_queue_data, 'AgentValue').text = str(_agent_employee_id[str(_report_agent_id)])

		# XML Handled node, value from _report_handled, otherwise empty
		_xml_handled = et.SubElement(_xml_agent_queue_data, 'Handled')
		if _key in _report_handled:
			et.SubElement(_xml_handled, 'count').text = str(_report_handled[_key])
		else:
			et.SubElement(_xml_handled, 'count').text = ''

		# XML HandleTime node, value from _report_handle_time, otherwise empty
		_xml_handle_time = et.SubElement(_xml_agent_queue_data, 'HandleTime')
		_xml_handle_time_dur = et.SubElement(_xml_handle_time, 'duration')
		if _key in _report_handle_time:
			et.SubElement(_xml_handle_time_dur, 'totalseconds').text = str(_report_handle_time[_key])
		else:
			et.SubElement(_xml_handle_time_dur, 'totalseconds').text = ''

		# XML HoldTime node, value fixed to 0
		_xml_hold_time = et.SubElement(_xml_agent_queue_data, 'HoldTime')
		_xml_hold_time_dur = et.SubElement(_xml_hold_time, 'duration')
		et.SubElement(_xml_hold_time_dur, 'totalseconds').text = str(_report_hold_time)

		# XML WorkTime node, value fixed to 0
		_xml_work_time = et.SubElement(_xml_agent_queue_data, 'WorkTime')
		_xml_work_time_dur = et.SubElement(_xml_work_time, 'duration')
		et.SubElement(_xml_work_time_dur, 'totalseconds').text = str(_report_work_time)

		# XML RightPartyContacts node, value fixed to 0
		_xml_right_party_contacts = et.SubElement(_xml_agent_queue_data, 'RightPartyContacts')
		et.SubElement(_xml_right_party_contacts, 'count').text = str(_report_right_party_contacts)

		# XML RightPartyTalkTime node, value fixed to 0
		_xml_right_party_talk_time = et.SubElement(_xml_agent_queue_data, 'RightPartyTalkTime')
		_xml_right_party_talk_time_dur = et.SubElement(_xml_right_party_talk_time, 'duration')
		et.SubElement(_xml_right_party_talk_time_dur, 'totalseconds').text = str(_report_right_party_talk_time)

		# XML WrongPartyContacts node, value fixed to 0
		_xml_wrong_party_contacts = et.SubElement(_xml_agent_queue_data, 'WrongPartyContacts')
		et.SubElement(_xml_wrong_party_contacts, 'count').text = str(_report_wrong_party_contacts)

		# XML WrongPartyTalkTime node, value fixed to 0
		_xml_wrong_party_talk_time = et.SubElement(_xml_agent_queue_data, 'WrongPartyTalkTime')
		_xml_wrong_party_talk_time_dur = et.SubElement(_xml_wrong_party_talk_time, 'duration')
		et.SubElement(_xml_wrong_party_talk_time_dur, 'totalseconds').text = str(_report_wrong_party_talk_time)

# This block only runs for 24-hour-based postday reports; NOT for 15-min-based intraday reports
# POSTDAY XML REPORT
else:

	# Interact over each key tuple (skill ID, employee ID) recorded within one of the _report_* dictionaries and record data to XML
	for _key in _postday_aq_handled:

		# XML AgentQueueData node
		_xml_agent_queue_data = et.SubElement(_xml_agent_queue_node, 'AgentQueueData')

		# Recover skill ID and agent ID from key tuple (skill ID, agent ID)
		_report_skill_id = str(_key).split(',')[0]
		_report_employee_id = str(_key).split(',')[1]

		# XML QueueValue node, skill ID recovered from _key
		et.SubElement(_xml_agent_queue_data, 'QueueValue').text = str(_report_skill_id)

		# XML AgentValue node, employee ID recovered from agent ID recovered from _key
		et.SubElement(_xml_agent_queue_data, 'AgentValue').text = str(_report_employee_id)

		# XML Handled node, value from _report_handled, otherwise empty
		_xml_handled = et.SubElement(_xml_agent_queue_data, 'Handled')
		if _key in _postday_aq_handled:
			et.SubElement(_xml_handled, 'count').text = str(_postday_aq_handled[_key])
		else:
			et.SubElement(_xml_handled, 'count').text = ''

		# XML HandleTime node, value from _report_handle_time, otherwise empty
		_xml_handle_time = et.SubElement(_xml_agent_queue_data, 'HandleTime')
		_xml_handle_time_dur = et.SubElement(_xml_handle_time, 'duration')
		if _key in _postday_aq_handle_time:
			et.SubElement(_xml_handle_time_dur, 'totalseconds').text = str(_postday_aq_handle_time[_key])
		else:
			et.SubElement(_xml_handle_time_dur, 'totalseconds').text = ''

		# XML HoldTime node, value fixed to 0
		_xml_hold_time = et.SubElement(_xml_agent_queue_data, 'HoldTime')
		_xml_hold_time_dur = et.SubElement(_xml_hold_time, 'duration')
		et.SubElement(_xml_hold_time_dur, 'totalseconds').text = str(_report_hold_time)

		# XML WorkTime node, value fixed to 0
		_xml_work_time = et.SubElement(_xml_agent_queue_data, 'WorkTime')
		_xml_work_time_dur = et.SubElement(_xml_work_time, 'duration')
		et.SubElement(_xml_work_time_dur, 'totalseconds').text = str(_report_work_time)

		# XML RightPartyContacts node, value fixed to 0
		_xml_right_party_contacts = et.SubElement(_xml_agent_queue_data, 'RightPartyContacts')
		et.SubElement(_xml_right_party_contacts, 'count').text = str(_report_right_party_contacts)

		# XML RightPartyTalkTime node, value fixed to 0
		_xml_right_party_talk_time = et.SubElement(_xml_agent_queue_data, 'RightPartyTalkTime')
		_xml_right_party_talk_time_dur = et.SubElement(_xml_right_party_talk_time, 'duration')
		et.SubElement(_xml_right_party_talk_time_dur, 'totalseconds').text = str(_report_right_party_talk_time)

		# XML WrongPartyContacts node, value fixed to 0
		_xml_wrong_party_contacts = et.SubElement(_xml_agent_queue_data, 'WrongPartyContacts')
		et.SubElement(_xml_wrong_party_contacts, 'count').text = str(_report_wrong_party_contacts)

		# XML WrongPartyTalkTime node, value fixed to 0
		_xml_wrong_party_talk_time = et.SubElement(_xml_agent_queue_data, 'WrongPartyTalkTime')
		_xml_wrong_party_talk_time_dur = et.SubElement(_xml_wrong_party_talk_time, 'duration')
		et.SubElement(_xml_wrong_party_talk_time_dur, 'totalseconds').text = str(_report_wrong_party_talk_time)


# Record the XML tree in the report file and clear the XML tree for further use
tree = et.ElementTree(_xml_root)
tree.write(_report_AQ_file_name)
_xml_root.clear()

# Write to app log
_log_text = '> Agent Queue Data report successfully generated.\n'
GenFunctions.writeToAppLog(_app_log_file, _log_text)

# Print debug messages
if _debug_level == GenDefinitions._DEBUG_VERBOSE:
	print '      Agent Queue (' + str(_report_AQ_file_name) + ') Data report generation is successful'

# Open ftp file and write content in it
with open("ftp/files_to_upload.txt",'a') as _ftp_file:
	_ftp_file.write(str(_report_AQ_file_name)+'\n')
_ftp_file.close()

##########

########## Create Queue Data report (XML file) ##########


# Print debug message
if _debug_level == GenDefinitions._DEBUG_VERBOSE:
	print '=== Creating Queue (Q_*) Data report (XML file) ==='

# Root HistPlugin node
_xml_root = et.Element('HistPlugin')
# XML DataSourceNode node
_xml_data_source_node = et.SubElement(_xml_root, 'DataSourceNode')
# XML Vendor node
et.SubElement(_xml_data_source_node, 'Vendor').text = 'Telstra'

# XML QueueNode
_xml_queue_node = et.SubElement(_xml_data_source_node, 'QueueNode')
# XML - add time period for the report - format is %Y%m%dT%H%M - Note the T between %d and %H
_xml_time_period = et.SubElement(_xml_queue_node, 'TimePeriod')
# Time period DateTime node in specific format
_report_date_time = datetime.datetime.fromtimestamp(int(_myargs['-d'])-300).strftime('%Y%m%dT%H%M')
et.SubElement(_xml_time_period, 'DateTime').text = _report_date_time


# This block only runs for 15-min-based intraday reports; NOT for 24-hour-based postday reports
# INTRADAY XML REPORT
if not _postday_operation:

	# Interact over each skill recorded within one of the _report_* dictionaries and record data to XML
	for _skill in _report_contacts_received:

		# XML QueueData node
		_xml_queue_data = et.SubElement(_xml_queue_node, 'QueueData')

		# XML QueueValue node
		et.SubElement(_xml_queue_data, 'QueueValue').text = str(_skill)

		# XML ContactsReceived node, value from _report_contacts_received, otherwise empty
		_xml_contacts_received = et.SubElement(_xml_queue_data, 'ContactsReceived')
		if str(_skill) in _report_contacts_received:
			et.SubElement(_xml_contacts_received, 'count').text = str(_report_contacts_received[str(_skill)])
		else:
			et.SubElement(_xml_contacts_received, 'count').text = ''

		# XML AbandonedShort node, value fixed to 0
		_xml_abandoned_short = et.SubElement(_xml_queue_data, 'AbandonedShort')
		et.SubElement(_xml_abandoned_short, 'count').text = str(_report_abandoned_short)

		# XML AbandonedLong node, value from _report_abandoned_long, otherwise empty
		_xml_abandoned_long = et.SubElement(_xml_queue_data, 'AbandonedLong')
		if str(_skill) in _report_abandoned_long:
			et.SubElement(_xml_abandoned_long, 'count').text = str(_report_abandoned_long[str(_skill)])
		else:
			et.SubElement(_xml_abandoned_long, 'count').text = ''

		# XML HandledShort node, value from _report_handled_short, otherwise empty
		_xml_handled_short = et.SubElement(_xml_queue_data, 'HandledShort')
		if str(_skill) in _report_handled_short:
			et.SubElement(_xml_handled_short, 'count').text = str(_report_handled_short[str(_skill)])
		else:
			et.SubElement(_xml_handled_short, 'count').text = ''

		# XML HandledLong node, value from _report_handled_long, otherwise empty
		_xml_handled_long = et.SubElement(_xml_queue_data, 'HandledLong')
		if str(_skill) in _report_handled_long:
			et.SubElement(_xml_handled_long, 'count').text = str(_report_handled_long[str(_skill)])
		else:
			et.SubElement(_xml_handled_long, 'count').text = ''

		# XML HandleTime node, value from _report_queue_handle_time, otherwise empty
		_xml_handle_time = et.SubElement(_xml_queue_data, 'HandleTime')
		_xml_handle_time_dur = et.SubElement(_xml_handle_time, 'duration')
		if str(_skill) in _report_queue_handle_time:
			et.SubElement(_xml_handle_time_dur, 'totalseconds').text = str(_report_queue_handle_time[str(_skill)])
		else:
			et.SubElement(_xml_handle_time_dur, 'totalseconds').text = ''
		# NOTE: Below is the deprecated for of calculating handle time using data from Operational Realtime API
		#_xml_handle_time = et.SubElement(_xml_queue_data, 'HandleTime')
		#_xml_handle_time_dur = et.SubElement(_xml_handle_time, 'duration')
		#if str(_skill) in _queue_handle_time:
		#	et.SubElement(_xml_handle_time_dur, 'totalseconds').text = str(_queue_handle_time[str(_skill)])
		#else:
		#	et.SubElement(_xml_handle_time_dur, 'totalseconds').text = ''

		# XML HoldTime node, value fixed to 0
		_xml_hold_time = et.SubElement(_xml_queue_data, 'HoldTime')
		_xml_hold_time_dur = et.SubElement(_xml_hold_time, 'duration')
		et.SubElement(_xml_hold_time_dur, 'totalseconds').text = str(_report_hold_time)

		# XML WorkTime node, value fixed to 0
		_xml_work_time = et.SubElement(_xml_queue_data, 'WorkTime')
		_xml_work_time_dur = et.SubElement(_xml_work_time, 'duration')
		et.SubElement(_xml_work_time_dur, 'totalseconds').text = str(_report_work_time)

		# XML QueueDelayTime node, value from _report_queue_delay_time, otherwise empty
		_xml_queue_delay_time = et.SubElement(_xml_queue_data, 'QueueDelayTime')
		_xml_queue_delay_time_dur = et.SubElement(_xml_queue_delay_time, 'duration')
		if str(_skill) in _report_queue_delay_time:
			et.SubElement(_xml_queue_delay_time_dur, 'totalseconds').text = str(_report_queue_delay_time[str(_skill)])
		else:
			et.SubElement(_xml_queue_delay_time_dur, 'totalseconds').text = ''

		# XML SvcLvlPct node, value fixed to 0
		_xml_svc_lvl_pct = et.SubElement(_xml_queue_data, 'SvcLvlPct')
		et.SubElement(_xml_svc_lvl_pct, 'percentage').text = str(_report_svc_lvl_pct)

		# XML Backlog node, value fixed to 0
		_xml_backlog = et.SubElement(_xml_queue_data, 'Backlog')
		et.SubElement(_xml_backlog, 'count').text = str(_report_backlog)

		# XML BacklogNotExpired node, value fixed to 0
		_xml_backlog_not_expired = et.SubElement(_xml_queue_data, 'BacklogiNotExpired')
		et.SubElement(_xml_backlog_not_expired, 'count').text = str(_report_backlog_not_expired)

		# XML BacklogExpired node, value fixed to 0
		_xml_backlog_expired = et.SubElement(_xml_queue_data, 'BacklogExpired')
		et.SubElement(_xml_backlog_expired, 'count').text = str(_report_backlog_expired)

		# XML RightPartyContacts node, value fixed to 0
		_xml_right_party_contacts = et.SubElement(_xml_queue_data, 'RightPartyContacts')
		et.SubElement(_xml_right_party_contacts, 'count').text = str(_report_right_party_contacts)

		# XML RightPartyTalkTime node, value fixed to 0
		_xml_right_party_talk_time = et.SubElement(_xml_queue_data, 'RightPartyTalkTime')
		_xml_right_party_talk_time_dur = et.SubElement(_xml_right_party_talk_time, 'duration')
		et.SubElement(_xml_right_party_talk_time_dur, 'totalseconds').text = str(_report_right_party_talk_time)

		# XML WrongPartyContacts node, value fixed to 0
		_xml_wrong_party_contacts = et.SubElement(_xml_queue_data, 'WrongPartyContacts')
		et.SubElement(_xml_wrong_party_contacts, 'count').text = str(_report_wrong_party_contacts)

		# XML WrongPartyTalkTime node, value fixed to 0
		_xml_wrong_party_talk_time = et.SubElement(_xml_queue_data, 'WrongPartyTalkTime')
		_xml_wrong_party_talk_time_dur = et.SubElement(_xml_wrong_party_talk_time, 'duration')
		et.SubElement(_xml_wrong_party_talk_time_dur, 'totalseconds').text = str(_report_wrong_party_talk_time)

# This block only runs for 24-hour-based postday reports; NOT for 15-min-based intraday reports
# POSTDAY XML REPORT
else:

	# Interact over each skill recorded within one of the _report_* dictionaries and record data to XML
	for _skill in _postday_q_contacts_received:

		# XML QueueData node
		_xml_queue_data = et.SubElement(_xml_queue_node, 'QueueData')

		# XML QueueValue node
		et.SubElement(_xml_queue_data, 'QueueValue').text = str(_skill)

		# XML ContactsReceived node, value from _report_contacts_received, otherwise empty
		_xml_contacts_received = et.SubElement(_xml_queue_data, 'ContactsReceived')
		if str(_skill) in _postday_q_contacts_received:
			et.SubElement(_xml_contacts_received, 'count').text = str(_postday_q_contacts_received[str(_skill)])
		else:
			et.SubElement(_xml_contacts_received, 'count').text = ''

		# XML AbandonedShort node, value fixed to 0
		_xml_abandoned_short = et.SubElement(_xml_queue_data, 'AbandonedShort')
		et.SubElement(_xml_abandoned_short, 'count').text = str(_report_abandoned_short)

		# XML AbandonedLong node, value from _report_abandoned_long, otherwise empty
		_xml_abandoned_long = et.SubElement(_xml_queue_data, 'AbandonedLong')
		if str(_skill) in _postday_q_abandoned_long:
			et.SubElement(_xml_abandoned_long, 'count').text = str(_postday_q_abandoned_long[str(_skill)])
		else:
			et.SubElement(_xml_abandoned_long, 'count').text = ''

		# XML HandledShort node, value from _report_handled_short, otherwise empty
		_xml_handled_short = et.SubElement(_xml_queue_data, 'HandledShort')
		if str(_skill) in _postday_q_handled_short:
			et.SubElement(_xml_handled_short, 'count').text = str(_postday_q_handled_short[str(_skill)])
		else:
			et.SubElement(_xml_handled_short, 'count').text = ''

		# XML HandledLong node, value from _report_handled_long, otherwise empty
		_xml_handled_long = et.SubElement(_xml_queue_data, 'HandledLong')
		if str(_skill) in _postday_q_handled_long:
			et.SubElement(_xml_handled_long, 'count').text = str(_postday_q_handled_long[str(_skill)])
		else:
			et.SubElement(_xml_handled_long, 'count').text = ''

		# XML HandleTime node, value from _report_queue_handle_time, otherwise empty
		_xml_handle_time = et.SubElement(_xml_queue_data, 'HandleTime')
		_xml_handle_time_dur = et.SubElement(_xml_handle_time, 'duration')
		if str(_skill) in _postday_q_handle_time:
			et.SubElement(_xml_handle_time_dur, 'totalseconds').text = str(_postday_q_handle_time[str(_skill)])
		else:
			et.SubElement(_xml_handle_time_dur, 'totalseconds').text = ''
		# NOTE: Below is the deprecated for of calculating handle time using data from Operational Realtime API
		#_xml_handle_time = et.SubElement(_xml_queue_data, 'HandleTime')
		#_xml_handle_time_dur = et.SubElement(_xml_handle_time, 'duration')
		#if str(_skill) in _queue_handle_time:
		#	et.SubElement(_xml_handle_time_dur, 'totalseconds').text = str(_queue_handle_time[str(_skill)])
		#else:
		#	et.SubElement(_xml_handle_time_dur, 'totalseconds').text = ''

		# XML HoldTime node, value fixed to 0
		_xml_hold_time = et.SubElement(_xml_queue_data, 'HoldTime')
		_xml_hold_time_dur = et.SubElement(_xml_hold_time, 'duration')
		et.SubElement(_xml_hold_time_dur, 'totalseconds').text = str(_report_hold_time)

		# XML WorkTime node, value fixed to 0
		_xml_work_time = et.SubElement(_xml_queue_data, 'WorkTime')
		_xml_work_time_dur = et.SubElement(_xml_work_time, 'duration')
		et.SubElement(_xml_work_time_dur, 'totalseconds').text = str(_report_work_time)

		# XML QueueDelayTime node, value from _report_queue_delay_time, otherwise empty
		_xml_queue_delay_time = et.SubElement(_xml_queue_data, 'QueueDelayTime')
		_xml_queue_delay_time_dur = et.SubElement(_xml_queue_delay_time, 'duration')
		if str(_skill) in _postday_q_queue_delay_time:
			et.SubElement(_xml_queue_delay_time_dur, 'totalseconds').text = str(_postday_q_queue_delay_time[str(_skill)])
		else:
			et.SubElement(_xml_queue_delay_time_dur, 'totalseconds').text = ''

		# XML SvcLvlPct node, value fixed to 0
		_xml_svc_lvl_pct = et.SubElement(_xml_queue_data, 'SvcLvlPct')
		et.SubElement(_xml_svc_lvl_pct, 'percentage').text = str(_report_svc_lvl_pct)

		# XML Backlog node, value fixed to 0
		_xml_backlog = et.SubElement(_xml_queue_data, 'Backlog')
		et.SubElement(_xml_backlog, 'count').text = str(_report_backlog)

		# XML BacklogNotExpired node, value fixed to 0
		_xml_backlog_not_expired = et.SubElement(_xml_queue_data, 'BacklogiNotExpired')
		et.SubElement(_xml_backlog_not_expired, 'count').text = str(_report_backlog_not_expired)

		# XML BacklogExpired node, value fixed to 0
		_xml_backlog_expired = et.SubElement(_xml_queue_data, 'BacklogExpired')
		et.SubElement(_xml_backlog_expired, 'count').text = str(_report_backlog_expired)

		# XML RightPartyContacts node, value fixed to 0
		_xml_right_party_contacts = et.SubElement(_xml_queue_data, 'RightPartyContacts')
		et.SubElement(_xml_right_party_contacts, 'count').text = str(_report_right_party_contacts)

		# XML RightPartyTalkTime node, value fixed to 0
		_xml_right_party_talk_time = et.SubElement(_xml_queue_data, 'RightPartyTalkTime')
		_xml_right_party_talk_time_dur = et.SubElement(_xml_right_party_talk_time, 'duration')
		et.SubElement(_xml_right_party_talk_time_dur, 'totalseconds').text = str(_report_right_party_talk_time)

		# XML WrongPartyContacts node, value fixed to 0
		_xml_wrong_party_contacts = et.SubElement(_xml_queue_data, 'WrongPartyContacts')
		et.SubElement(_xml_wrong_party_contacts, 'count').text = str(_report_wrong_party_contacts)

		# XML WrongPartyTalkTime node, value fixed to 0
		_xml_wrong_party_talk_time = et.SubElement(_xml_queue_data, 'WrongPartyTalkTime')
		_xml_wrong_party_talk_time_dur = et.SubElement(_xml_wrong_party_talk_time, 'duration')
		et.SubElement(_xml_wrong_party_talk_time_dur, 'totalseconds').text = str(_report_wrong_party_talk_time)


# Record the XML tree in the report file and clear the XML tree for further use
tree = et.ElementTree(_xml_root)
tree.write(_report_Q_file_name)
_xml_root.clear()

# Write to app log
_log_text = '> Queue Data report successfully generated.\n'
GenFunctions.writeToAppLog(_app_log_file, _log_text)

# Print debug messages
if _debug_level == GenDefinitions._DEBUG_VERBOSE:
  print '      Queue (' + str(_report_Q_file_name) + ') Data report generation is successful'

# Open ftp file and write content in it
with open("ftp/files_to_upload.txt",'a') as _ftp_file:
	_ftp_file.write(str(_report_Q_file_name)+'\n')
_ftp_file.close()

##########


########## Update agent-to-employee map file ##########

# This block only runs for 15-min-based intraday reports; NOT for 24-hour-based postday reports
if not _postday_operation:

	# Print debug message
	if _debug_level == GenDefinitions._DEBUG_VERBOSE:
		print '=== Saving the agent to employee ID mapping ==='

	# If there are new employee IDs in the mapping of agent and employee IDs, save it to the file
	if len(_agent_employee_id) > _agent_employee_id_file_size:

		# Print debug message
		if _debug_level == GenDefinitions._DEBUG_VERBOSE:
			print '      Saving %d new employee IDs' % (len(_agent_employee_id) - _agent_employee_id_file_size)

		# Remove current file
		if os.path.exists(_myargs['-e']):
			os.remove(_myargs['-e'])

		# Go through dictionary and store information in _output_file
		_output_file = open(_myargs['-e'], 'w')
		for _agent in _agent_employee_id:
			_output_file.write('%s,%s\n' % (str(_agent), str(_agent_employee_id[_agent])))

		_output_file.close()

	# There are no new employee IDs to save
	else:
		if _debug_level == GenDefinitions._DEBUG_VERBOSE:
			print '      Nothing to add'

# Open ftp file and write end of script run
_now = str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))

with open("ftp/files_to_upload.txt",'a') as _ftp_file:
    _ftp_file.write("--- End of Script Run" + _now + " - --\n")
_ftp_file.close()

##########

# Just add an extra space at the end of script running
print











