#!/usr/bin/env python
#
# Author:
#   Ricardo de O. Schmidt
#     NAVOMI.com Cloud Solutions
#
# Date:
#   February 14, 2018
#
# Contact:
#   ricardo.schmidt@navomi.com
#
# Description:
#   This is a Python script called from the concurrency_factor.sh to perform the
#   task the user has requested via CLI.
#


import sys
import os


_OPT_EXIT	= 0
_OPT_SET	= 1
_OPT_GET	= 2
_OPT_DROP	= 3

# Function to parse arguments, returning a dictionary called args
def parseArgs(_argv):
    # Start an empty dictionary to store arguments
    _args = {}
    # Iterate through all arguments, removing each of them once processed
    while _argv:
        # Check if this is a "name value"
        if _argv[0][0] == '-':
            # Save the "name value" as index and its respective argument value
            _args[_argv[0]] = _argv[1]
        # Remove the first element of the argument array
        _argv = _argv[1:]
    # Return the dictionary
    return _args

# Function to get the current value associated to a given skill ID
def GetConcurrencyFactor(_skill, _concurrency_factor):
	if _skill in _concurrency_factor:
		return _concurrency_factor[_skill]
	else:
		return '-1'

def SetConcurrencyFactor(_skill, _concurrency_factor, _value):
	_concurrency_factor[_skill] = _value
	return

def DropConcurrencyFactor(_skill, _concurrency_factor):
	if _skill not in _concurrency_factor:
		return '-1'
	else:
		_value = _concurrency_factor[_skill]
		_concurrency_factor.pop(_skill)
		return _value


# Function to verify if the informed skill ID already exists
def FindConcurrencyFactor(_skill, _values):
	if str(_skill) not in _values:
		return False
	else:
		return True


def LoadConcurrencyFactor(_file):
	_values = {}
	with open(_file, 'r') as _concurrency_file:
		for _line in _concurrency_file:
			if len(_line) > 0:
				_values[str(_line.split(',')[0])] = str(_line.split(',')[1]).rstrip()
	return _values



# Function to print menu of options
def PrintOptions(_msg):
	os.system('clear')
	if _msg != '':
		print
		print _msg
		print
	print
	print ' (%d) Save and exit this program' % _OPT_EXIT
	print ' (%d) SET a new value to a given Skill ID' % _OPT_SET
	print ' (%d) GET the current factor value for a given Skill ID' % _OPT_GET
	print ' (%d) DROP the current factor value for a given Skill ID' % _OPT_DROP
	print
	return




##### START OF SCRIPT #####



# Parse arguments
_myargs = {}
_myargs = parseArgs(sys.argv)

# Load the concurrency factor file informed in the arguments
print "Loading current concurrency factor file..."
_concurrency_factor = {}
_concurrency_factor = LoadConcurrencyFactor(str(_myargs['-file']))
print "... %d values loaded" % len(_concurrency_factor)


# Start while loop to interact with user
_msg = 'Welcome to the concurrency factor CLI program'
while True:

	# Read option from the user
	try:
		PrintOptions(_msg)
		_option = int(raw_input('Enter a valid option: '))
	except ValueError:
		_msg = 'Not a valid option.'

	# EXIT option
	if _option == _OPT_EXIT:
		# Just break from the while
		break

	# GET option
	elif _option == _OPT_GET:
		# Read the skill ID
		_skill = raw_input('Inform the Skill ID: ')
		if _skill == '':
			_msg = 'Not a valid option'

		# Get the concurrency factor for the informed skill
		_factor = GetConcurrencyFactor(_skill, _concurrency_factor)

		if _factor == '-1':
			_msg = 'Skill ID %s does not have a concurrency factor associated to it' % str(_skill)
		else:
			_msg = 'The current factor for skill %s is %s' % (str(_skill), str(_factor))

	# DROP option
	elif _option == _OPT_DROP:
		# Read the skill ID
		_skill = raw_input('Inform the Skill ID: ')
		if _skill == '':
			_msg = 'Not a valid option'

		# Get the concurrency factor for the informed skill
		_factor = DropConcurrencyFactor(_skill, _concurrency_factor)

		if _factor == '-1':
			_msg = 'Skill ID %s does not have a concurrency factor associated to it\nNothing to drop' % str(_skill)
		else:
			_msg = 'The current factor %s for skill %s has been dropped' % (str(_factor), str(_skill))

	# SET option
	elif _option == _OPT_SET:
		# Read the skill ID
		_skill = raw_input('Inform the Skill ID: ')
		if _skill == '':
			_msg = 'Not a valid option'

		# Read the concurrency factor value
		try:
			_value = float(raw_input('Enter a value for the concurrency factor: '))
			_good = True
		except ValueError:
			_msg = 'Not a valid value (use . as decimal separator)'


		if _good:

			# Get the concurrency factor for the informed skill
			_factor = GetConcurrencyFactor(_skill, _concurrency_factor)

			if _factor == '-1':
				_msg = 'There is no concurrency factor associated to skill ID %s\nAdding a new one with value %s' % (str(_skill), str(_value))
			else:
				_msg = 'The current factor %s for skill %s has been changed to %s' % (str(_factor), str(_skill), str(_value))

			SetConcurrencyFactor(_skill, _concurrency_factor, _value)

	else:
		_msg = 'Not a valid option'


os.system('clear')
print
print 'Saving changes'
print 'Recording %d concurrency factor values' % len(_concurrency_factor)
_file = open(_myargs['-file'], 'w')
for _item in _concurrency_factor:
	_file.write('%s,%s\n' % (str(_item), str(_concurrency_factor[_item])))
_file.close()

print
print 'Exiting... bye!'
print











