#!/bin/bash
#
# August 30, 2017
#
# Ricardo de O. Schmidt
#   ricardo.schmidt@navomi.com
#
# Description:
#   This bash is the manager agent for the 15-minute report generation for Telstra.
#   The tasks it performs every time it is called are:
#     1. Calls 15_minute_reports.py that fetches data from Life Person API and generates XML reports.
#     2. Uploads the generates reports to the specified FTP server.
#     3. Logs in any error into the pertinent log file.
#     4. Attempts to solve previously logged errors from pertinent log files.
#
# Dependencies:
#   For dependencies, please check the README file that comes with this software package.
#
# Configuration file:
#   Please check config.navomi
#
# Calling this script:
#   By definition of the Telstra project, this script is to be called every 15
#   minutes from a scheduled cron job. The exact time is full hour H:00, H:15,
#   H:30 and H:45. The result XML report files are to be uploaded within 5 min
#   of the script call, at H:05, H:20, H:35 and H:50 respectively.
#


###
### FIRST THING IS TO GET TIMESTAMP
###

_current_timestamp=`date +%s`;


###
### ARGUMENTS PARSING
###

# First check if they are there
if [ $# -ne 3 ]; then
  echo "*** ERROR: arguments missing.";
  echo "";
  echo "    Usage:";
  echo "    ./rt_loop.sh <CONF> <OS> <DEBUG>";
  echo "      <CONF>  path/to/config.file";
  echo "      <OS>    OS type (1:Mac OS, 2:Linux)";
  echo "      <DEBUG> Debug level (0:None, 1:Basic, 2:Verbose)";
  echo "";
  echo "    Arguments have to follow this specific order.";
  echo "    Check README file for more information.";
  echo "    Exiting...";
  exit;
fi;

# Check configuration file
if [ ! -f $1 ]; then
  echo "*** ERROR: cannot open configuration file.";
  echo "    $1 not found.";
  echo "    Exiting...";
  exit;
else
  # Loads configuration
  source $1;
fi;

# Check OS type
if [ $2 -eq 1 ]; then # 1 for Mac OS
  _os="MacOS";
elif [ $2 -eq 2 ]; then # 2 for Linux
  _os="Linux";
else
  echo "*** ERROR: invalid OS option.";
  echo "    Use 1:Mac OS or 2:Linux";
  echo "    Exiting...";
  exit;
fi;

# Check Debug level option
if [ $3 -eq 0 ]; then # 0 for none
  _debug_level=0
elif [ $3 -eq 1 ]; then # 1 for basic
  _debug_level=1
elif [ $3 -eq 2 ]; then # 2 for verbose
  _debug_level=2
else
  echo "*** ERROR: invalid debug option.";
  echo "    Use 0:None, 1:Basic or 2:Verbose";
  echo "    Exiting...";
  exit;
fi;


###
### VERIFY INFORMATION IN CONFIG.NAVOMI
###

# Debug
if [ $_debug_level -eq 2 ]; then
  echo ". Verifying and validating configuration file ..."
fi;

# Check if config file exists
if [ ! -f $_config_file_path ]; then
  echo "*** ERROR: configuration file cannot be found.";
  echo "    Informed path is: $_config_file_path";
  echo "    Exiting...";
  exit;
else
  if [ $_debug_level -ge 1 ]; then
    echo ".. configuration file found..."
  fi;
fi;

# Check if socket file exists
if (echo $_db_socket | egrep '[^0-9]' &> /dev/null) then # if variable is not numeric
    if [ ! -e $_db_socket ]; then
        echo "*** ERROR: socket file cannot be found.";
        echo "    Informed path is: $_db_socket";
        echo "    Exiting...";
        exit;
    else
        if [ $_debug_level -ge 1 ]; then
            echo ".. socket file found..."
        fi;
    fi;
else
    if [ $_db_socket -ne 0 ]; then
        echo "*** ERROR: socket file cannot be a number not equal 0.";
        echo "    Informed path is: $_db_socket";
        echo "    Exiting...";
        exit;
    fi;
fi;

# Check whether mandatory LP API access credentials are informed in the config file
if [ -z $_lp_api_account ] ||
   [ -z $_lp_api_key ] || \
   [ -z $_lp_api_secret ] || \
   [ -z $_lp_api_token ] || \
   [ -z $_lp_api_token_secret ] || \
   [ -z $_lp_api_signature_method ]; then
  echo "*** ERROR: missing access credentials for the Live Person API.";
  echo "    Please inform these in the configuration file: $_config_file_path";
  echo "    Exiting...";
else
  if [ $_debug_level -eq 2 ]; then
    echo ".. access credentials for LP API found in the config file...";
  fi;
fi;

# Check whether non-mandatory fields were informed and load the default if needed

# Path to store the log files
if [ -z $_output_path_log ]; then
  _output_path_log="./logs/";
  if [ $_debug_level -eq 2 ]; then
    echo ".. output path for log files not informed, set to default...";
  fi;
fi;
# Create folder for logs if it doesn't exist
if [ ! -d $_output_path_log ]; then
  mkdir $_output_path_log;
  if [ $_debug_level -eq 2 ]; then
    echo ".. output folder for log files created...";
  fi;
fi;

# Input file containing the agent-to-employee IDs mapping
if [ -z $_input_agent_employee ]; then
  _input_agent_employee="./agent_employee_map.csv";
  if [ $_debug_level -eq 2 ]; then
    echo ".. input file for agent-to-employee mapping not informed, set to default...";
  fi;
fi;
# Create input file if it doesn't exist
if [ ! -f $_input_agent_employee ]; then
  touch $_input_agent_employee;
  if [ $_debug_level -eq 2 ]; then
    echo ".. input file for agent-to-employee $_input_agent_employee created...";
  fi;
fi;

# API arguments
if [ -z $_api_arg_timeframe ]; then _api_arg_timeframe="5"; fi; # Default to 15 minutes, which is the minimum value possible
if [ -z $_api_arg_interval ]; then _api_arg_interval="5"; fi; # Default to 5 minutes, which is the minimum value possible
if [ -z $_api_arg_version ]; then _api_arg_version="1"; fi; # Default to version 1
if [ -z $_api_arg_agent_ids ]; then _api_arg_agent_ids="all"; fi; # Default set to all, that is no specific agent
if [ $_debug_level -eq 2 ]; then
  echo ".. API arguments set to default (if not given in the config file)...";
fi;

if [ $_debug_level -ge 1 ]; then
  echo "... configuration file check done!";
fi;



###
### ENTERS THE LOOP ZONE, NO GOING BACK FROM HERE...
###

# Fix timeframe and interval values for request in 5 minutes each
_api_arg_timeframe=5;
_api_arg_interval=5;

# TODO - this info should come from database
_agent_state_file="agent-state.csv";

# Credentials to access the LivePerson are retrieved from config.navomi inside the Python script
# Arguments specification is given in the beginning of iex_rta.py script

# Enters the infinite loop to run call the iex_rta.py script every 10 seconds
while true; do

  # Calculate the request time, rounded to multiple of 10
  # Round request time to multiple of 10 in seconds
  _request_ts=`date +%s`;
  while [ $((_request_ts%10)) != 0 ]; do
    let _request_ts=$_request_ts-1;
  done;

  if [ $_os == "MacOS" ]; then
    _request_time=`date -r $_request_ts +%m%d%y.%H%M%S`;
  elif [ $_os == "Linux" ]; then
    _request_time=`date -d @$_request_ts +%m%d%y.%H%M%S`;
  fi;

  # source proxies.txt
  exec python2.7 iex_rta.py \
            -a $_lp_api_account \
            -k $_lp_api_key \
            -s $_lp_api_secret \
            -t $_lp_api_token \
            -T $_lp_api_token_secret \
            -m $_lp_api_signature_method \
            -o $_agent_state_file \
            -l $_output_path_log \
            -f $_api_arg_timeframe \
            -i $_api_arg_interval \
            -v $_api_arg_version \
            -A $_api_arg_agent_ids \
            -e $_input_agent_employee \
            -d $_request_time \
            -ts $_request_ts \
            -V $_debug_level \
            -dn $_db_name \
            -dp $_db_port \
            -du $_db_user \
            -dw $_db_pw \
            -socket $_db_socket &

  sleep 10

done;


###
### ATTEMPT TO FIX PREVIOUSLY LOGGED ERRORS
###


# TODO - error log and resolution















