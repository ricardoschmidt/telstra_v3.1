#/usr/bin/env python
#
# Ricardo de O. Schmidt
#   September 1, 2017
#
# Description:
#   Connect to FTP server, check/create folder structure, and upload report file.
#
# Arguments (all of them are mandatory):
#   -u : FTP user
#   -s : FTP password
#   -h : FTP host
#   -p : FTP port
#   -o : Path to reports to be uploaded
#   -l : Path to log files
#

import ftplib
import socket
import pathlib
import sys
import time


###
### Macros
###

# IMPORTANT: watch for consistency with file extension defined in 15_min_reports.py
_M_FILE_EXT = ".rep" # TODO - this has to be changed to XML later


###
### Function to parse arguments, returning a dictionary called args
def parseArgs(argv):
  args = {} # Start an empty dictionary to store arguments
  while argv: # Iterate through all arguments, removing each of them once processed
    if argv[0][0] == '-': # This is a "name value"
      args[argv[0]] = argv[1] # Save the "name value" as index and its respec arg value
    argv = argv[1:] # Remove the first element of the arg array
  return args # Return the dictionary


###
### START OF THE ACTUAL SCRIPT
###


# Call parseArgs and store arguments in the dictionary _myargs
_myargs = {}
_myargs = parseArgs(sys.argv)

###
### Check if the current files are ready to be sent
###

_to_upload = 3
_base_report_file_name = _myargs['-o'] + time.strftime('%Y%m%d%H%M')

_report_agent_system_data = pathlib.Path(_base_report_file_name + "-agentSystemData" + _M_FILE_EXT)
if not _report_agent_system_data.is_file():
  print "** Issue: report agentSystemData cannot be found for current time."
  _to_upload -= 1
  # TODO - add to error log

_report_agent_queue_data = pathlib.Path(_base_report_file_name + "-agentQueueData" + _M_FILE_EXT)
if not _report_agent_queue_data.is_file():
  print "** Issue: report agentQueueData cannot be found for current time."
  _to_upload -= 1
  # TODO - add to error log

_report_queue_data = pathlib.Path(_base_report_file_name + "-queueData" + _M_FILE_EXT)
if not _report_queue_data.is_file():
  print "** Issue: report queueData cannot be found for current time."
  _to_upload -= 1
  # TODO - add to error log

if _to_upload == 0:
  print "** Issue: no report found to be uploaded at the current time."
  #sys.exit(0)
  # TODO - remove comment from line above


###
### At least one report to be uploaded... proceed to FTP connection and upload
###

# Base folder structure to upload current reports
_ftp_base_folder = time.strftime('%Y') + "/" + time.strftime("%m") + "/" + time.strftime("%d") + "/"

# Try to connect with the FTP server and send the files
_retry_connect = True   # Control the number of retries
while _retry_connect:
  try:
    _ftp = ftplib.FTP(_myargs['-h'])          # Specify the FTP host to connect, with port number for now
    _ftp.login(_myargs['-u'], _myargs['-s'])  # Login information

    _ftp.cwd(_ftp_base_folder)  # Move to the base folder


    # TODO - modify the next two lines to upload the actual report files
    _test_file = "testFile.txt"
    _ftp.storlines("STOR " + _test_file, open(_test_file, 'r'))

  except ftplib.error_perm as _e:
    _retry_connect = False
    print "*** ERROR: FTP error."
    print "           " + str(_e)
    print "           Exiting..."

  else:
    print "Files uploaded with success!"
    _retry_connect = False

