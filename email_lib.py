#!/usr/bin/env python
#
# Author:
#   Ricardo de O. Schmidt
#     NAVOMI.com Cloud Solutions
#
# Date:
#   February 14, 2018
#
# Contact:
#   ricardo.schmidt@navomi.com
#
# Description:
#   This is a Python library script with a collection of functions to send
#   emails using the smtplib library.
#


import json
import sys
import smtplib

# General definitions of macros
import general_definitions as GeneralDef


##
## Macros

_debug_level = GeneralDef._DEBUG_VERBOSE


##
## Function to send a notification email in case of exceptions or errors
##  Argument _content_json contains the information about the email message to be sent:
##      {
##          'server': 'smtp.server.address',
##          'port': 80,
##          'pw': 'P4ssw0rd'
##          'from': 'source@address.com'
##          'to': 'destination@address.com'
##          'subject': '[TAG] Subject line with tags'
##          'message': 'Text message for the email body'
##      }
##  Return: boolen value indicating success (true) or failure on sending the message (false)
def sendNotification(_content_json):
    if _debug_level == GeneralDef._DEBUG_VERBOSE:
        print "*** Sending notification email\nTo: " + _content_json['to'] + "\nSubject: " + _content_json['subject'] + "\nMessage:\n" + _content_json['message']
        print "***"

    _server = smtplib.SMTP(_content_json['server'], _content_json['port'])
    #_server.starttls()
    #_server.login(_content_json['from'], _content_json['pw'])
    
    # Set smtplib debug level to null (0) for now
    _server.set_debuglevel(0)
    
    _msg = ("From: %s\r\nTo: %s\r\nSubject: %s\r\n\r\n" % (_content_json['from'], _content_json['to'], _content_json['subject']))
    _msg = _msg + _content_json['message']
    
    _server.sendmail(_content_json['from'], _content_json['to'].split(","), _msg)
    
    _server.quit()
    
    return







