# WFM-NICE-IEX Integration for Tesltra -- version 2 #

## Description

This is the repository for the Telstra WFM-IEX-Integration project.

## Files

This project consists of the following Python and Bash scripts:

* __email_lib.py__ contains a library of email-related functions.

* __le_api_lib.py__ contains a library of functions used to interact with LivePerson's APIs.

* __general_definitions.py__ contains a library of general definitions used across scripts.

* __general_functions.py__ contains a library of general functions used across scripts.

* __email_reports.py__ contains the code needed to email intraday and postday reports.

* __generate_reports.py__ contains the main code for generating the 15-minute historical intraday/postday data reports, using statistic data collected through LivePerson's Operation Realtime API and Engagement History API.

* __agent_employee_id.csv__ contains the known mapping between agent and employee IDs.

* __loop.sh__ contains the main call of 15-min historic intraday data reports. This script should run in a loop scheduled using Cron job(s).

* __rerun.sh__ contains the main call of 15-min historic intraday data reports, when reports are to be generated manually for a predefined period of time.

* __concurrency_factor.csv__ contains the list of skills (queues) and their respective concurrency factor to be used for corrections to the reported interaction duration.

* __concurrency_factor.py__ contains the code for the CLI used to manipulate data inside the __concurrency_factor.csv__ file.

* __postday.sh__ contains the main call of 15-min historic data reports, when reports are to be regenerated for a period of 24-hours (postday reports).

* __config.navomi__ contains all the configurable information and fields needed for the proper functioning of all the systems above described. Without this file and some of the information within, some of the scripts above described will not be able to run.

* __dls.sh__ contains the script to control if reports was generated in correect way in DLS starts day and DLS ends day.

## Configuring

### Dependencies

These dependencies have to be fixed so that the scripts listed above run properly.

#### Python version

The adaptor is implemented to be run by Python 2.7.10. Using different version of Python than this one might cause unexpected behavior.

#### Python package "requests"

The __requests__ package is used to send HTTP requests to a given API. This package must be installed within Python modules.  
To install: **\# sudo pip install requests**

#### Python package "requests_oauthlib"

The __requests_oauthlib__ package is used for authentication within APIs. This package must be installed within Python modules.  
To install: **\# sudo pip install requests_oauthlib**

#### Python package "datetime"

The __datetime__ package is used for date/time calculations and must be installed within Python modules.  
To install: **\# sudo pip install datetime**

#### Installing Python packages

The guide on package installation above uses __pip__. To install __pip__, you first have to fetch the __get-pip.py__ file from **https://bootstrap.pypa.io/get-pip.py** and then run the command: **\# sudo python get-pip.py**

## Report Conventions

Historical intraday and postday data reports are generated for 15-min intervals. The date that goes into the report filename indicates the start of the 15-min interval the report contains.

Reports naming follow NICE WFM Data Connector specification:  
**XX\_MMDDYY.hhmm**.xml

Where:
* __XX__: Letter combination identifying the *TYPE* of report. Possible values are  
          *AS*: Agent System Data, *AQ*: Agent Queue Data, and *Q*: Queue Data  
* __MM__: Two numeric digits representing the *MONTH* of the report interval  
* __DD__: Two numeric digits representing the *DAY* of the report interval  
* __YY__: Two numeric digits representing the last two digits of the *YEAR* of the  
      report interval  
* __hh__: Two numeric digits representing the 24h-clock *HOUR* of the report interval  
* __mm__: Two numeric digits representing the *MINUTES* of the report interval  

### Report files delivered by email

If report files are chosen to be delivered by email, these will be sent once per day for intraday reports, and once per day for postday reports.  

For the intraday reports, a zip file containing 288 xml data files will be emailed. This zip file is named as follows: **MMDDYY**.zip (following the letter definitions above).  

For the postday reports, a zip file containing 288 xml data files will be emailed. This zip file is named as follows: **MMDDYY**\_refreshed.zip (following the letter definitions above).

### Report Interval

When requested for a report, the script will generate the report for the
15-minute interval immediately before the calling time. Save the case of
rerun.sh, which runs for intervals specified in the arguments of the script
command call.

The hour is strictly divided into four periods of 15 minutes each, which are
used to identify the report to be generated and also to define which time
intervals of data collected through LivePerson API to process. The table below
gives the relastionship between report time and data intervals. (Request time
indicates the period in which a report can be requested.)

Report time | Request time | Report interval      | Operational Realtime API 5-min buckets | Engagement History API interval | Report name
------------|--------------|----------------------|----------------------------------------|---------------------------------|--------------------
hh:00       | (hh+1):01:00 | hh:00:00 -- hh:14:59 | hh:05:00, hh:10:00, hh:15:00           | hh:00:00 -- hh:14:59            | XX\_MMDDYY.hh00.xml
hh:15       | (hh+1):16:00 | hh:15:00 -- hh:29:59 | hh:20:00, hh:25:00, hh:30:00           | hh:15:00 -- hh:29:59            | XX\_MMDDYY.hh15.xml
hh:30       | (hh+1):31:00 | hh:30:00 -- hh:44:59 | hh:35:00, hh:40:00, hh:45:00           | hh:30:00 -- hh:44:59            | XX\_MMDDYY.hh30.xml
hh:45       | (hh+1):46:00 | hh:45:00 -- hh:59:59 | hh:50:00, hh:55:00, (hh+1):00:00       | hh:45:00 -- hh:59:59            | XX\_MMDDYY.hh45.xml

### Report Content

The report content and XML structure follow the specifications within the document *NICE WFM Data Connector -- Historical Data Interface Design Specification* (from 12/11/14, document version 1.10). For specifics about the structure of the XML files, please take a look at:
* __Section 7.3__ for Queue Data report
* __Section 7.4__ for Agent Queue Data report
* __Section 7.5__ for Agent System Data report

## Running Scripts

The generation of 15-min intraday data reports is called from __loop.sh__, which must be scheduled as a Cron job to be run every 15 minutes, as defined by the table above. Intraday reports should not be generated manually by occasionally calling __loop.sh__. For postday reports, the same observations apply, and reports are generated by __postday.sh__ script.

### Running/scheduling intraday reports generation

For scheduling __loop.sh__ the following line arguments are expected:  
**\# ./loop.sh [CONFIG FILE] [FTP OPT] [OS TYPE] [DEBUG OPT]**  

Where:
* CONFIG FILE is the path/to/config.file (default configuration file is __config.navomi__)
* FTP OPT is the FTP option, possible values are 1:Push and 2:Pull
* OS TYPE is the Operational System, possible values are 1:MacOS and 2:Linux
* DEBUG OPT is the debugging level, possible values are 0:None, 1:Basic and 2:Verbose

Examples:  
**\# ./loop.sh config.navomi 2 1 0**  
To use configuration file __config.navomi__, reports will be stored in the folder specified in the configuration file to be retrieved via FTP pull, running in a Mac OS environment, and without debugging messages.  

**\# ./loop.sh config.navomi 1 2 2**  
To use configuration file __config.navomi__, reports will be sent via FTP using destination host and credentials specified in the configuration file, running in a Linux environment, and with verbose debugging.

### Running/scheduling postday reports generation

For scheduling __postday.sh__ the following line arguments are expected:  
**\# ./loop.sh [CONFIG FILE] [FTP OPT] [OS TYPE] [DEBUG OPT] [OUT PATH] <XML FILES>**  

Where:
* CONFIG FILE is the path/to/config.file (default configuration file is __config.navomi__)
* FTP OPT is the FTP option, possible values are 1:Push and 2:Pull
* OS TYPE is the Operational System, possible values are 1:MacOS and 2:Linux
* DEBUG OPT is the debugging level, possible values are 0:None, 1:Basic and 2:Verbose
* OUT PATH indicates the folder where postday reports will be saved
* XML FILES indicates the location where intraday reports XML files are found

### Running rerun.sh

For running __rerun.sh__ the following line arguments are expected:  
**\# ./rerun.sh [CONFIG FILE] [FTP OPT] [OS TYPE] [DEBUG OPT] [OUTPUT PATH] [START TIME] [END TIME]**  

Where:  
* CONFIG FILE is the path/to/config.file (default configuration file is __config.navomi__)
* FTP OPT is the FTP option, possible values are 1:Push and 2:Pull
* OS TYPE is the Operational System, possible values are 1:MacOS and 2:Linux
* DEBUG OPT is the debugging level, possible values are 0:None, 1:Basic and 2:Verbose
* OUTPUT PATH is the output folder to store reports to be generated (it has to
  be different than the one specified in the configuration file)
* START TIME is the start time for the report generation, i.e. the time for the
  first report to be generated. It has to be less than 24 hours back in time,
  and has to follow the format __MMDDYY.hhmm__ (UTC)
* END TIME is the end time for the report generation, i.e. the time for the last
  report to be generated. If only one report is to be generated, end time has to
  be equal than start time. It has to be less than 24 hours back in time, and
  has to follow the format __MMDDYY.hhmm__ (UTC)  

Example:  
**\# ./rerun.sh config.navomi 1 1 1 old-reports/ 180917.1000 180917.1045**  
To use configuration file __config.navomi__, reports will be copied to the FTP
folder given in the configuration file to be retrieved via FTP pull, running in
a Mac OS environment, debugging messages set to basic, reports to be stored
locally in the folder __old-reports/__. In this run, 4 reports will be
generated, one for each quarter starting on 10AM o'clock of Sep 18th, 2017
(check table above for report naming).

## Configuration File

The configuration file defines many information and fields to be used in the report generation process. Some of these are mandatory and report generation will not be concluded if such information is not defined within the configuration file. For the others, in the absence of definitions, a default value is used. For more details on information and fields refer to the configuration file itself.
