#!/usr/bin/env python
#
# Author:
#   Ricardo de Oliveira Schmidt
#     NAVOMI.com Cloud Solutions
#
# Date:
#   December 4, 2017
#
# Contact:
#   ricardo.schmidt@navomi.com
#
# Description:
#   This is a Python script that is meant to be called from loop.sh
#   This script sends all the reports generated within a day to one
#   or multiple email addresses specified in the configuration file.
#
# Input:
#   -src      # Source address/email account name
#   -dst      # Destination address(es)
#   -subject  # Subject of the email
#   -server   # Email SMTP server name/address
#   -port     # Email SMTP server port
#   -passwd   # Email account password
#   -filename # File name of the zip containing the reports to be sent
#   -log      # Path/to/log_file to register success or failure
#   -retry    # Number of retries to send the reports in case it fails
#   -date     # Date of the reports; format MMDDYY
#   -message  # Message/content of the email
#


import os
import sys
import smtplib
import mimetypes
from email import encoders
from email.mime.multipart import MIMEMultipart
from email.mime.multipart import MIMEBase


###
### Function to parse arguments, returning a dictionary called args
def parseArgs(argv):
  args = {} # Start an empty dictionary to store arguments
  while argv: # Iterate through all arguments, removing each of them once processed
    if argv[0][0] == '-': # This is a "name value"
      args[argv[0]] = argv[1] # Save the "name value" as index and its respec arg value
    argv = argv[1:] # Remove the first element of the arg array
  return args # Return the dictionary


# Parse arguments
_myargs = {}
_myargs = parseArgs(sys.argv)

# Check for type of reports: intraday or postday
if _myargs['-message'] != 'False' and _myargs['-subject'] != 'False':
    import email_lib as Email

    _message = _myargs['-message'].replace(",", "\n")
    _body = "The following postday (refreshed) reports were not generated because their intraday counterparts could not be found:\n" \
                "<LIST OF REPORT NAMES>\n" + str(_message)

    _subject = "Missing intraday reports - postday reports NOT generated"

    # Build basic email information template
    _email_info = {'server': _myargs['-server'],
                            'port': _myargs['-port'],
                            'pw': _myargs['-passwd'],
                            'from': _myargs['-src'],
                            'to': _myargs['-dst'],
                            'message': _body,
                            'subject': _subject}

    # Send email
    Email.sendNotification(_email_info)
    # Exit program because Base URI is critical information
    print "Exiting..."
    sys.exit()

else:


    # Build message
    _msg = MIMEMultipart()
    # Add refreshed to subject if this is postday email
    if str(_myargs['-filename']).find('refreshed') != -1:
        _msg['Subject'] = "Reports from " + _myargs['-date'] + " (refreshed)"
    else:
        _msg['Subject'] = "Reports from " + _myargs['-date']

    _msg['From'] = _myargs['-src']
    #_msg['To'] = _myargs['-dst']
    _to_addrs = str(_myargs['-dst']).split(",")
    _msg['To'] = ", ".join(_to_addrs)

    _preamble = "Reports from " + str(_myargs['-date'])
    # Add refreshed to preamble if this is postday email
    if str(_myargs['-filename']).find('refreshed') != -1:
        _preamble = _preamble + " (refreshed)"
    sys.exit
    _msg.preamble = str(_preamble)


    # Attach the zip file containing all the reports of the day
    ctype, encoding = mimetypes.guess_type(str(_myargs['-filename']))
    maintype, subtype = ctype.split('/', 1)
    _fp = open(str(_myargs['-filename']), 'rb')
    _msg_att = MIMEBase(maintype, subtype)
    _msg_att.set_payload(_fp.read())
    _fp.close()
    encoders.encode_base64(_msg_att)

    _msg_att.add_header('Content-Disposition', 'attachment', filename=str(_myargs['-filename']))

    _msg.attach(_msg_att)

    _msg_as_str = _msg.as_string()

    # Send the email (retry as many times as informed in the argument -retry
    _count_retry = 0
    while _count_retry < int(_myargs['-retry']):

        _failed = False
        _count_retry += 1

        _server = smtplib.SMTP(_myargs['-server'], _myargs['-port'])
        # _server.starttls()

        try:
            # _server.login(_myargs['-src'], _myargs['-passwd'])
            _server.set_debuglevel(0)
            _server.sendmail(_myargs['-src'], _msg['To'].split(","), _msg_as_str)
        except smtplib.SMTPException as _e:
            print "*** ERROR: Problems on connecting to the SMTP server to send email."
            if _count_retry < int(_myargs['-retry']):
                print "           Retrying..."
                _failed = True
            else:
                print "           Aborting and exiting..."
                # Write to fail temp log file
                _f_name = _myargs['-log'] + ".email_fail"
                _f = open(_f_name, 'a')
                _f_str = "fail " + str(_myargs['-date']) + "\n"
                _f.write(_f_str)
                _f.close()
                sys.exit()

        # If the email sending didn't fail, then close SMTP connection and quit the while loop
        if not _failed:
            # Close email server connection
            _server.quit()

            # Write to success temp log file
            _f_name = _myargs['-log'] + ".email_success"
            _f = open(_f_name, 'a')
            _f_str = "success " + str(_myargs['-date']) + "\n"
            _f.write(_f_str)
            _f.close()
            break

