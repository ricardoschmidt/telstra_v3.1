#!/usr/bin/env python
#
# Author:
#   Ricardo de O. Schmidt
#     NAVOMI.com Cloud Solutions
#
# Date:
#   February 12, 2018
#
# Contact:
#   ricardo.schmidt@navomi.com
#
# Description:
#   This is a Python library script with a collection of functions to access the
#   LE Operational Realtime API and retrieve desired data.
#

import imports_lib
import requests
from requests_oauthlib import OAuth1
import json
import cgi
import sys
import time
import datetime
import os
import xml.etree.cElementTree as et
import collections


# General definitions of macros
import general_definitions as GeneralDef


##
## Macros

# Number of request attempts before assuming a problem accessing the API
_M_REQUEST_RETRY = 3

# IMPORTANT: watch for consistency with file extension defined in ftp_connect.py
_M_SERV_NAME_USERS_API      = "accountConfigReadOnly"  	# Service name for LP Users API
_M_SERV_NAME_SKILLS_API     = "accountConfigReadOnly" 	# Service name for LP Skills API
_M_SERV_NAME_REAL_TIME_API  = "leDataReporting"    		# Service name for LP Realtime Operational API
_M_SERV_NAME_ENG_HIST_API	= "engHistDomain"			# Service name for LP Engagement History API

# Debug MACROS
_M_DEBUG_NONE       = 0
_M_DEBUG_BASIC      = 1
_M_DEBUG_VERBOSE    = 2

# Set debug to verbose
_debug_level = _M_DEBUG_VERBOSE



## Function to get the base URI for a given service name
## Return: a string containing the URI in case of success or an empty string in case of failure;
##				 and a notification message that should be reported by email if a failure happens
def LEGetBaseURI(_service_name, _account_number):

    # List of parameters for the request, only version is needed in this case
    _params = { 'version': '1.0' }

    # Build base URI for request including account number and service name
    _request_uri = "http://api.liveperson.net/api/account/" + _account_number + "/service/" + _service_name + "/baseURI.json"

    # Submit the request, max of retries is defined in macro _M_REQUEST_RETRY
    _retries = 0
    while _retries < _M_REQUEST_RETRY:
	    # Increment retries variable control
        _retries += 1

        # Try to retrieve desired data through GET request
        try:
            _response = requests.get(_request_uri, params=_params)
        except requests.exceptions.RequestException as _e:
            # Print error notification message
            if _debug_level == GeneralDef._DEBUG_VERBOSE:
                print "*** Error: Problem on API request at LEGetBaseURI function"
            # If this is the last request attempt, then send a notification message
            if _retries == _M_REQUEST_RETRY:
                _notification_msg = "Error on API request\nRequest: " + str(_request_uri) + "\nException: " + str(_e)
                return '', _notification_msg

        # Exit loop in case of success
        if _response.status_code == 200:
            # Check if there is content in the response
            if len(_response.text) > 0:
                break
            # If not, and this is the last request attempt, send a notification message
            else:
                if _retries == _M_REQUEST_RETRY:
                    _notification_msg = "Error on API request\nRequest: " + str(_request_uri) + "\nEmpty/invalid JSON was returned"
                    return '', _notification_msg

    # If we are here, everything went well and we have a valid response to the API request
    # Now, try to load the returned JSON content
    try:
        _response_json = json.loads(_response.text)
    except ValueError as _e:
        _notification_msg = "Error on loading JSON content from response\nRequest: " + str(_request_uri) + "\nError: " + str(_e)
        return '', _notification_msg

    # Getting to this point means we have a safe response and can directly parse the element of interest into the function return
    return _response_json['baseURI'], ''



## Function to retrieve the employee ID of a given agent ID through the Users API
## Return: a string containing the Employee ID;
##				 "-1" is returned if the agent doesn't have an Employee ID;
##				 and a notification message that should be reported by email if a failure happens
def LEGetEmployeeID(_base_uri, _account_number, _auth, _agent_id):

    # Build request URI
    # _base_uri should be obtained through the LEGetBaseURI
    _request_uri = "https://" + _base_uri + "/api/account/" + _account_number + "/configuration/le-users/users/" + _agent_id

    # Submit the request, max of retries is defined in macro _M_REQUEST_RETRY
    _retries = 0
    while _retries < _M_REQUEST_RETRY:
        # Increment retries variable control
        _retries += 1

        # Try to retrieve desired data through GET request
        try:
            _response = requests.get(_request_uri, auth=_auth)
        except requests.exceptions.RequestException as _e:
            # Print error notification message
            if _debug_level == GeneralDef._DEBUG_VERBOSE:
                print "*** Error: Problem on API request at LEGetEmployeeID function"
            # If this is the last request attempt, build the notification message
            if _retries == _M_REQUEST_RETRY:
                _notification_msg = "Error on API request\nRequest: " + str(_request_uri) + "\nException: " + str(_e)
                return '', _notification_msg

        # Exit loop in case of success
        if _response.status_code == 200:
            # Check if there is content in the response
            if len(_response.text) > 0:
                break
            # If not, and this is the last request attempt, build the notification message
            else:
                if _retries == _M_REQUEST_RETRY:
                    _notification_msg = "Error on API request\nRequest: " + str(_request_uri) + "\nEmpty/invalid JSON was returned"
                    return '', _notification_msg

    # If we are here, everything went well and we have a valid response to the API request
    # Now, try to load the retruned JSON content
    try:
        _response_json = json.loads(_response.text)
    except ValueError as _e:
        _notification_msg = "Error on loading JSON content from response\nRequest: " + str(_request_uri) + "\nError: " + str(_e)
        return '', _notification_msg
        
    # Getting to this point means that we have a safe response and can directly parse the element of interest into the function return
    if _response_json.get('employeeId'):
        return _response_json['employeeId'], ''
    else:
        return '-1', ''



## Function to retrieve Agent Activity through Operational Realtime API
## Return: the entire JSON response if successful;
##				 or an empty string on fail
def LEGetAgentActivity(_base_uri, _account_number, _auth, _params):

    # Build request URI
    # _base_uri should be obtained through the LEGetBaseURI
    _request_uri = "https://" + _base_uri + "/operations/api/account/" + _account_number + "/agentactivity"

    # Submit request up to a max of _M_REQUEST_RETRY retries
    _retries = 0
    while _retries < _M_REQUEST_RETRY:
        # Increment retry variable control
        _retries += 1
        
        # Try to receive desired data through GET request
        try:
            _response = requests.get(_request_uri, params=_params, auth=_auth)
        except requests.exceptions.RequestException as _e:
            # Print error notification message
            if _debug_level == GeneralDef._DEBUG_VERBOSE:
                print "*** Error: Problem on API request at LEGetAgentActivity function\nException: " + str(_e)
            # If this is the last request attempt, build the notification message
            if _retries == _M_REQUEST_RETRY:
                _notification_msg = "Error on API request\nRequest: " + str(_request_uri) + "\nException: " + str(_e)
                return '', _notification_msg

        # Exit loop in case of success
        if _response.status_code == 200:
            # Check if there is content in the response
            if len(_response.text) > 0:
                break
            # If not, and this is the last request attempt, build the notification message
            else:
                if _retries == _M_REQUEST_RETRY:
                    _notification_msg = "Error on API request\nRequest: " + str(_request_uri) + "\nEmpty/invalid JSON was returned"
                    return '', _notification_msg

    # If we are here, everything went well and we have a valid response to the API request
    # Now, try to load the returned JSON content
    try:
        _response_json = json.loads(_response.text)
    except ValueError as _e:
        _notification_msg = "Error on loading JSON content from response\nRequest: " + str(_request_uri) + "\nError: " + str(_e)
        return '', _notification_msg

    # Getting to this point means that we have a safe response and can return the JSON response content
    return _response_json, ''



## Function to retrieve Engagement Activity through Operational Realtime API
## Return: the entire JSON response if successful;
##				 or an empty string on fail
def LEGetEngActivity(_base_uri, _account_number, _auth, _params):

	# Build request URI
	# _base_uri should be obtained through the LEGetBaseURI
    _request_uri = "https://" + _base_uri + "/operations/api/account/" + _account_number + "/engactivity"

    # Submit request up to a max of _M_REQUEST_RETRY retries
    _retries = 0
    while _retries < _M_REQUEST_RETRY:
        # Increment retry variable control
        _retries += 1

        # Try to receive desired data through GET request
        try:
            _response = requests.get(_request_uri, params=_params, auth=_auth)
        except requests.exceptions.RequestException as _e:
            # Print error notification message
            if _debug_level == GeneralDef._DEBUG_VERBOSE:
                print "*** Error: Problem on API request at LEGetEngActivity function"
            # If this is the last request attempt, build the notification message
            if _retries == _M_REQUEST_RETRY:
                _notification_mesg = "Error on API request\nRequest: " + str(_request_uri) + "\nException: " + str(_e)
                return '', _notification_msg

        # Exit loop in case of success
        if _response.status_code == 200:
            # Check if there is content in the response
            if len(_response.text) > 0:
                break
            # If not, and this is the last request attempt, build the notification message
            else:
                if _retries == _M_REQUEST_RETRY:
                    _notification_msg = "Error on API request\nRequest: " + str(_request_uri) + "\nEmpty/invalid JSON was returned"
                    return '', _notification_msg

    # If we are here, everything went well and we have a valid response to the API request
    # Now, try to load the returned JSON content
    try:
        _response_json = json.loads(_response.text)
    except ValueError as _e:
        _notification_msg = "Error on loading JSON content from response\nRequest: " + str(_request_uri) + "\nError: " + str(_e)
        return '', _notification_msg

    # Getting to this point means that we have a safe response and can return the JSON response content
    return _response_json, ''



## Function to retrieve Queue Health through Operational Realtime API
## Return: the entire JSON response if successful;
##				 or an empty string on fail
def LEGetQueueHealth(_base_uri, _account_number, _auth, _params):

    # Build request URI
    # _base_uri should be obtained through the LEGetBaseURI
    _request_uri = "https://" + _base_uri + "/operations/api/account/" + _account_number + "/queuehealth"

    # Submit request up to a max of _M_REQUEST_RETRY retries
    _retries = 0
    while _retries < _M_REQUEST_RETRY:
        # Increment retry variable control
        _retries += 1

        # Try to receive desired data through GET request
        try:
            _response = requests.get(_request_uri, params=_params, auth=_auth)
        except requests.exceptions.RequestException as _e:
            # Print error notification message
            if _debug_level == GeneralDef._DEBUG_VERBOSE:
                print "*** Error: Problem on API request at LEGetQueueHealth function\nException: " + str(_e)
            # If this is the last request attempt, build the notification message
            if _retries == _M_REQUEST_RETRY:
                _notification_msg = "Error on API request\nRequest: " + str(_request_uri) + "\nException: " + str(_e)
                return '', _notification_msg

        # Exit loop in case of success
        if _response.status_code == 200:
            # Check if there is content in the response
            if len(_response.text) > 0:
                break
            # If not, and this is the last request attempt, build the notification message
            else:
                if _retries == _M_REQUEST_RETRY:
                    _notification_msg = "Error on API request\nRequest: " + str(_request_uri) + "\nEmpty/invalid JSON was returned"
                    return '', _notification_msg

    # If we are here, everything went well and we have a valid response to the API request
    # Now, try to load the returned JSON content
    try:
        _response_json = json.loads(_response.text)
    except ValueError as _e:
        _notification_msg = "Error on loading JSON content from response\nRequest: " + str(_request_uri) + "\nError: " + str(_e)
        return '', _notification_msg

    # Gettting to this point means that we have a safe response and can return the JSON response content
    return _response_json, ''



## Function to retrieve data through Engagement History API
## Return: the entire JSON response if successful;
##         or an empty string on fail
## Argument _next controls whether this is a first request or a follow up
def LEGetEngHistory(_next, _base_uri, _account_number, _auth, _data, _headers):

    # If this is a follow up request, the _request_uri is already built in _base_uri
    if _next:
        _request_uri = _base_uri
    else:
        # Build request URI
        # _base_uri should be obtained through the LEGetQueueHealth
        # NOTE: Number of records in the response is hardcoded to 100;
        #       timing tests have shown that requesting 100 at a time is faster than
        #       the default 50 or the suggested (by LP) 25 records per request.
        _request_uri = "https://" + _base_uri + "/interaction_history/api/account/" + _account_number + "/interactions/search?limit=100"

    # Submit request up to a max of _M_REQUEST_RETRY retries
    _retries = 0
    while _retries < _M_REQUEST_RETRY:
        # Increment retry variable control
        _retries += 1

        # Try to receive desired data through POST request
        try:
            _response = requests.post(_request_uri, auth=_auth, data=_data, headers=_headers)
        except requests.exceptions.RequestException as _e:
            # Print error notification message
            if _debug_level == GeneralDef._DEBUG_VERBOSE:
                print "*** Error: Problem on API request at LEGetEngHist function\nException: " + str(_e)
            # If this is the last request attempt, build the notification message
            if _retries == _M_REQUEST_RETRY:
                _notification_msg = "Error on API request\nRequest: " + str(_request_uri) + "\nException: " + str(_e)
                return '', _notification_msg

        # Exit loop in case of success
        if _response.status_code == 200 or _response.status_code == 201:
            # Check if there is content in the response
            if len(_response.text) > 0:
                break
            # If not, and this is the last request attempt, build the notification message
            else:
                if _retries == _M_REQUEST_RETRY:
                    _notification_msg = "Error on API request\nRequest: " + str(_request_uri) + "\nEmpty/invalid JSON was returned"
                    return '', _notification_msg

    # If we are here, everything went well and we have a valid response to the API request
    # Now, try to load the returned JSON content
    try:
        _response_json = json.loads(_response.text)
    except ValueError as _e:
        _notification_msg = "Error on loading JSON content from response\nRequest: " + str(_request_uri) + "\nError: " + str(_e)
        return '', _notification_msg

    # Gettting to this point means that we have a safe response and can return the JSON response content
    return _response_json, ''



## Function to retrieve the list of all skills using "Get all skills" method from Skills API
## Return: the entire JSON response if successful;
##         or an empty string on fail
def LEGetAllSkills(_base_uri, _account_number, _auth):

	# Build request URI
	# _base_uri should be obtained through the LEGetBaseURI
	_request_uri = 'https://' + _base_uri + '/api/account/' + _account_number + '/configuration/le-users/skills'

	# Submit request up to a max of _M_REQUEST_RETRY retries
	_retries = 0
	while _retries < _M_REQUEST_RETRY:
		# Increment retry variable control
		_retries += 1

		# Try to receive desired data through GET request
		try:
			_response = requests.get(_request_uri, auth=_auth)
		except requests.exceptions.RequestException as _e:
			# Print error notification message
			if _debug_level == GeneralDef._DEBUG_VERBOSE:
				print '*** Error: Problem on API request at LEGetAllSkills function\nException: ' + str(_e)
			# If this is the last request attempt, build the notification message
			if _retries == _M_REQUEST_RETRY:
				_notification_msg = 'Error on API request\nRequest: ' + str(_request_uri) + '\nException: ' + str(_e)
				return '', _notification_msg

		# Exit loop in case of success
		if _response.status_code == 200:
			# Check if there is content in the response
			if len(_response.text) > 0:
				break
			# If not, and this is the alst request attempt, build the notification message
			else:
				if _retries == _M_REQUEST_RETRY:
					_notification_msg = 'Error on API request\nRequest: ' + str(_request_uri) + '\nEmpty/invalid JSON was returned'
					return '', _notification_msg

	# If we are here, everything went well and we have a valid response to the API request
	# Now, try to load the returned JSON content
	try:
		_response_json = json.loads(_response.text)
	except ValueError as _e:
		_notification_msg = 'Error on loading JSON content from response\nRequest: ' + str(_request_uri) + '\nError: ' + str(_e)
		return '', _notification_msg

	# Getting to this point means that we have a safe response and can return the JSON response content
	return _response_json, ''



