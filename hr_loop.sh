#!/bin/bash
#
# Author:
#   Ricardo de O. Schmidt
#     NAVOMI.com Cloud Solutions
#
# Date:
#   October 4, 2017
#
# Contact:
#   ricardo.schmidt@navomi.com
#
# Description:
#   This is a Bash script that, based on information provided through a
#   configuration file, will call the Python script 15_min_reports.py to
#   generate XML reports every 15 minutes. This Bash script is meant to be
#   called from Cron jobs every quarter of an hour (i.e., 00, 15, 30 and 45
#   minutes), to generate the XML reports for the quarter immediately before.
#
#   This script executes a series of steps:
#     1.  Makes sure all arguments and pertinent information is found within the
#         configuration file.
#     2.  Calls the 15_min_reports.py script to generate the report for the
#         quarter of the hour immediately before the current run time.
#     3a. In case of S/FTP PUSH option, uploads the generated reports to the FTP
#         server informed in the configuration file.
#     3b. In case of S/FTP PULL option, copies the generated reports to the FTP
#         local folder so that an external code/entity can download the files.
#     4.  Checks on previously failed attempts of report generation and attempt
#         on generating them once again; provided these failed reports are no
#         older than 24 hours (limitation imposed by the available data of
#         LivePerson API).
#
# Input arguments (all mandatory and in order):
#   1.  Path/to/CONFIG_FILE containing configuration information.
#       Default file is config.navomi
#   2.  S/FTP option for getting the reports to the customer; options are:
#         1: PUSH
#         2: PULL
#   3.  Operational System in which this script will run; options are:
#         1: Mac OS
#         2: Linux
#   4.  Debug option; options are:
#         0: None
#         1: Basic
#         2: Verbose
#
# Running:
#   For running instructions please refer to the README file.
#
# Error log:
#   The following are possible error/codes saved within the error log file:
#     (EFR) Error Failed Report: No report file generated for S/FTP transfer.
#

###
### MACROS
###

__LOG_FILE="log";
__ERROR_FILE="error";


###
### ARGUMENTS PARSING
###

# First check if they are there
if [ $# -ne 4 ]; then
  echo "*** ERROR: arguments missing.";
  echo "";
  echo "    Usage:";
  echo "    ./hr_loop.sh <CONF> <FTP> <OS> <DEBUG>";
  echo "      <CONF>  path/to/config.file";
  echo "      <FTP>   FTP option (1:Push, 2:Pull)";
  echo "      <OS>    OS type (1:Mac OS, 2:Linux)";
  echo "      <DEBUG> Debug level (0:None, 1:Basic, 2:Verbose)";
  echo "";
  echo "    Arguments have to follow this specific order.";
  echo "    Check README file for more information.";
  echo "    Exiting...";
  exit;
fi;

# Check configuration file
if [ ! -f $1 ]; then
  echo "*** ERROR: cannot open configuration file.";
  echo "    $1 not found.";
  echo "    Exiting...";
  exit;
else
  # Loads configuration
  source $1;
fi;

# Check FTP option
if [ $2 -eq 1 ]; then # 1 for PUSH
  _ftp_option="PUSH";
elif [ $2 -eq 2 ]; then # 2 for PULL
  _ftp_option="PULL";
else
  echo "*** ERROR: invalid FTP option.";
  echo "    Use 1:Push or 2:Pull";
  echo "    Exiting...";
  exit;
fi;

# Check OS type
if [ $3 -eq 1 ]; then # 1 for Mac OS
  _os="MacOS";
elif [ $3 -eq 2 ]; then # 2 for Linux
  _os="Linux";
else
  echo "*** ERROR: invalid OS option.";
  echo "    Use 1:Mac OS or 2:Linux";
  echo "    Exiting...";
  exit;
fi;

# Check Debug level option
if [ $4 -eq 0 ]; then # 0 for none
  _debug_level=0
elif [ $4 -eq 1 ]; then # 1 for basic
  _debug_level=1
elif [ $4 -eq 2 ]; then # 2 for verbose
  _debug_level=2
else
  echo "*** ERROR: invalid debug option.";
  echo "    Use 0:None, 1:Basic or 2:Verbose";
  echo "    Exiting...";
  exit;
fi;


if [ $_debug_level -eq 1 ]; then
  echo "[ Debug level set to BASIC ]";
elif [ $_debug_level -eq 2 ]; then
  echo "[ Debug level set to VERBOSE ]";
fi;


###
### INITIATE ERROR STRING
###
declare -a _error_str;
declare -i _error_count=0;



###
### FIND THE CORRECT TIMESTAMP FOR THE CURRENT REPORT
###


if [ $_debug_level -ge 1 ]; then
  echo "< Computing date and time for the report >";
fi;


# Check which OS is running, 'cause arguments of command date then change
if [ $_os == "MacOS" ]; then

	# Get current date as _call_time_ts
	_call_time_ts=`date +%s`										# First get the current epoch time (GMT)
	_call_time_dt=`date -r $_call_time_ts +%y%m%d%H%M`				# Converts into readable/manageable format
	_call_time_dt=`echo "${_call_time_dt}00"`						# Add the zero-seconds to round the call time
	_call_time_ts=`date -j -f '%y%m%d%H%M%S' $_call_time_dt +%s`	# Generates again the call time with rounded seconds
	_call_time_hr=`date -r $_call_time_ts +%y-%m-%d.%H:%M:%S`		# Converts into human-readable call time

	# Calculate the report time, which is _call_time_ts reduced by 1 hour
	_report_time_ts=`date -j -f '%s' -v-1H $_call_time_ts +%s`
	_report_time_dt=`date -r $_report_time_ts +%y%m%d%H%M%S`
	_report_time_hr=`date -r $_report_time_ts +%y-%m-%d.%H:%M:%S`

	# Calculate the rounded report time (quarter), which is _call_time_ts reduced by 1 hour and rounded down to a multiplier of 15
	_rounded_time_min=`echo ${_report_time_dt:8:2} | awk '{if($1%15==0){_rounded=$1;}else{_rounded=int($1/15)*15;} if(_rounded==0){print "00";} else{print _rounded;}}'`
	_rounded_time_dt=`echo ${_report_time_dt:0:8}${_rounded_time_min}00`
	_rounded_time_hr=`date -j -f '%y%m%d%H%M%S' $_rounded_time_dt +%y-%m-%d.%H:%M:%S`
	_rounded_time_ts=`date -j -f '%y%m%d%H%M%S' $_rounded_time_dt +%s`

	# Calculate the report name
	_report_name=`date -r $_rounded_time_ts +%m%d%y.%H%M`

	# Calculate 5-min OR API buckets
	_or_api_bucket_1_ts=`date -j -f '%s' -v+5M $_rounded_time_ts +%s`
	_or_api_bucket_1_dt=`date -r $_or_api_bucket_1_ts +%y%m%d%H%M`
	_or_api_bucket_1_hr=`date -r $_or_api_bucket_1_ts +%y-%m-%d.%H:%M`
	_or_api_bucket_2_ts=`date -j -f '%s' -v+10M $_rounded_time_ts +%s`
	_or_api_bucket_2_dt=`date -r $_or_api_bucket_2_ts +%y%m%d%H%M`
	_or_api_bucket_2_hr=`date -r $_or_api_bucket_2_ts +%y-%m-%d.%H:%M`
	_or_api_bucket_3_ts=`date -j -f '%s' -v+15M $_rounded_time_ts +%s`
	_or_api_bucket_3_dt=`date -r $_or_api_bucket_3_ts +%y%m%d%H%M`
	_or_api_bucket_3_hr=`date -r $_or_api_bucket_3_ts +%y-%m-%d.%H:%M`

	# OR API timeframe in seconds, which is the _call_time_ts reduced by the _report_time_ts
	_or_api_timeframe=`echo $_call_time_ts $_report_time_ts | awk '{print ($1-$2)/60}'`

	# Calculate 15-min EH API request interval -- be careful to add seconds in here because they are important
	_eh_api_from_dt=$_rounded_time_dt
	_eh_api_from_ts=`date -j -f '%y%m%d%H%M%S' $_eh_api_from_dt +%s`
	_eh_api_from_hr=`date -j -f '%s' $_eh_api_from_ts +%y-%m-%d.%H:%M:%S`
	_eh_api_to_ts=`date -j -f '%s' -v+899S $_eh_api_from_ts +%s`
	_eh_api_to_dt=`date -r $_eh_api_to_ts +%y%m%d%H%M%S`
	_eh_api_to_hr=`date -r $_eh_api_to_ts +%y-%m-%d.%H:%M:%S`

elif [ $_os == "Linux" ]; then

	# Get current date as _call_time_ts
	_call_time_ts=`date +%s`										# First get the current epoch time (GMT)
	_call_time_dt=`date -d @$_call_time_ts +%y%m%d%H%M`				# Converts into readable/manageable format
	_call_time_dt=`echo "${_call_time_dt}00"`						# Add the zero-seconds to round the call time
	_call_time_lx=`echo "20${_call_time_dt:0:2}-${_call_time_dt:2:2}-${_call_time_dt:4:2} ${_call_time_dt:6:2}:${_call_time_dt:8:2}:${_call_time_dt:10:2}"`	# This has to be done so that we can convert from human readable date format to epoch
	_call_time_ts=`date -d "$_call_time_lx" +%s`					# Generates again the call time with rounded seconds
	_call_time_hr=`date -d @$_call_time_ts +%y-%m-%d.%H:%M:%S`		# Converts into human-readable call time
	_call_time_str=`date -d @$_call_time_ts`						# Save the full date string to perform date operations next

	# Calculate the report time, which is _call_time_ts reduced by 1 hour
	_report_time_ts=`date -d "$_call_time_str-1 hour" +%s`
	_report_time_dt=`date -d @$_report_time_ts +%y%m%d%H%M%S`
	_report_time_hr=`date -d @$_report_time_ts +%y-%m-%d.%H:%M:%S`

	# Calculate the rounded report time (quarter), which is _call_time_ts reduced by 1 hour and rounded down to a multiplier of 15
	_rounded_time_min=`echo ${_report_time_dt:8:2} | awk '{if($1%15==0){_rounded=$1;}else{_rounded=int($1/15)*15;} if(_rounded==0){print "00";} else{print _rounded;}}'`
	_rounded_time_dt=`echo ${_report_time_dt:0:8}${_rounded_time_min}00`
	_rounded_time_lx=`echo "20${_rounded_time_dt:0:2}-${_rounded_time_dt:2:2}-${_rounded_time_dt:4:2} ${_rounded_time_dt:6:2}:${_rounded_time_dt:8:2}:${_rounded_time_dt:10:2}"`
	_rounded_time_ts=`date -d "$_rounded_time_lx" +%s`
	_rounded_time_hr=`date -d @$_rounded_time_ts +%y-%m-%d:%H:%M:%S`
	_rounded_time_str=`date -d @$_rounded_time_ts`

	# Calculate the report name
	_report_name=`date -d "$_rounded_time_lx" +%m%d%y.%H%M`

	# Calculate 5-min OR API buckets
	_or_api_bucket_1_ts=`date -d "$_rounded_time_str+5 minutes" +%s`
	_or_api_bucket_1_dt=`date -d @$_or_api_bucket_1_ts +%y%m%d%H%M`
	_or_api_bucket_1_hr=`date -d @$_or_api_bucket_1_ts +%y-%m-%d.%H:%M`
	_or_api_bucket_2_ts=`date -d "$_rounded_time_str+10 minutes" +%s`
	_or_api_bucket_2_dt=`date -d @$_or_api_bucket_2_ts +%y%m%d%H%M`
	_or_api_bucket_2_hr=`date -d @$_or_api_bucket_2_ts +%y-%m-%d.%H:%M`
	_or_api_bucket_3_ts=`date -d "$_rounded_time_str+15 minutes" +%s`
	_or_api_bucket_3_dt=`date -d @$_or_api_bucket_3_ts +%y%m%d%H%M`
	_or_api_bucket_3_hr=`date -d @$_or_api_bucket_3_ts +%y-%m-%d.%H:%M`

	# OR API timeframe in seconds, which is the _call_time_ts reduced by the _report_time_ts
	_or_api_timeframe=`echo $_call_time_ts $_report_time_ts | awk '{print ($1-$2)/60}'`

	# Calculate 15-min EH API request interval -- be careful to add seconds in here because they are important
	_eh_api_from_dt=$_rounded_time_dt
	_eh_api_from_lx=`echo "20${_eh_api_from_dt:0:2}-${_eh_api_from_dt:2:2}-${_eh_api_from_dt:4:2} ${_eh_api_from_dt:6:2}:${_eh_api_from_dt:8:2}:${_eh_api_from_dt:10:2}"`
	_eh_api_from_ts=`date -d "$_eh_api_from_lx" +%s`
	_eh_api_from_hr=`date -d @$_eh_api_from_ts +%y-%m-%d.%H:%M:%S`
	_eh_api_from_str=`date -d @$_eh_api_from_ts`

	_eh_api_to_ts=`date -d "$_eh_api_from_str+899 seconds" +%s`
	_eh_api_to_dt=`date -d @$_eh_api_to_ts +%y%m%d%H%M%S`
	_eh_api_to_hr=`date -d @$_eh_api_to_ts +%y-%m-%d.%H:%M:%S`

fi;

# Print debug message informing times to be used in the generation of reports
if [ $_debug_level -eq 2 ]; then
	echo "===========================================================================";
	echo "Call time        : $_call_time_hr [$_call_time_dt] ($_call_time_ts)"
	echo "Report time      : $_report_time_hr [$_report_time_dt] ($_report_time_ts)"
	echo "Rounded time     : $_rounded_time_hr [$_rounded_time_dt] ($_rounded_time_ts)"
	echo "Report name      : *_$_report_name.xml"
	echo "OR API buckets   : $_or_api_bucket_1_hr [$_or_api_bucket_1_dt] ($_or_api_bucket_1_ts)"
	echo "                   $_or_api_bucket_2_hr [$_or_api_bucket_2_dt] ($_or_api_bucket_2_ts)"
	echo "                   $_or_api_bucket_3_hr [$_or_api_bucket_3_dt] ($_or_api_bucket_3_ts)"
	echo "OR API timeframe : $_or_api_timeframe"
	echo "EH API interval  : $_eh_api_from_hr [$_eh_api_from_dt] ($_eh_api_from_ts)"
	echo "                   $_eh_api_to_hr [$_eh_api_to_dt] ($_eh_api_to_ts)"
	echo "===========================================================================";
fi;

###
### VERIFY INFORMATION IN CONFIG.NAVOMI
###

if [ $_debug_level -ge 1 ]; then
  echo "< Verifying and validating configuration file $1 >";
fi;

# Check if config file exists
if [ ! -f $_config_file_path ]; then
  echo "*** ERROR: configuration file cannot be found.";
  echo "    Informed path is: $_config_file_path";
  echo "    Exiting...";
  exit;
else
  if [ $_debug_level -ge 1 ]; then
    echo "  . Configuration file $_config_file_path found";
  fi;
fi;

# Check whether mandatory LP API access credentials are informed in the config file
if [ -z $_lp_api_account ] ||
   [ -z $_lp_api_key ] || \
   [ -z $_lp_api_secret ] || \
   [ -z $_lp_api_token ] || \
   [ -z $_lp_api_token_secret ] || \
   [ -z $_lp_api_signature_method ]; then
  echo "*** ERROR: missing access credentials for the Live Person API.";
  echo "    Please inform these in the configuration file: $_config_file_path";
  echo "    Exiting...";
  exit;
else
  if [ $_debug_level -eq 2 ]; then
    echo "    .. Access credentials for LP API found in the config file";
  fi;
fi;

# Check whether mandatory FTP account access credentials for PUSH are informed in the config file
if [ $_ftp_option == "PUSH" ]; then
  if [ -z $_ftp_host ] || \
     [ -z $_ftp_port ] || \
     [ -z $_ftp_user ] || \
     [ -z $_ftp_pw ]; then
    echo "*** ERROR: missing access credentials for the FTP account.";
    echo "    Please inform these in the configuration file: $_config_file_path";
    echo "    Exiting...";
    exit;
  else
    if [ $_debug_level -eq 2 ]; then
      echo "    .. Access credentials for FTP account found in the config file...";
    fi;
  fi;
# Check whether mandatory FTP local directory for PULL is informed and actually exists
elif [ $_ftp_option == "PULL" ]; then
  if [ -z $_ftp_local_dir ]; then
    echo "*** ERROR: missing local directory for PULL FTP.";
    echo "    Please inform these in the configuration file: $_config_file_path";
    echo "    Exiting...";
    exit;
  fi;
  if [ ! -d $_ftp_local_dir ]; then
    echo "*** ERROR: the informed local directory $_ftp_local_dir for FTP PULL does not exist."
    echo "    Please create the directory and try again."
    echo "    Exiting..."
    exit;
  fi;
fi;

# Check whether mandatory email information and credentials are informed in the config file
if [ -z $_email_server ] || \
   [ -z $_email_port ] || \
   [ -z $_email_from_addr ] || \
   [ -z $_email_pw ] || \
   [ -z $_email_to_addr ] || \
   [ -z $_email_subject ]; then
  echo "*** ERROR: missing access credentials and information for notification emails.";
  echo "    Please inform these in the configuration file: $_config_file_path";
  echo "    Exiting...";
  exit;
fi;

# Check whether non-mandatory fields were informed and load the default if needed

# Output path for storing reports (XML files)
if [ -z $_output_path_report ]; then
  _output_path_report="./reports/";
  if [ $_debug_level -eq 2]; then
    echo "    .. Output path for report files not informed, set to default...";
  fi;
fi;
# Create folder for reports if it doesn't exist
if [ ! -d $_output_path_report ]; then
  mkdir $_output_path_report;
  if [ $_debug_level -eq 2 ]; then
    echo "    .. Output folder for report files created...";
  fi;
fi;

# Output path for storing reports (XML files) from reruns through rerun.sh
if [ -z $_output_path_rerun ]; then
  _output_path_rerun="./rerun_reports/";
  if [ $_debug_level -eq 2]; then
    echo "    .. Output path for report files not informed, set to default...";
  fi;
fi;
# Create folder for reports if it doesn't exist
if [ ! -d $_output_path_rerun ]; then
  mkdir $_output_path_rerun;
  if [ $_debug_level -eq 2 ]; then
    echo "    .. Output folder for report files created...";
  fi;
fi;

# Path to store the log files
if [ -z $_output_path_log ]; then
  _output_path_log="./logs/";
  if [ $_debug_level -eq 2 ]; then
    echo "    .. Output path for log files not informed, set to default...";
  fi;
fi;
# Create folder for logs if it doesn't exist
if [ ! -d $_output_path_log ]; then
  mkdir $_output_path_log;
  if [ $_debug_level -eq 2 ]; then
    echo "    .. Output folder for log files created...";
  fi;
fi;

# Path to store the applog files
if [ -z $_output_path_applog ]; then
  _output_path_applog="./app_logs/";
  if [ $_debug_level -eq 2 ]; then
    echo "    .. Output path for applog files not informed, set to default...";
  fi;
fi;
# Create folder for logs if it doesn't exist
if [ ! -d $_output_path_applog ]; then
  mkdir $_output_path_applog;
  if [ $_debug_level -eq 2 ]; then
    echo "    .. Output folder for applog files created...";
  fi;
fi;

# Input file containing the agent-to-employee IDs mapping
if [ -z $_input_agent_employee ]; then
  _input_agent_employee="./agent_employee_map.csv";
  if [ $_debug_level -eq 2 ]; then
    echo "    .. Input file for agent-to-employee mapping not informed, set to default...";
  fi;
fi;
# Create input file if it doesn't exist
if [ ! -f $_input_agent_employee ]; then
  touch $_input_agent_employee;
  if [ $_debug_level -eq 2 ]; then
    echo "    .. Input file for agent-to-employee $_input_agent_employee created...";
  fi;
fi;

# Output path for JSON files
if [ -z $_output_path_json ]; then
	_output_path_json="./json/";
	if [ $_debug_level -eq 2 ]; then
		echo "    .. Output path for JSON files not informed, set to default...";
	fi;
fi;
if [ ! -d $_output_path_json ]; then
    mkdir $_output_path_json;
	if [ $_debug_level -eq 2 ]; then
		echo "    .. Output folder for JSON files created...";
	fi;
fi;

# Input file containing the concurrency factors for all skills
if [ -z $_input_concurrency_factor ]; then
	_input_concurrency_factor="./concurrency_factor.csv"
	if [ $_debug_level -eq 2 ]; then
		echo "    .. Input file for concurrency factors not informed, set to default...";
	fi;
fi;
# Create input file if it doesn't exist
if [ ! -f $_input_concurrency_factor ]; then
	touch $_input_concurrency_factor;
	if [ $_debug_level -eq 2 ]; then
		echo "    .. Input file for concurrency factor $_input_concurrency_factor created...";
	fi;
fi;

# Set the default concurrency factor if none is informed
if [ -z $_default_concurrency_factor ]; then _default_concurrency_factor=1; fi;

# API arguments
if [ -z $_api_arg_timeframe ]; then _api_arg_timeframe="15"; fi; # Default to 15 minutes, which is the minimum value possible
if [ -z $_api_arg_interval ]; then _api_arg_interval="5"; fi; # Default to 5 minutes, which is the minimum value possible
if [ -z $_api_arg_version ]; then _api_arg_version="1"; fi; # Default to version 1
if [ -z $_api_arg_agent_ids ]; then _api_arg_agent_ids="all"; fi; # Default set to all, that is no specific agent
if [ -z $_api_arg_skill_ids ]; then _api_arg_skill_ids="all"; fi; # Default set to all, that is no specific skill
if [ $_debug_level -eq 2 ]; then
  echo "    .. API arguments set to default (if not given in the config file)...";
fi;

# Maximum validity date for report files before being deleted
if [ -z $_max_file_hours ]; then
  _max_file_hours="168";
  echo "    .. Maximum hours for validity of report files not informed, set to default (168 hours; i.e. one week)...";
fi;

if [ $_debug_level -ge 1 ]; then
  echo "  . Configuration file check completed";
fi;



###
### GENERATE REPORT FOR THE CURRENT PERIOD
###

if [ $_debug_level -ge 1 ]; then
  echo "< Generating XML reports for ${_report_name} >";
fi;

# Log start time
_time_before=`date +%s`;

# Credentials to access the LivePerson are retrieved from config.navomi inside the Python script
# Arguments specification is given in the beginning of 15_min_reports.py script
_timeframe="20"
# python 15_min_reports.py \
# source proxies.txt
python2.7 generate_reports.py \
        -a $_lp_api_account \
        -k $_lp_api_key \
        -s $_lp_api_secret \
        -t $_lp_api_token \
        -T $_lp_api_token_secret \
        -m $_lp_api_signature_method \
        -o $_output_path_report \
        -l $_output_path_log \
        -al $_output_path_applog \
        -f $_or_api_timeframe \
		-b1ts $_or_api_bucket_1_ts \
		-b2ts $_or_api_bucket_2_ts \
		-b3ts $_or_api_bucket_3_ts \
		-b1hr $_or_api_bucket_1_hr \
		-b2hr $_or_api_bucket_2_hr \
		-b3hr $_or_api_bucket_3_hr \
        -i $_api_arg_interval \
        -v $_api_arg_version \
        -A $_api_arg_agent_ids \
        -S $_api_arg_skill_ids \
        -e $_input_agent_employee \
        -d $_or_api_bucket_1_ts \
        -F $_or_api_bucket_3_ts \
        -V $_debug_level \
        -n $_report_name \
        -j $_output_path_json \
        -es $_email_server \
        -ep $_email_port \
        -ew $_email_pw \
        -ef $_email_from_addr \
        -et $_email_to_addr \
        -ej $_email_subject \
		-ehFrom $_eh_api_from_ts \
		-ehTo $_eh_api_to_ts \
		-cf $_input_concurrency_factor \
		-dcf $_default_concurrency_factor \
		-cts $_call_time_ts \
		-postday False \
		-inxml .

if [ $_debug_level -ge 1 ]; then
  echo "  . Reports generation completed";
fi;

# Log end time
_time_after=`date +%s`;
echo "$_time_before 15_min_rep ${_report_name:1:11} $_time_after" >> $_output_path_log$__LOG_FILE;


#echo ""
#echo "quiting..."
#echo ""
#
#exit;






###
### ATTEMPT TO FIX PREVIOUSLY LOGGED ERRORS
###


if [ $_debug_level -eq 2 ]; then
	echo "< Error handling, regenerating failed reports >";
fi;

# Check if the error log file exists, if not, there is nothing to fix
if [ ! -f $_output_path_log$__ERROR_FILE ]; then
	echo "  . No error to be fixed";

# There is an error log file, every line means one failed interval for 15-min intraday report
else

	# Check how many lines there are in the log file. If none, there is nothing to fix
	_error_count_lines=`wc -l $_output_path_log$__ERROR_FILE | awk '{print $1;}'`;
	if [ $_error_count_lines -eq 0 ]; then
		echo "  . No error to be fixed";
	
	# There are lines
	else

		# Read line by line of the error file and save back only those that fail to be regenerated
		_error_count=0
		while IFS=',' read -ra _line; do

			echo "  . FIXING: Report for ${_line[1]} will be regenerated"

			# Check if the error is longer than 24 hours (limit for report regeneration)
			_time_now_ts=`date +%s`
			if [ $_os == 'MacOS' ]; then
				_line_ts=`date -j -u -f "%m%d%y.%H%M" ${_line[1]} +%s`
			elif [ $_os == 'Linux' ]; then
				_line_str=`echo "20${_line[1]:4:2}-${_line[1]:0:2}-${_line[1]:2:2} ${_line[1]:7:2}:${_line[1]:9:2}:00"`
				_line_ts=`date -d "$_line_str" +%s`
			fi;

			# Calculate the difference between the two dates in seconds
			let _diff_ts=${_time_now_ts}-${_line_ts}

			# 24 hours == 86400 seconds
			if [ $_diff_ts -ge 86400 ]; then
				# Reports are older than 24 hours, which is the time limit imposed by the Operational Realtime API from LivePerson. Regeneration is no longer possible
				if [ $_debug_level -eq 2 ]; then
					echo '    .. Reports are too old to be regenerated'
				fi;
			else
				# Call the rerun script to regenerate the report with debug option set to verbose
				./rerun.sh $1 $2 $3 2 $_output_path_rerun ${_line[1]} ${_line[1]}

				# Check if report files were generated
				if [ -f ${_output_path_rerun}AS_${_line[1]}.xml ] &&
				   [ -f ${_output_path_rerun}AQ_${_line[1]}.xml ] &&
				   [ -f ${_output_path_rerun}Q_${_line[1]}.xml ]; then

					# Copy regenerated reports to the reports folder
					cp ${_output_path_rerun}*_${_line[1]}.xml ${_output_path_report}

					if [ $_debug_level -eq 2 ]; then
						echo "    .. ${_file[1]} reports regenerated and copied to $_output_path_report"
					fi;
				else
					_error_count+=1
					_error_str[$_error_count]=`echo "${_line[0]},${_line[1]}"`
					echo "    .. Regeneration failed, ${_line[1]} back to error log"
				fi;

			fi;

		done < "$_output_path_log$__ERROR_FILE"

	fi;

fi;

# "Clean" error file
if [ -f $_output_path_log$__ERROR_FILE ]; then
	rm $_output_path_log$__ERROR_FILE
fi;

# Save errors from this run time to error log
if [ ${#_error_str[@]} -gt 0 ]; then
	if [ $_debug_level -eq 2 ]; then
		echo "    .. Saving persisting errors to $_output_path_log$__ERROR_FILE"
	fi;

	for _e in ${_error_str[@]}; do
		echo $_e >> $_output_path_log$__ERROR_FILE
	done;
fi;



###
### CHECK IF REPORTS FROM THIS RUN TIME FAILED
###
### NOTE: It adds this error here so that it doesn't try to regenerate immediately after it failed.
###

_error_count=0

# Check if AS report has been successfully generated
if [ ! -f ${_output_path_report}AS_${_report_name}.xml ]; then
	_error_count+=1

elif [ ! -f ${_output_path_report}AQ_${_report_name}.xml ]; then
	_error_count+=1

elif [ ! -f ${_output_path_report}Q_${_report_name}.xml ]; then
	_error_count+=1
fi;

if [ $_error_count -gt 0 ]; then
	echo "EFR,${_rounded_time_dt:2:2}${_rounded_time_dt:4:2}${_rounded_time_dt:0:2}.${_rounded_time_dt:6:2}${_rounded_time_dt:8:2}" >> $_output_path_log$__ERROR_FILE;
fi;



###
### DELETE EXPIRED FILES
###


if [ $_debug_level -ge 1 ]; then
  echo "< Removing expired XML report and JSON data files >";
fi;

_current_ts=`date +%s`;
let _max_file_secs=${_max_file_hours}*3600;
_count_removed=0;

for _file in $( ls ${_output_path_report}Q_*.xml | xargs -n1 basename ); do

  # Get report name (AS, AQ or Q)
  _arr=(${_file//"_"/ });
  _rep_name=${_arr[0]};

  # Get report date and time
  _arr=(${_arr[1]//"."/ });
  _rep_date=${_arr[0]};
  _rep_time=`echo ${_arr[1]}00`;

  _rep_hr=`echo $_rep_date$_rep_time`;
  if [ $_os == "MacOS" ]; then
    _rep_ts=`date -j -u -f "%m%d%y%H%M%S" $_rep_hr +%s`;
  elif [ $_os == "Linux" ]; then
    _rep_hr_linux=`echo "20${_rep_hr:4:2}-${_rep_hr:0:2}-${_rep_hr:2:2} ${_rep_hr:6:2}:${_rep_hr:8:2}:${_rep_hr:10:2}"`;
    _rep_ts=`date -d "$_rep_hr_linux" +%s`;
  fi;

  let _diff_time=${_current_ts}-${_rep_ts};
  let _diff_hours=${_diff_time}/3600;

  # Chech if the report is OLDER than threshold, and if so remove all related files to it
  if [ "$_diff_time" -ge "$_max_file_secs" ]; then

    if [ $_debug_level -eq 2 ]; then
      echo "  . Report for $_rep_date.${_rep_time:0:4} is $_diff_hours hours old";
    fi;

    # Remove xml report and json data files for the Queue data report (Q)
    if [ -f ${_output_path_report}Q_${_rep_date}.${_rep_time:0:4}.xml ]; then
      if [ $_debug_level -eq 2 ]; then
        echo "    .. Removing ${_output_path_report}Q_${_rep_date}${_rep_time:0:4}.xml";
      fi;
      rm -f ${_output_path_report}Q_${_rep_date}.${_rep_time:0:4}.xml;
      let _count_removed=$_count_removed+1; # Increment removed counter
    fi;
    if [ -f ${_output_path_json}Q_${_rep_date}.${_rep_time:0:4}.json ]; then
      if [ $_debug_level -eq 2 ]; then
        echo "    .. Removing ${_output_path_json}Q_${_rep_date}${_rep_time:0:4}.json";
      fi;
      rm -f ${_output_path_json}Q_${_rep_date}.${_rep_time:0:4}.json;
      let _count_removed=$_count_removed+1; # Increment removed counter
    fi;

    # Remove xml report and json data files for the Agent Queue data report (AQ)
    if [ -f ${_output_path_report}AQ_${_rep_date}.${_rep_time:0:4}.xml ]; then
      if [ $_debug_level -eq 2 ]; then
        echo "    .. Removing ${_output_path_report}AQ_${_rep_date}${_rep_time:0:4}.xml";
      fi;
      rm -f ${_output_path_report}AQ_${_rep_date}.${_rep_time:0:4}.xml;
      let _count_removed=$_count_removed+1; # Increment removed counter
    fi;
    if [ -f ${_output_path_json}AQ_${_rep_date}.${_rep_time:0:4}.json ]; then
      if [ $_debug_level -eq 2 ]; then
        echo "    .. Removing ${_output_path_json}AQ_${_rep_date}${_rep_time:0:4}.json";
      fi;
      rm -f ${_output_path_json}AQ_${_rep_date}.${_rep_time:0:4}.json;
      let _count_removed=$_count_removed+1; # Increment removed counter
    fi;

    # Remove xml report and json data files for the Agent System data report (AS)
    if [ -f ${_output_path_report}AS_${_rep_date}.${_rep_time:0:4}.xml ]; then
      if [ $_debug_level -eq 2 ]; then
        echo "    .. Removing ${_output_path_report}AS_${_rep_date}${_rep_time:0:4}.xml";
      fi;
      rm -f ${_output_path_report}AS_${_rep_date}.${_rep_time:0:4}.xml;
      let _count_removed=$_count_removed+1; # Increment removed counter
    fi;
    if [ -f ${_output_path_json}AS_${_rep_date}.${_rep_time:0:4}.json ]; then
      if [ $_debug_level -eq 2 ]; then
        echo "    .. Removing ${_output_path_json}AS_${_rep_date}${_rep_time:0:4}.json";
      fi;
      rm -f ${_output_path_json}AS_${_rep_date}.${_rep_time:0:4}.json;
      let _count_removed=$_count_removed+1; # Increment removed counter
    fi;

	# Remove all json from EH API, including refreshed ones
    if [ -f ${_output_path_json}EH_${_rep_date}.${_rep_time:0:4}.json ]; then
      if [ $_debug_level -eq 2 ]; then
        echo "    .. Removing ${_output_path_json}EH_${_rep_date}${_rep_time:0:4}.json";
      fi;
      rm -f ${_output_path_json}EH_${_rep_date}.${_rep_time:0:4}.json;
      let _count_removed=$_count_removed+1; # Increment removed counter
    fi;
    if [ -f ${_output_path_json}EH_${_rep_date}.${_rep_time:0:4}.refreshed.json ]; then
      if [ $_debug_level -eq 2 ]; then
        echo "    .. Removing ${_output_path_json}EH_${_rep_date}${_rep_time:0:4}.refreshed.json";
      fi;
      rm -f ${_output_path_json}EH_${_rep_date}.${_rep_time:0:4}.refreshed.json;
      let _count_removed=$_count_removed+1; # Increment removed counter
    fi;

    # Remove all AS, AQ and Q xml reports from postday output folder
    if [ -f ${_output_path_postday}AS_${_rep_date}.${_rep_time:0:4}.xml ]; then
      if [ $_debug_level -eq 2 ]; then
        echo "    .. Removing ${_output_path_postday}AS_${_rep_date}${_rep_time:0:4}.xml";
      fi;
      rm -f ${_output_path_postday}AS_${_rep_date}.${_rep_time:0:4}.xml;
      let _count_removed=$_count_removed+1; # Increment removed counter
    fi;
    if [ -f ${_output_path_postday}AQ_${_rep_date}.${_rep_time:0:4}.xml ]; then
      if [ $_debug_level -eq 2 ]; then
        echo "    .. Removing ${_output_path_postday}AQ_${_rep_date}${_rep_time:0:4}.xml";
      fi;
      rm -f ${_output_path_postday}AQ_${_rep_date}.${_rep_time:0:4}.xml;
      let _count_removed=$_count_removed+1; # Increment removed counter
    fi;
    if [ -f ${_output_path_postday}Q_${_rep_date}.${_rep_time:0:4}.xml ]; then
      if [ $_debug_level -eq 2 ]; then
        echo "    .. Removing ${_output_path_postday}Q_${_rep_date}${_rep_time:0:4}.xml";
      fi;
      rm -f ${_output_path_postday}Q_${_rep_date}.${_rep_time:0:4}.xml;
      let _count_removed=$_count_removed+1; # Increment removed counter
    fi;

  fi;

done;

if [ $_debug_level -ge 1 ]; then
  echo "  . $_count_removed removed files";
fi;














###
### PREPARE REPORT FILES FOR FTP PULL OR SEND IN CASE OF PUSH
###
### NOTE: DEPRECATED (use skip to avoid FTP operation)
###

declare -i _skip=1

if [ $_skip -eq 0 ]; then # Start of SKIP condition

# Recover report file name
let _rep_ts=${_start_bucket_ts}-300;
if [ $_os == "MacOS" ]; then
  _rep_date=`date -r $_rep_ts +%m%d%y`;
  _rep_time=`date -r $_rep_ts +%H%M`;
elif [ $_os == "Linux" ]; then
  _rep_date=`date -d @$_rep_ts +%m%d%y`;
  _rep_time=`date -d @$_rep_ts +%H%M`;
fi;

_error_count=0;
if [ $_ftp_option == "PULL" ]; then

  # Check if AS report is in $_output_path_report, if so copy it to _ftp_local_dir
  if [ ! -f ${_output_path_report}AS_${_rep_date}.${_rep_time}.* ]; then
    echo "*** ERROR: report AS was not found in ${_output_path_report}.";
    echo "    This report won't be available for FTP PULL.";
    _time_now=`date +%s`;
    _error_count+=1;
  else
    cp ${_output_path_report}AS_${_rep_date}.${_rep_time}.* ${_ftp_local_dir};
  fi;

  # Check if AQ report is in $_output_path_report, if so copy it to _ftp_local_dir
  if [ ! -f ${_output_path_report}AQ_${_rep_date}.${_rep_time}.* ]; then
    echo "*** ERROR: report AQ was not found in ${_output_path_report}.";
    echo "    This report won't be available for FTP PULL.";
    _time_now=`date +%s`;
    _error_count+=1;
  else
    cp ${_output_path_report}AQ_${_rep_date}.${_rep_time}.* ${_ftp_local_dir};
  fi;

  # Check if Q report is in $_output_path_report, if so copy it to _ftp_local_dir
  if [ ! -f ${_output_path_report}Q_${_rep_date}.${_rep_time}.* ]; then
    echo "*** ERROR: report Q was not found in ${_output_path_report}.";
    echo "    This report won't be available for FTP PULL.";
    _time_now=`date +%s`;
    _error_count+=1;
  else
    cp ${_output_path_report}Q_${_rep_date}.${_rep_time}.* ${_ftp_local_dir};
  fi;

  if [ $_error_count -gt 0 ]; then
    _error_str[$_error_count]=`echo "EFR,$_time_now,${_rep_date}.${_rep_time}"`;
  fi;

elif [ $_ftp_option == "PUSH" ]; then

  ### CHECK FOLDER STRUCTURE ON FTP SERVER
  ### NOTE - This can be scheduled to run only once at the very first time of the day to save time
  if [ $_ftp_option == "PUSH" ]; then
    python2.7 ftp_create_folder.py \
            -u $_ftp_user \
            -s $_ftp_pw \
            -h $_ftp_host \
            -p $_ftp_port
  fi;

  ### SEND REPORT VIA S/FTP
  # Call Python script that will
  #   1. connect to the FTP server
  #   2. check/create folder structure (YYYY/mm/dd)
  #   3. upload the report file to the pertinent folder
  python2.7 ftp_send_report.py \
          -u $_ftp_user \
          -s $_ftp_pw \
          -h $_ftp_host \
          -p $_ftp_port \
          -o $_output_path_report \
          -l $_output_path_log

fi;

fi; # End of SKIP condition

