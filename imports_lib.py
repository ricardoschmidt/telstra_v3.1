#!/usr/bin/env python
#
# Author:
#   Luciano Buhler
#     NAVOMI.com Cloud Solutions
#
# Date:
#   March 27, 2018
#
# Contact:
#   luciano.buhler@navomi.com
#
# Description: This script add the path '<PROJECT_PATH>/LIB' to SYS.PATH to solve problems
# of libraries imports in restricted environment

import os, sys

impDebug = ""

# test plataform
# linux
if sys.platform == "linux" or sys.platform == "linux2":
    libPath = str(os.path.dirname(os.path.abspath(__file__)) + '/lib')
# OS X
elif sys.platform == "darwin":
    libPath = str(os.path.dirname(os.path.abspath(__file__)) + '/lib')
# Windows...
elif sys.platform == "win32":
    libPath = str(os.path.dirname(os.path.abspath(__file__)) + '\lib')

#print(libPath)
# print ('')

# '<path>/lib' will be added in SYS.PATH if is not alredy
if (libPath not in sys.path):
    # sys.path.append(libPath)
    sys.path.insert(0, libPath)
    impDebug = "Added path: '" + libPath + "' to SYS.PATH"
    # print(impDebug)
else:
    impDebug = "The path: '" + libPath + "' is already in SYS.PATH"
    # print(impDebug)
