#/usr/bin/env python
#
# Ricardo de O. Schmidt
#   September 3, 2017
#
# Description:
#   Connect to the FTP server and creates the folder structure for the current day.
#   Folder structure has to follow the following structure:
#     <FTP host>:~/YYYY/mm/dd/
#   Where:
#     YYYY : four digits of current year, e.g. 2017
#     mm   : two digits of current month, e.g. 09
#     dd   : two digits of current day, e.g. 03
#
# Arguments (all of them are mandatory):
#   -u : FTP user
#   -s : FTP password
#   -h : FTP host
#   -p : FTP port
#

import ftplib
import socket
import pathlib
import sys
import time


###
### Function to parse arguments, returning a dictionary called args
def parseArgs(argv):
  args = {} # Start an empty dictionary to store arguments
  while argv: # Iterate through all arguments, removing each of them once processed
    if argv[0][0] == '-': # This is a "name value"
      args[argv[0]] = argv[1] # Save the "name value" as index and its respec arg value
    argv = argv[1:] # Remove the first element of the arg array
  return args # Return the dictionary


###
### Function to check and create a given folder
def folderCheckCreate(_folder):
  try:
    _ftp.cwd(_folder) # Attempt to access folder

  except ftplib.error_perm as _e:
    _e_code = str(_e).split(' ')[0] # Get error code

    # 550 == Can't change directory to <DIR>: No such file or directory
    if _e_code == "550":
      print "** Issue: folder " + _folder + " not found at FTP server, creating..."

      try:
        _ftp.mkd(_folder)

      except ftplib.error_perm as _e2:
        print "*** ERROR: not possible to create folder " + _folder + " at the FTP server."
        print "           " + str(_e2)
        print "           Exiting..."
        sys.exit(0)

      else:
        print "** Issue: folder " + _folder + " created with success!"

  else:
    print "Folder " + _folder + " found, nothing to do."




###
### START OF THE ACTUAL SCRIPT
###


# Call parseArgs and store arguments in the dictionary _myargs
_myargs = {}
_myargs = parseArgs(sys.argv)


# Try to connect with the FTP server and send the files
_retry_connect = True   # Control the number of retries
while _retry_connect:
  try:
    _ftp = ftplib.FTP(_myargs['-h'])          # Specify the FTP host to connect, with port number for now
    _ftp.login(_myargs['-u'], _myargs['-s'])  # Login information

  except ftplib.error_perm as _e:
    _retry_connect = False
    print "*** ERROR: problem on connecting to FTP server."
    print "           " + _e
    print "           Exiting..."
    sys.exit(0)

  else:
    print "FTP connected with success!"
    _retry_connect = False

# Check if folder <year> exists, if not create the folder
folderCheckCreate(time.strftime('%Y'))

# Check if folder <month> exists, if not create the folder
folderCheckCreate(time.strftime('%m'))

# Check if folder <day> exists, if not create the folder
folderCheckCreate(time.strftime('%d'))

