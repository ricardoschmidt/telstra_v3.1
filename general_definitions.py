#!/usr/bin/env python
#
#	Author:
#		Ricardo de O. Schmidt
#			NAVOMI.com Cloud Solutions
#
#	Date:
#		February 12, 2018
#
#	Contact:
#		ricardo.schmidt@navomi.com
#
# Description:
#   This script only contains MACROS definitions.
#

_DEBUG_NONE     = 0
_DEBUG_BASIC    = 1
_DEBUG_VERBOSE  = 2

