#!/bin/bash
#
# April 10, 2018
#
# Luciano Buhler
#   luciano.buhler@navomi.com
#
# Description:
#   This bash is the manager for the call the script to connect to LP APIs and try to retrieve something
#   back from them.
#
# Dependencies:
#   For dependencies, please check the README file that comes with this software package.
#
# Configuration file:
#   Please check config.navomi
#
# Arguments (all of them are mandatory, less SOCKET):
#   -ip     : IP of the TCP server
#   -port   : Port of the TCP service
#   -buffer : Buffer for receiving data
#   -f      : file containing the up-to-date agent-state values (maintaing by iex_rta.py)
#   -V      : debug level (0:off, 1:basic, 2:verbose)
#   -dn     : database name
#   -dp     : database port
#   -du     : database user
#   -dw     : database password
#   -socket : database socket
#
# Command to call in bash
# $ python2.7 listener.py -ip 127.0.0.1 -port 5005 -buffer 1024 -f agent-state.csv -V 2 -dn iex_rta -dp 5432 -du navomi -dw NavomiSQL -socket 0

###
### ARGUMENTS PARSING
###

# First check if they are there
if [ $# -ne 2 ]; then
    echo "*** ERROR: arguments missing.";
    echo "";
    echo "    Usage:";
    echo "    ./listener.sh <CONF> <DEBUG>";
    echo "      <CONF>  path/to/config.file";
    echo "      <DEBUG> Debug level (0:None, 1:Basic, 2:Verbose)";
    echo "";
    echo "    Arguments have to follow this specific order.";
    echo "    Check README file for more information.";
    echo "    Exiting...";
    exit;
fi;

# Check configuration file
if [ ! -f $1 ]; then
    echo "*** ERROR: cannot open configuration file.";
    echo "    $1 not found.";
    echo "    Exiting...";
    exit;
else
    # Loads configuration
    source $1;
fi;

# Check Debug level option
if [ $2 -eq 0 ]; then # 0 for none
    _debug_level=0
elif [ $2 -eq 1 ]; then # 1 for basic
    _debug_level=1
elif [ $2 -eq 2 ]; then # 2 for verbose
    _debug_level=2
else
    echo "*** ERROR: invalid debug option.";
    echo "    Use 0:None, 1:Basic or 2:Verbose";
    echo "    Exiting...";
    exit;
fi;


###
### VERIFY INFORMATION IN CONFIG.NAVOMI
###

# Debug
if [ $_debug_level -eq 2 ]; then
    echo ". Verifying and validating configuration file ..."
fi;

# Check if config file exists
if [ ! -f $_config_file_path ]; then
    echo "*** ERROR: configuration file cannot be found.";
    echo "    Informed path is: $_config_file_path";
    echo "    Exiting...";
    exit;
else
    if [ $_debug_level -ge 1 ]; then
        echo ".. configuration file found..."
    fi;
fi;

# Input file containing the agent-to-employee IDs mapping
if [ -z $_input_agent_employee ]; then
  _input_agent_employee="./agent_employee_map.csv";
  if [ $_debug_level -eq 2 ]; then
    echo ".. input file for agent-to-employee mapping not informed, set to default...";
  fi;
fi;
# Create input file if it doesn't exist
if [ ! -f $_input_agent_employee ]; then
  touch $_input_agent_employee;
  if [ $_debug_level -eq 2 ]; then
    echo ".. input file for agent-to-employee $_input_agent_employee created...";
  fi;
fi;

# Check if socket file exists
if (echo $_db_socket | egrep '[^0-9]' &> /dev/null) then # if variable is not numeric
    if [ ! -e $_db_socket ]; then
        echo "*** ERROR: socket file cannot be found.";
        echo "    Informed path is: $_db_socket";
        echo "    Exiting...";
        exit;
    else
        if [ $_debug_level -ge 1 ]; then
            echo ".. socket file found..."
        fi;
    fi;
else
    if [ $_db_socket -ne 0 ]; then
        echo "*** ERROR: socket file cannot be a number not equal 0.";
        echo "    Informed path is: $_db_socket";
        echo "    Exiting...";
        exit;
    fi;
fi;

###
### Exec the listener.py with all arguments
###
source proxies.txt
exec python2.7 listener.py \
            -ip $_conf_tcp_ip \
            -port $_conf_tcp_port \
            -buffer $_conf_tcp_buffer_size \
            -f $_file_agent_state \
			-e $_input_agent_employee \
            -V $_debug_level \
            -dn $_db_name \
            -dp $_db_port \
            -du $_db_user \
            -dw $_db_pw \
            -socket $_db_socket


