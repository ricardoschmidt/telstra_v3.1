#!/bin/bash
#
# Author:
#   Ricardo de O. Schmidt
#     NAVOMI.com Cloud Solutions
#
# Date:
#   October 4, 2017
#
# Contact:
#   ricardo.schmidt@navomi.com
#
# Description:
#   This is a bash script that defines a command line interface to manipulate
#		values that are stored in the concurrency factor file. The location of
#		this file is given in the configuration file.
#

###
### MACROS
###

__LOG_FILE="log";
__ERROR_FILE="error";


###
### ARGUMENTS PARSING
###

# First check if they are there
if [ $# -ne 1 ]; then
  echo "*** ERROR: arguments missing.";
  echo "";
  echo "    Usage:";
  echo "    ./loop.sh <CONF>";
  echo "      <CONF>  path/to/config.file";
  echo "";
  echo "    Exiting...";
  exit;
fi;

# Check configuration file
if [ ! -f $1 ]; then
  echo "*** ERROR: cannot open configuration file.";
  echo "    $1 not found.";
  echo "    Exiting...";
  exit;
else
  # Loads configuration
  source $1;
fi;

# Input file containing the concurrency factors for all skills (comming from configuration file)
if [ -z $_input_concurrency_factor ]; then
	_input_concurrency_factor="./concurrency_factor.csv"
	if [ $_debug_level -eq 2 ]; then
		echo "    .. Input file for concurrency factors not informed, set to default...";
	fi;
fi;
# Create input file if it doesn't exist
if [ ! -f $_input_concurrency_factor ]; then
	touch $_input_concurrency_factor;
	if [ $_debug_level -eq 2 ]; then
		echo "    .. Input file for concurrency factor $_input_concurrency_factor created...";
	fi;
fi;



_key=1
while [ $_key -ne 0 ]; do

	echo "";
	echo "Choose an action:";
	echo "	(0) Exit";
	echo "	(1) SET the concurrency factor value for a given skill";
	echo "	(2) DROP the concurrency factor value for a given skill";
	echo "	(3) GET the concurrency factor value for a given skill";
	echo "";
	echo -n "Your choice: ";
	read _key;

	if [ $_key -eq 0 ]; then
		echo "Exiting...";
		echo "";
		exit;
	elif [ $_key -eq 1 ]; then
		# Set the value for a given skill ID
		echo -n "For Skill ID: ";
		read _skill;
	elif [ $_key -eq 2 ]; then
		# Drop the value for a given skill ID
		echo -n "For Skill ID: ";
		read _skill;
	elif [ $_key -eq 3 ]; then
		# Get the value for a given skill ID
		echo -n "For Skill ID: ";
		read _skill;
	fi;


done;




















