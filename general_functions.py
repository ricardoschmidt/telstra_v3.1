#!/usr/bin/env python
#
#	Author:
#		Ricardo de O. Schmidt
#			NAVOMI.com Cloud Solutions
#
#	Date:
#		February 12, 2018
#
#	Contact:
#		ricardo.schmidt@navomi.com
#
# Description:
#   This script only contains general functions.
#



# Python library with general functions
import general_definitions as GenDefinitions



##
## Function to parse arguments, returning a dictionary called args
def parseArgs(_argv):

	# Start an empty dictionary to store arguments
	_args = {}

	# Iterate through all arguments, removing each of them once processed
	while _argv:

		# Check if this is a "name value"
		if _argv[0][0] == '-':
			# Save the "name value" as index and its respective argument value
			_args[_argv[0]] = _argv[1]

		# Remove the first element of the argument array
		_argv = _argv[1:]

	# Return the dictionary
	return _args



##
## Function to write to application log
def writeToAppLog(_log_path, _content):

	# Open application log file and append content
	_log_path_file = _log_path + "application.log"

	# Open file and write content in it
	with open(_log_path_file, 'a') as _log_file:
		_log_file.write(_content)
		_log_file.close()

	return








