#!/usr/bin/env python
#
# Ricardo de O. Schmidt
# September 18, 2017
#
# Description:
#   This is a basic script to connect to LP APIs and try to retrieve something
#   back from them. See in-line comments to know which API the following
#   commands relate to.
#
#
# Arguments (all of them are mandatory, less SOCKET):
#   -ip     : IP of the TCP server
#   -port   : Port of the TCP service
#   -buffer : Buffer for receiving data
#   -f      : file containing the up-to-date agent-state values (maintaing by iex_rta.py)
#   -V      : debug level (0:off, 1:basic, 2:verbose)
#   -dn     : database name
#   -dp     : database port
#   -du     : database user
#   -dw     : database password
#   -socket : database socket
#


import imports_lib
import requests
from requests_oauthlib import OAuth1
import json
import cgi
import sys
import time
import datetime
import os
import xml.etree.cElementTree as et
from pathlib import Path
import pg8000
import socket


###
### Macros
###

# IMPORTANT: watch for consistency with file extension defined in ftp_connect.py
_M_SERV_NAME_USERS_API = "accountConfigReadOnly"  # Service name for LP Users API
_M_SERV_NAME_REAL_TIME_API = "leDataReporting"    # Service name for LP Realtime Operational API

_M_MSG_SESSION_LOGIN          = "Login"     # Session Login message
_M_MSG_INITIALIZATION         = "Init"      # Session Initialization message
_M_MSG_AGENT_STATUS_REQUEST   = "StatReq"   # Agent Status Request message
_M_MSG_AGENT_STATUS_DOWNLOAD  = "AgtStats"  # Agent Status Download message
_M_MSG_AGENT_STATUS_ACK       = "StatAck"   # Agent Status Acknowledgement message
_M_MSG_AGENT_STATUS_CHANGE    = "StatChg"   # Agent Status Change message
_M_MSG_HEARTBEAT_REQUEST      = "BeatReq"   # Heartbeat Request message
_M_MSG_HEARTBEAT_RESPONSE     = "HrtBeat"   # Heartbeat Response message
_M_MSG_SESSION_TERMINATION    = "Term"      # Session Terminantion message
_M_MSG_REJECT                 = "Reject"    # Message Rejection message

_M_MODE_SNAPSHOT      = "Snapshot"
_M_MODE_ASYNCHRONOUS  = "AS"

_M_TIME_UNIVERSAL = "UT"
_M_TIME_SWITCH    = "Switch"
_M_TIME_LOCAL     = "Local"

_M_HEARTBEAT_VALUE = 0  # Default set to zero, meaning no heartbeat

_M_STATE_ONLINE_ID    = 2
_M_STATE_BACKIN5_ID   = 3
_M_STATE_AWAY_ID      = 4

_M_STATE_ONLINE_NAME  = "Online"
_M_STATE_BACKIN5_NAME = "Back in 5"
_M_STATE_AWAY_NAME    = "Away"

_session_msg_number = {}  # Global counter to control session message numbers
_session_msg_request = {} # Global counter to control status request message numbers


###
### Function to parse arguments, returning a dictionary called args
def parseArgs(argv):
	args = {} # Start an empty dictionary to store arguments
	while argv: # Iterate through all arguments, removing each of them once processed
		if argv[0][0] == '-': # This is a "name value"
			args[argv[0]] = argv[1] # Save the "name value" as index and its respec arg value
		argv = argv[1:] # Remove the first element of the arg array
	return args # Return the dictionary



###
### Function to return the list of active agents
### in response to an Agent Status Request (StatReq)
### The return is the current content from agent-state.csv
def getAgentStatusDownload(_file):

	_agent_count = 0
	_msg = ""

    # Check if the file exists
	_agent_state_file = Path(_file)
	if _agent_state_file.is_file():

		# Open the agent to employee ID mapping (origi
		_agent_employee_id = {}
		with open(_myargs['-e'], 'r') as _ae_file:
			for _line in _ae_file:
				if len(_line) > 0:
					_agent_employee_id[str(_line.split(',')[0])] = str(_line.split(',')[1]).rstrip()
		# Close file
		_ae_file.close()

        # Read line by line and save values into message to be sent back to the TCP client
		with open(_file) as _state_file:

			for _line in _state_file:

				# Get employee ID
				if str(_line.split(',')[0]) in _agent_employee_id:

					_agent_count += 1

					# Second column is last request time
					_change_datetime = str(_line.split(',')[1])

					# Check if the date is complete (13 characters)
					if len(_change_datetime) == 13:
						_change_date = str(_change_datetime.split('.')[0])
						_change_time = str(_change_datetime.split('.')[1])
					else:
						_change_date = '0' + str(_change_datetime.split('.')[0])
						_change_time = str(_change_datetime.split('.')[1])

					# Build the records list into the message body
					if _agent_count == 1:
						#_msg = str(_line.split(',')[0]) + "|"
						_msg = str(_agent_employee_id[str(_line.split(',')[0])]) + "|" + \
							   str(_line.split(',')[2]) + "|" + \
							   _change_time + "|" + \
							   _change_date[0:4] + "20" + _change_date[4:6]
					else:
						_msg = _msg + "," + \
							   str(_agent_employee_id[str(_line.split(',')[0])]) + "|" + \
							   str(_line.split(',')[2]) + "|" + \
							   _change_time + "|" + \
							   _change_date[0:4] + "20" + _change_date[4:6]

            # Close file
			_state_file.close()

    # Return values for the response message
	return _agent_count, _msg



###
### Function to return the list of agent state changes
### in response to an Agent Status Change (StatChg)
### The return is the last 30 seconds of records in the database
def getAgentStatusChange(_last_ts, _db_cursor):

	_agent_count = 0
	_msg = ""

    # Run a select command for all agents within the last 30 seconds
	try:
		_db_cursor.execute("""SELECT agent_id, state_id, timestamp, datetime FROM STATE WHERE timestamp>""" + str(_last_ts))
	except:
		print "*** WARNING: Not able to fetch data from database"
		print "             No agent data to report"
		return _agent_count, _msg

    # Go through all rows returned from database and build the return message body
	_rows = _g_db_cursor.fetchall()

	for _row in _rows:
		_agent_count += 1

        # Fourth column is the last request time
		_change_datetime = str(_row[3])

		# Check if the date is complete (13 characters)
		if len(_change_datetime) == 13:
			_change_date = str(_change_datetime.split('.')[0])
			_change_time = str(_change_datetime.split('.')[1])
		# Date is missing a starting zero (0) for the month
		else:
			_change_date = '0' + str(_change_datetime.split('.')[0])
			_change_time = str(_change_datetime.split('.')[1])

        # Build the records list into the message body
		if _agent_count == 1:
			_msg = str(_row[0]) + "|" + \
                   str(_row[1]) + "|" + \
                   _change_time + "|" + \
                   _change_date[0:4] + "20" + _change_date[4:6]
		else:
			_msg = _msg + "," + \
                   str(_row[0]) + "|" + \
                   str(_row[1]) + "|" + \
                   _change_time + "|" + \
                   _change_date[0:4] + "20" + _change_date[4:6]

    # Close connection to the database
    #_db_cursor.close()
    #_db_conn.close()

    # Return values for the response message
	return _agent_count, _msg


###
### Function to build header
def getHeader(_msg_type, _msg_number, _record_count):

	_timestamp = time.strftime('%m%d%Y%H%M%S')

	_header = "RTA|" + \
			  _msg_type + "|" + \
			  str(_msg_number) + "|" + \
			  _timestamp + "|" + \
			  str(_record_count)

	return _header


###
### Function to listen to the TCP port connection
def listenConn(_tcp_ip, _tcp_port, _buffer_size, _db_cursor):
	global _session_msg_number
	global _session_msg_request

    # Used to control the last request time, so that we send all the state changes since the last request
	_last_req_ts = 0

	try:
        # Create socket
		_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # Overcome/ignore the issue of port being already in use
		_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	except socket.error as _msg:
        # TODO - save to error log
		print "*** ERROR: Not possible to set up socket"
		print "           Exiting..."
		sys.exit()

	try:
        # Initialize socket and start listening
		_socket.bind((_tcp_ip, _tcp_port))
		_socket.listen(15)
	except socket.error as _error:
        # TODO - save to error log
		print "*** ERROR: Not possible to connect/listen on socket"
		print "           Exiting..."
		print _error
		sys.exit()

    # Accept incoming connections
	_conn, _addr = _socket.accept()

	_session_id = str(_addr[0]) + "," + str(_addr[1])

	while 1:
		_data = _conn.recv(_buffer_size)
		if not _data:
			break

        # Split the header from the rest of the message
		_recv_header = _data.split(',')[0]

        # Recover the message type
		_recv_type = _recv_header.split('|')[1]

        # Recover the body of the message
		_recv_body = _data[len(_recv_header)+1:len(_data)]

        ##
        ## SESSION LOGIN MESSAGE
		if _recv_type.upper() == _M_MSG_SESSION_LOGIN.upper():
			print "Client " + _session_id + " is requesting to login, send back a session initialization message"

            # Check if there is a session for this address already and if so print a warning message
			if _session_id in _session_msg_number:
				print "*** WARNING: There is a running session for the address " + _session_id
				print "             Another session WILL NOT be initiated and the message number will be kept"
			else:
				_session_msg_number[_session_id] = 1
				_session_msg_request[_session_id] = 0

            # Increment message number for this session
			_session_msg_number[_session_id] += 1

            # Get header
			_header = getHeader(_M_MSG_INITIALIZATION.upper(), _session_msg_number[_session_id], 1)

            # Build return message
			_return_message = ""
			_return_message = _header + "," + \
                              str(_M_MODE_SNAPSHOT) + "|" + \
                              str(_M_TIME_LOCAL) + "|" + \
                              str(_M_HEARTBEAT_VALUE) + ","

			print _return_message

            # Send return message back to the client
			_conn.sendall(_return_message)

        ##
        ## SESSION TERMINATION MESSAGE
		elif _recv_type.upper() == _M_MSG_SESSION_TERMINATION.upper():
			print "Client " + _session_id + " wishes to terminate the ongoing session"

            # Check if the session is still active
			if _session_id in _session_msg_number:
				del _session_msg_number[_session_id]
				del _session_msg_request[_session_id]

        ##
        ## AGENT STATUS REQUEST MESSAGE
		elif _recv_type.upper() == _M_MSG_AGENT_STATUS_REQUEST.upper():

			print "Client " + _session_id + " is requesting Agent Status"

            # First check if there is a session for the given _session_id, if not create one
			if _session_id not in _session_msg_number:
				print "*** WARNING: This request has been received without a proper session initialization"
				print "             There is no session for " + _session_id + ". Creating one..."
				_session_msg_number[_session_id] = 0
				_session_msg_request[_session_id] = 0
                # TODO - Perhaps it should not continue and send some sort of error message instead
                #        But for now, just continue without sending a session initialization back to the client

            # Increment the message and request counts for this session
			_session_msg_number[_session_id] += 1
			_session_msg_request[_session_id] += 1

            # If session request count equals 1, we have to return the list of all active agents and their current states
			if _session_msg_request[_session_id] == 1:
				print "Calling getAgentStatusDownload"
                # Call function to build a message containing the state of all active agents
                # This information comes from the agent-state.csv file (or another name as defined in the config file)
                # The name of the agent-state file is given as argument to this script
				_record_count = 0
				_records = ""
				_record_count, _records = getAgentStatusDownload(str(_myargs['-f']))

                # Get header
				_header = getHeader(_M_MSG_AGENT_STATUS_DOWNLOAD.upper(), _session_msg_number[_session_id], _record_count)

                # Set the last timestamp
				_last_req_ts = int(time.strftime('%s'))

            # The session request count is bigger than 1, we have to return only those agents that changed state in the last 30 seconds
			else:
                # Immediately saves the request time
				_temp_last_req_ts = int(time.strftime('%s'))

				print "Calling getAgentStatusChange"
                # Call fucntion to build a message containing the state of only those agents that change state in the last 30 seconds.
                # This information comes from the database (informed int he config file)
				_record_count = 0
				_records = ""
                #_record_count, _records = getAgentStatusChange(_myargs['-dn'], _myargs['-dp'], _myargs['-du'], _myargs['-dw'], _last_req_ts, _myargs['-socket'])
				_record_count, _records = getAgentStatusChange(_last_req_ts, _db_cursor)

                # Get header and build return message with values returned from getAgentStatusChange
				_header = getHeader(_M_MSG_AGENT_STATUS_CHANGE.upper(), _session_msg_number[_session_id], _record_count)

                # After using for the request, update the last request timestamp
				_last_req_ts = _temp_last_req_ts


            # Build return message joining header and body with values
			_return_message = ""
			# If no recors, return just the header
			if _record_count == 0:
				_return_message = _header
            # Otherwise return the header + records
			else:
				_return_message = _header + "," + _records + ","

			print _return_message

            # If slistenConnuccessful, return accumulated values in _records, otherwise empty/error message
			if len(_records) > 0:
				print "Returning " + str(_record_count) + " agents in the Agent Status Download (AgtStats) message"
			else:
				print "*** WARNING: There is no agent state information available"
				print "             Returning empty message (only header)"
                # TODO - should we implement an error message in here or returning empty response is sufficient

            # Send return message back to the client
			_conn.sendall(_return_message)

        ##
        ## AGENT STATUS ACKNOWLEDGE MESSAGE
		elif _recv_type.upper() == _M_MSG_AGENT_STATUS_ACK.upper():
			print "Just locally confirm that those packets/messages were received"
            # TODO - should we implement a retry method?

        ##
        ## INVALID MESSAGE -- NO MESSAGE TYPE RECEIVED FROM CLIENT
		elif len(_recv_type) == 0:
			print "*** ERROR: Message is empty"
			print "			  Do not expect reply to it..."

        ##
        ## INVALID MESSAGE -- MESSAGE TYPE NOT RECOGNIZED
		else:
			print "*** ERROR: Message type %s is not valid" % _data
			print "			  Do not expect reply to it..."


    # Close the connection
    # TODO - use the proper message exchange to terminate the connection
	_conn.close()
	return



def getDbCursor(_db_name, _db_port, _db_user, _db_pw, _db_socket):

	_msg = ""

    # Connect to the database
	try:
		if _db_socket == '0':
			_db_conn = pg8000.connect(database=_db_name, user=_db_user, password=_db_pw, port=int(_db_port))
		else:
			_db_conn = pg8000.connect(database=_db_name, user=_db_user, password=_db_pw, port=int(_db_port), unix_sock=_db_socket)
	except pg8000.Error as _msg:
		print "*** ERROR: Not able to connect to database"
		print "           No agent data to report"
		print _msg
		return ""
	finally:
		if _debug_level == 2:
			print "   .. database connection success"

    # Open a cursor to interact with DB
	_db_cursor = _db_conn.cursor()

	return _db_cursor


###
### START OF THE ACTUAL SCRIPT
###


# Call parseArgs and store arguments in the dictionary _myargs
_myargs = {}
_myargs = parseArgs(sys.argv)
_debug_level = _myargs['-V']

## Connect to the database and open a cursor to interact with DB

# global _g_db_cursor
_g_db_cursor = getDbCursor(_myargs['-dn'], _myargs['-dp'], _myargs['-du'], _myargs['-dw'], _myargs['-socket'])

## Create the base name for the log files
#_base_log_file_name = _myargs['-l'] + time.strftime('%Y%m%d%H%M')
#
## Build the authorization set of keys to be used from here on
#_auth = OAuth1(str(_myargs['-k']), str(_myargs['-s']), str(_myargs['-t']), str(_myargs['-T']))


# In an infinite loop, listen to incoming connections in port _conn_port
while 1:
	listenConn(str(_myargs['-ip']), int(_myargs['-port']), int(_myargs['-buffer']), _g_db_cursor)

