#!/bin/bash
#
# Author:
#   Ricardo de O. Schmidt
#     NAVOMI.com Cloud Solutions
#
# Date:
#   October 4, 2017
#
# Contact:
#   ricardo.schmidt@navomi.com
#
# Description:
#   This is a Bash script that, based on information in a configuration file and
#   input via command line, calls the Python script 15_min_reports.py to
#   generate 15-min report files for a given period, starting no longer than 24
#   hours in the past. The 24-hour limit is due to data availability of
#   LivePerson Operational Realtime API.
#
#   This script executes a series of steps:
#     1.  Makes sure all arguments and pertinent information is found in the
#         configuration file.
#     2.  Calls the 15_min_reports.py Python script as many times as needed to
#         generate the report files for the given date/time interval.
#     3.  Stores the report files within the folder given in the command line.
#
# Input arguments (all mandatory and in order):
#   1.  Path/to/CONFIG_FILE containing configuration information.
#       Default file is config.navomi
#   2.  S/FTP option for getting the reports to the customer; options are:
#         1: PUSH
#         2: PULL
#   3.  Operational System in which this script will run; options are:
#         1: Mac OS
#         2: Linux
#   4.  Debug option; options are:
#         0: None
#         1: Basic
#         2: Verbose
#   5.  Path/to/OUTPUT_REPORT_FILE/, where reports will be stored.
#       Default is ./rerun_reports/
#   6.  Start time of the interval for report generation; that is, time of the
#       first report to be generated.
#       Format for start time is MMDDYY.hhmm (UTC time)
#   7.  End time of the interval for report generation; that is, time of the
#       last report to be generated.
#       NOTE: for a single report, start time equals end time.
#       Format for end time is MMDDYY.hhmm (UTC time)
#
# Running:
#   For running instructions please refer to the README file.
#

###
### ARGUMENTS PARSING
###

# First check if they are ther7 if [ $# -ne 5 ]; then
if [ $# -ne 7 ]; then
  echo "";
  echo "    Usage:";
  echo "    ./rerun.sh <CONF> <FTP> <OS> <DEBUG> <OUTPUT PATH> <START TIME> <END TIME>";
  echo "      <CONF>          Path/to/config.file";
  echo "      <FTP>           FTP option (1:Push, 2:Pull)";
  echo "      <OS>            OS type (1:Mac OS, 2:Linux)";
  echo "      <DEBUG>         Debug level (0:None, 1:Basic, 2:Verbose)";
  echo "      <OUTPUT PATH>   Output directory to store reports (has to be different than the one in config.file";
  echo "      <START TIME>    Start time for the report generation (less than 24 hours back in time). Format MMDDYY.hhmm (UTC)";
  echo "      <END TIME>      End time for the report generation (less than 24 hours back in time). Format MMDDYY.hhmm (UTC)";
  echo "";
  echo "    Arguments have to follow this specific order.";
  echo "    Check README file for more information.";
  echo "    Exiting...";
  exit;
fi;

# Check configuration file
if [ ! -f $1 ]; then
  echo "*** ERROR: cannot open configuration file.";
  echo "    $1 not found.";
  echo "    Exiting...";
  exit;
else
  # Loads configuration
  source $1;
fi;

# Check FTP option
if [ $2 -eq 1 ]; then # 1 for PUSH
  _ftp_option="PUSH";
elif [ $2 -eq 2 ]; then # 2 for PULL
  _ftp_option="PULL";
else
  echo "*** ERROR: invalid FTP option.";
  echo "    Use 1:Push or 2:Pull";
  echo "    Exiting...";
  exit;
fi;

# Check OS type
if [ $3 -eq 1 ]; then # 1 for Mac OS
  _os="MacOS";
elif [ $3 -eq 2 ]; then # 2 for Linux
  _os="Linux";
else
  echo "*** ERROR: invalid OS option.";
  echo "    Use 1:Mac OS or 2:Linux";
  echo "    Exiting...";
  exit;
fi;

# Check Debug level option
if [ $4 -eq 0 ]; then # 0 for none
  _debug_level=0
elif [ $4 -eq 1 ]; then # 1 for basic
  _debug_level=1
elif [ $4 -eq 2 ]; then # 2 for verbose
  _debug_level=2
else
  echo "*** ERROR: invalid debug option.";
  echo "    Use 0:None, 1:Basic or 2:Verbose";
  echo "    Exiting...";
  exit;
fi;

# Check if output directory exists
if [ ! -d $5 ]; then
  echo "** Warning: outputh directory $5 not found.";
  echo "   Creating default ./rerun_reports";
  mkdir rerun_reports;
  _output_path_report="./rerun_reports/";
else
  _output_path_report=$5;
fi;

# Check informed time, different command date sintax depending on OS
if [ $_os == "MacOS" ]; then  # Mac OS
  # Convert start and end times to epoch
  _start_ts=`date -j -f "%m%d%y.%H%M%S" ${6}00 +%s`;
  _end_ts=`date -j -f "%m%d%y.%H%M%S" ${7}00 +%s`;
elif [ $_os == "Linux" ]; then # Linux
  # Convert start and end times to epoch
  _start_nice=`echo $6 | awk '{print substr($1,1,2)"/"substr($1,3,2)"/"substr($1,5,2)" "substr($1,8,2)":"substr($1,10,2)":00";}'`;
  _start_ts=`date -d "$_start_nice" +%s`;
  _end_nice=`echo $7 | awk '{print substr($1,1,2)"/"substr($1,3,2)"/"substr($1,5,2)" "substr($1,8,2)":"substr($1,10,2)":00";}'`;
  _end_ts=`date -d "$_end_nice" +%s`;
fi;

# Check if end time is bigger than start time
if [ $_start_ts -gt $_end_ts ]; then
  echo "*** ERROR: start time has to be smaller than end time.";
  echo "    Exiting...";
  exit;
fi;

# Check if start or end time is more than 24 hours back in time
_current_ts=`date +%s`;
let _diff_start=$_current_ts-$_start_ts;
let _diff_end=$_current_ts-$_end_ts;
_max_diff="86400";

# Check if the difference between current time and informed start time is less than 86400 sec (ie 24 hours)
if [ $_diff_start -ge $_max_diff ] || [ $_diff_end -ge $_max_diff ]; then
  echo "*** ERROR: start and end time have to be less than 24h back in time.";
  echo "    Exiting...";
  exit;
fi;

# Check if informed start or end time are in the future
if [ $_start_ts -gt $_current_ts ] || [ $_end_ts -gt $_current_ts ]; then
  echo "*** ERROR: start and end time CANNOT be in the future.";
  echo "    Exiting...";
  exit;
fi;

# Check the applog output path
if [ -z $_output_path_applog_rerun ]; then
    _output_path_applog_rerun="./app_logs_postday/";
    if [ $_debug_level -eq 2 ]; then
        echo "    .. Output path for app log files not informed, set to default...";
    fi;
fi;
if [ ! -d $_output_path_applog_rerun ]; then
    mkdir $_output_path_applog_rerun;
    if [ $_debug_level -eq 2 ]; then
        echo "    .. Output folder for app log files created...";
    fi;
fi;


###
### Find out how many reports it has to generate
###

_diff_start_end=$(expr $_end_ts - $_start_ts);
_tot_rep=$(expr $_diff_start_end / 60 / 15 + 1);
if [ $_debug_level -eq 2 ]; then
	echo "==========================================================================="
	echo "Staring the generation of $_tot_rep reports"
fi;


# Start counter
echo "==========================================================================="
_rep_generated=0;
_report_generated_count=0
while [ $_report_generated_count -ne $_tot_rep ]; do


	if [ $_os == "MacOS" ]; then

		# Generation period
		_start_dt=`date -r $_start_ts +%m%d%y.%H%M`
		_start_hr=`date -r $_start_ts "+%Y-%m-%d.%H:%M:%S"`
		_end_dt=`date -r $_end_ts +%m%d%y.%H%M`
		_end_hr=`date -r $_end_ts "+%Y-%m-%d.%H:%M:%S"`


		# Calculate how many seconds we are ahead of call time
		_seconds_to_sum=`echo $_report_generated_count | awk '{print $1*900;}'`

		# Calculate report time
		_report_time_ts=`date -j -f '%s' -v +${_seconds_to_sum}S $_start_ts +%s`
		_report_time_dt=`date -j -f '%s' -v +${_seconds_to_sum}S $_start_ts +%m%d%y.%H%M`
		_report_time_hr=`date -j -f '%s' -v +${_seconds_to_sum}S $_start_ts "+%Y-%m-%d.%H:%M:%S"`

		# Compute report name
		_report_name=`date -r $_report_time_ts "+%m%d%y.%H%M"`

		# Compute OR API buckets
		_or_api_bucket_1_ts=`date -j -f '%s' -v +5M $_report_time_ts +%s`
		_or_api_bucket_1_dt=`date -r $_or_api_bucket_1_ts +%m%d%y.%H%M`
		_or_api_bucket_1_hr=`date -r $_or_api_bucket_1_ts "+%Y-%m-%d.%H:%M:%S"`
		_or_api_bucket_2_ts=`date -j -f '%s' -v +10M $_report_time_ts +%s`
		_or_api_bucket_2_dt=`date -r $_or_api_bucket_2_ts +%m%d%y.%H%M`
		_or_api_bucket_2_hr=`date -r $_or_api_bucket_2_ts "+%Y-%m-%d.%H:%M:%S"`
		_or_api_bucket_3_ts=`date -j -f '%s' -v +15M $_report_time_ts +%s`
		_or_api_bucket_3_dt=`date -r $_or_api_bucket_3_ts +%m%d%y.%H%M`
		_or_api_bucket_3_hr=`date -r $_or_api_bucket_3_ts "+%Y-%m-%d.%H:%M:%S"`

		# Compute the OR API timeframe
		_now_ts=`date +%s`
		_or_api_timeframe=`echo $_now_ts $_report_time_ts | awk '{_t=int(($1-$2)/60); _tr=int(_t/5)*5; print _tr;}'`

		# Compute EH API interval
		_eh_api_from_ts=$_report_time_ts
		_eh_api_from_dt=$_report_time_dt
		_eh_api_from_hr=$_report_time_hr
		_eh_api_to_ts=`date -j -f '%s' -v +899S $_eh_api_from_ts +%s`
		_eh_api_to_dt=`date -r $_eh_api_to_ts +%m%d%y.%H%M`
		_eh_api_to_hr=`date -r $_eh_api_to_ts "+%Y-%m-%d.%H:%M:%S"`

	elif [ $_os == "Linux" ]; then

		# Generation period
		_start_dt=`date -d @$_start_ts +%m%d%y.%H%M`
		_start_hr=`date -d @$_start_ts "+%Y-%m-%d.%H:%M:%S"`
		_start_str=`date -d @$_start_ts`
		_end_dt=`date -d @$_end_ts +%m%d%y.%H%M`
		_end_hr=`date -d @$_end_ts "+%Y-%m-%d.%H:%M:%S"`

		# Calculate how many seconds we are ahead of call time
		_seconds_to_sum=`echo $_report_generated_count | awk '{print $1*900;}'`

		# Calculate report time
		_report_time_ts=`date -d "$_start_str+${_seconds_to_sum} seconds" +%s`
		_report_time_dt=`date -d "$_start_str+${_seconds_to_sum} seconds" +%m%d%y.%H%M`
		_report_time_hr=`date -d "$_start_str+${_seconds_to_sum} seconds" "+%Y-%m-%d.%H:%M:%S"`
		_report_time_str=`date -d @$_report_time_ts`

		# Compute report name
		_report_name=`date -d @$_report_time_ts "+%m%d%y.%H%M"`

		# Compute OR API buckets
		_or_api_bucket_1_ts=`date -d "$_report_time_str+5 minutes" +%s`
		_or_api_bucket_1_dt=`date -d @$_or_api_bucket_1_ts +%m%d%y.%H%M`
		_or_api_bucket_1_hr=`date -d @$_or_api_bucket_1_ts "+%Y-%m-%d.%H:%M:%S"`
		_or_api_bucket_2_ts=`date -d "$_report_time_str+10 minutes" +%s`
		_or_api_bucket_2_dt=`date -d @$_or_api_bucket_2_ts +%m%d%y.%H%M`
		_or_api_bucket_2_hr=`date -d @$_or_api_bucket_2_ts "+%Y-%m-%d.%H:%M:%S"`
		_or_api_bucket_3_ts=`date -d "$_report_time_str+15 minutes" +%s`
		_or_api_bucket_3_dt=`date -d @$_or_api_bucket_3_ts +%m%d%y.%H%M`
		_or_api_bucket_3_hr=`date -d @$_or_api_bucket_3_ts "+%Y-%m-%d.%H:%M:%S"`

		# Compute the OR API timeframe (rounded to 5)
		_now_ts=`date +%s`
		_or_api_timeframe=`echo $_now_ts $_report_time_ts | awk '{_t=int(($1-$2)/60); _tr=int(_t/5)*5; print _tr;}'`

		# Compute EH API interval
		_eh_api_from_ts=$_report_time_ts
		_eh_api_from_dt=$_report_time_dt
		_eh_api_from_hr=$_report_time_hr
		_eh_api_to_ts=`date -d "$_report_time_str+899 seconds" +%s`
		_eh_api_to_dt=`date -d @$_eh_api_to_ts +%m%d%y.%H%M`
		_eh_api_to_hr=`date -d @$_eh_api_to_ts "+%Y-%m-%d.%H:%M:%S"`

	fi;

	# Print debug message informing times to be used in the generation of reports
	if [ $_debug_level -eq 2 ]; then
		#echo "==========================================================================="
		echo "Generation period: $_start_hr [$_start_dt] ($_start_ts)"
		echo "                 : $_end_hr [$_end_dt] ($_end_ts)"
		echo "Report time      : $_report_time_hr [$_report_time_dt] ($_report_time_ts)"
		echo "Report name      : *_$_report_name.xml"
		echo "OR API buckets   : $_or_api_bucket_1_hr [$_or_api_bucket_1_dt] ($_or_api_bucket_1_ts)"
		echo "                   $_or_api_bucket_2_hr [$_or_api_bucket_2_dt] ($_or_api_bucket_2_ts)"
		echo "                   $_or_api_bucket_3_hr [$_or_api_bucket_3_dt] ($_or_api_bucket_3_ts)"
		echo "OR API timeframe : $_or_api_timeframe"
		echo "EH API interval  : $_eh_api_from_hr [$_eh_api_from_dt] ($_eh_api_from_ts)"
		echo "                   $_eh_api_to_hr [$_eh_api_to_dt] ($_eh_api_to_ts)"
		echo "==========================================================================="
	fi;

	# Call the report generation for the given report
	# source proxies.txt
	python2.7 generate_reports.py \
		-a $_lp_api_account \
		-k $_lp_api_key \
        -s $_lp_api_secret \
        -t $_lp_api_token \
        -T $_lp_api_token_secret \
        -m $_lp_api_signature_method \
        -o $_output_path_report \
        -l $_output_path_log \
        -al $_output_path_applog_rerun \
        -f $_or_api_timeframe \
        -b1ts $_or_api_bucket_1_ts \
        -b2ts $_or_api_bucket_2_ts \
        -b3ts $_or_api_bucket_3_ts \
        -b1hr $_or_api_bucket_1_hr \
        -b2hr $_or_api_bucket_2_hr \
        -b3hr $_or_api_bucket_3_hr \
        -i $_api_arg_interval \
        -v $_api_arg_version \
        -A $_api_arg_agent_ids \
        -S $_api_arg_skill_ids \
        -e $_input_agent_employee \
        -d $_or_api_bucket_1_ts \
        -F $_or_api_bucket_3_ts \
        -V $_debug_level \
        -n $_report_name \
        -j $_output_path_json \
        -es $_email_server \
        -ep $_email_port \
        -ew $_email_pw \
        -ef $_email_from_addr \
        -et $_email_to_addr \
        -ej $_email_subject \
        -ehFrom $_eh_api_from_ts \
        -ehTo $_eh_api_to_ts \
        -cf $_input_concurrency_factor \
        -dcf $_default_concurrency_factor \
        -cts $_report_time_ts \
		-postday False \
		-inxml .

	# Increment report counter to be used for generation and number of seconds control
	let _report_generated_count+=1

	echo "==========================================================================="

done;


exit






###
### FIND THE CORRECT TIMESTAMP FOR THE CURRENT REPORT
###

# Check which OS is running, 'cause arguments of command date then change

if [ $_os == "MacOS" ]; then

  # Get dates for Mac OS

  _timestamp=`date +%s`
  _human_time=`date -r $_timestamp +%Y%m%d%H%M`

  _end_time_ts=`echo "$(date -r $_timestamp +%s) - ($(date -r $_timestamp +%s)%900)" | bc`
  _end_time_hr=`echo "$(date -r $_end_time_ts +%Y%m%d%H%M) - ($(date -r $_end_time_ts +%M)%15)" | bc`

  let _start_time_ts=${_end_time_ts}-900
  let _start_time_hr=${_end_time_hr}-15

  let _end_time_ts=${_end_time_ts}-1
  let _end_time_hr=${_end_time_hr}-1

  _cur_time_min=`date -r $_timestamp +%M`
  _end_time_min=`date -r $_end_time_ts +%M`
  let _timeframe=${_cur_time_min}-${_end_time_min}+15

  _name_time=`date -r $_start_time_ts +%H%M`
  _name_date=`date -r $_start_time_ts +%m%d%y`
  _name="XX_${_name_date}.${_name_time}"

  let _start_bucket_ts=${_start_time_ts}+300
  _start_bucket_hr=`date -r $_start_bucket_ts +%Y%m%d%H%M`

  let _end_bucket_ts=${_end_time_ts}+1
  _end_bucket_hr=`date -r $_end_bucket_ts +%Y%m%d%H%M`

elif [ $_os == "Linux" ]; then

  # Get dates for Linux

  _timestamp=`date +%s`
  _human_time=`date -d @$_timestamp +%Y%m%d%H%M`

  _end_time_ts=`echo "$(date -d @$_timestamp +%s) - ($(date -d @$_timestamp +%s)%900)" | bc`
  _end_time_hr=`echo "$(date -d @$_end_time_ts +%Y%m%d%H%M) - ($(date -d @$_end_time_ts +%M)%15)" | bc`

  let _start_time_ts=${_end_time_ts}-900
  let _start_time_hr=${_end_time_hr}-15

  let _end_time_ts=${_end_time_ts}-1
  let _end_time_hr=${_end_time_hr}-1

  _cur_time_min=`date -d @$_timestamp +%M`
  _end_time_min=`date -d @$_end_time_ts +%M`
  let _timeframe=${_cur_time_min}-${_end_time_min}+15

  _name_time=`date -d @$_start_time_ts +%H%M`
  _name_date=`date -d @$_start_time_ts +%m%d%y`
  _name="XX_${_name_date}.${_name_time}"

  let _start_bucket_ts=${_start_time_ts}+300
  _start_bucket_hr=`date -d @$_start_bucket_ts +%Y%m%d%H%M`

  let _end_bucket_ts=${_end_time_ts}+1
  _end_bucket_hr=`date -d @$_end_bucket_ts +%Y%m%d%H%M`

fi;

echo "CUR TIME   : $_timestamp $_human_time"
echo "START TIME : $_start_time_ts $_start_time_hr | $_start_bucket_ts $_start_bucket_hr"
echo "END TIME   : $_end_time_ts $_end_time_hr | $_end_bucket_ts $_end_bucket_hr"
echo "TIMEFRAME  : $_timeframe"
echo "REP NAME   : $_name"



###
### VERIFY INFORMATION IN CONFIG.NAVOMI
###

# Debug
if [ $_debug_level -eq 2 ]; then
  echo ". Verifying and validating configuration file ..."
fi;

# Check if config file exists
if [ ! -f $_config_file_path ]; then
  echo "*** ERROR: configuration file cannot be found.";
  echo "    Informed path is: $_config_file_path";
  echo "    Exiting...";
  exit;
else
  if [ $_debug_level -ge 1 ]; then
    echo ".. configuration file found..."
  fi;
fi;

# Check whether mandatory LP API access credentials are informed in the config file
if [ -z $_lp_api_account ] ||
   [ -z $_lp_api_key ] || \
   [ -z $_lp_api_secret ] || \
   [ -z $_lp_api_token ] || \
   [ -z $_lp_api_token_secret ] || \
   [ -z $_lp_api_signature_method ]; then
  echo "*** ERROR: missing access credentials for the Live Person API.";
  echo "    Please inform these in the configuration file: $_config_file_path";
  echo "    Exiting...";
else
  if [ $_debug_level -eq 2 ]; then
    echo ".. access credentials for LP API found in the config file...";
  fi;
fi;

# Check whether mandatory FTP account access credentials for PUSH are informed in the config file
if [ $_ftp_option == "PUSH" ]; then
  if [ -z $_ftp_host ] || \
     [ -z $_ftp_port ] || \
     [ -z $_ftp_user ] || \
     [ -z $_ftp_pw ]; then
    echo "*** ERROR: missing access credentials for the FTP account.";
    echo "    Please inform these in the configuration file: $_config_file_path";
    echo "    Exiting...";
  else
    if [ $_debug_level -eq 2 ]; then
      echo ".. access credentials for FTP account found in the config file...";
    fi;
  fi;
# Check whether mandatory FTP local directory for PULL is informed and actually exists
elif [ $_ftp_option == "PULL" ]; then
  if [ -z $_ftp_local_dir ]; then
    echo "*** ERROR: missing local directory for PULL FTP.";
    echo "    Please inform these in the configuration file: $_config_file_path";
    echo "    Exiting...";
  fi;
  if [ ! -d $_ftp_local_dir ]; then
    echo "*** ERROR: the informed local directory $_ftp_local_dir for FTP PULL does not exist."
    echo "    Please create the directory and try again."
    echo "    Exiting..."
  fi;
fi;

# Check whether non-mandatory fields were informed and load the default if needed

# Output path for storing reports (XML files)
if [ -z $_output_path_report ]; then
  _output_path_report="./reports/";
  if [ $_debug_level -eq 2]; then
    echo ".. output path for report files not informed, set to default...";
  fi;
fi;
# Create folder for reports if it doesn't exist
if [ ! -d $_output_path_report ]; then
  mkdir $_output_path_report;
  if [ $_debug_level -eq 2 ]; then
    echo ".. output folder for report files created...";
  fi;
fi;

# Path to store the log files
if [ -z $_output_path_log ]; then
  _output_path_log="./logs/";
  if [ $_debug_level -eq 2 ]; then
    echo ".. output path for log files not informed, set to default...";
  fi;
fi;
# Create folder for logs if it doesn't exist
if [ ! -d $_output_path_log ]; then
  mkdir $_output_path_log;
  if [ $_debug_level -eq 2 ]; then
    echo ".. output folder for log files created...";
  fi;
fi;

# Input file containing the agent-to-employee IDs mapping
if [ -z $_input_agent_employee ]; then
  _input_agent_employee="./agent_employee_map.csv";
  if [ $_debug_level -eq 2 ]; then
    echo ".. input file for agent-to-employee mapping not informed, set to default...";
  fi;
fi;
# Create input file if it doesn't exist
if [ ! -f $_input_agent_employee ]; then
  touch $_input_agent_employee;
  if [ $_debug_level -eq 2 ]; then
    echo ".. input file for agent-to-employee $_input_agent_employee created...";
  fi;
fi;

# API arguments
if [ -z $_api_arg_timeframe ]; then _api_arg_timeframe="15"; fi; # Default to 15 minutes, which is the minimum value possible
if [ -z $_api_arg_interval ]; then _api_arg_interval="5"; fi; # Default to 5 minutes, which is the minimum value possible
if [ -z $_api_arg_version ]; then _api_arg_version="1"; fi; # Default to version 1
if [ -z $_api_arg_agent_ids ]; then _api_arg_agent_ids="all"; fi; # Default set to all, that is no specific agent
if [ -z $_api_arg_skill_ids ]; then _api_arg_skill_ids="all"; fi; # Default set to all, that is no specific skill
if [ $_debug_level -eq 2 ]; then
  echo ".. API arguments set to default (if not given in the config file)...";
fi;

if [ $_debug_level -ge 1 ]; then
  echo "... configuration file check done!";
fi;



###
### GET THE LIST OF AGENT IDS AND EMPLOYEE IDS
### Not advised to run due to the large number of requests it sends to LP API
###
#python2.7 get_employee_id.py \
#        -a $_lp_api_account \
#        -k $_lp_api_key \
#        -s $_lp_api_secret \
#        -t $_lp_api_token \
#        -T $_lp_api_token_secret \
#        -m $_lp_api_signature_method \
#        -l $_output_path_log



###
### GENERATE REPORT FOR THE CURRENT PERIOD
###

# Credentials to access the LivePerson are retrieved from config.navomi inside the Python script
# Arguments specification is given in the beginning of 15_min_reports.py script
# source proxies.txt
python2.7 15_min_reports.py \
        -a $_lp_api_account \
        -k $_lp_api_key \
        -s $_lp_api_secret \
        -t $_lp_api_token \
        -T $_lp_api_token_secret \
        -m $_lp_api_signature_method \
        -o $_output_path_report \
        -l $_output_path_log \
        -f $_api_arg_timeframe \
        -i $_api_arg_interval \
        -v $_api_arg_version \
        -A $_api_arg_agent_ids \
        -S $_api_arg_skill_ids \
        -e $_input_agent_employee \
        -d $_start_bucket_ts \
        -F $_end_bucket_ts \
        -V $_debug_level \
        -n $_name \
        -j $_output_path_json \
        -es $_email_server \
        -ep $_email_port \
        -ew $_email_pw \
        -ef $_email_from_addr \
        -et $_email_to_addr \
        -ej $_email_subject




###
### PREPARE REPORT FILES FOR FTP PULL OR SEND IN CASE OF PUSH
###

# Recover report file name
let _rep_ts=${_start_bucket_ts}-300;
if [ $_os == "MacOS" ]; then
  _rep_date=`date -r $_rep_ts +%m%d%y`;
  _rep_time=`date -r $_rep_ts +%H%M`;
elif [ $_os == "Linux" ]; then
  _rep_date=`date -d @$_rep_ts +%m%d%y`;
  _rep_time=`date -d @$_rep_ts +%H%M`;
fi;

if [ $_ftp_option == "PULL" ]; then

  # Check if AS report is in $_output_path_report, if so copy it to _ftp_local_dir
  if [ ! -f ${_output_path_report}AS_${_rep_date}.${_rep_time}.* ]; then
    echo "*** ERROR: report AS was not found in ${_output_path_report}.";
    echo "    This report won't be available for FTP PULL.";
  else
    cp ${_output_path_report}AS_${_rep_date}.${_rep_time}.* ${_ftp_local_dir};
  fi;

  # Check if AQ report is in $_output_path_report, if so copy it to _ftp_local_dir
  if [ ! -f ${_output_path_report}AQ_${_rep_date}.${_rep_time}.* ]; then
    echo "*** ERROR: report AQ was not found in ${_output_path_report}.";
    echo "    This report won't be available for FTP PULL.";
  else
    cp ${_output_path_report}AQ_${_rep_date}.${_rep_time}.* ${_ftp_local_dir};
  fi;

  # Check if Q report is in $_output_path_report, if so copy it to _ftp_local_dir
  if [ ! -f ${_output_path_report}Q_${_rep_date}.${_rep_time}.* ]; then
    echo "*** ERROR: report Q was not found in ${_output_path_report}.";
    echo "    This report won't be available for FTP PULL.";
  else
    cp ${_output_path_report}Q_${_rep_date}.${_rep_time}.* ${_ftp_local_dir};
  fi;

elif [ $_ftp_option == "PUSH" ]; then

  ### CHECK FOLDER STRUCTURE ON FTP SERVER
  ### NOTE - This can be scheduled to run only once at the very first time of the day to save time
  if [ $_ftp_option == "PUSH" ]; then
    python2.7 ftp_create_folder.py \
            -u $_ftp_user \
            -s $_ftp_pw \
            -h $_ftp_host \
            -p $_ftp_port
  fi;

  ### SEND REPORT VIA S/FTP
  # Call Python script that will
  #   1. connect to the FTP server
  #   2. check/create folder structure (YYYY/mm/dd)
  #   3. upload the report file to the pertinent folder
  python2.7 ftp_send_report.py \
          -u $_ftp_user \
          -s $_ftp_pw \
          -h $_ftp_host \
          -p $_ftp_port \
          -o $_output_path_report \
          -l $_output_path_log

fi;


###
### ATTEMPT TO FIX PREVIOUSLY LOGGED ERRORS
###


# TODO - error log and resolution















