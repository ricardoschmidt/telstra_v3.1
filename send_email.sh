#!/bin/bash
#
# Author:
#   Ricardo de O. Schmidt
#     NAVOMI.com Cloud Solutions
#
# Date:
#   December 6, 2017
#
# Contact:
#   ricardo.schmidt@navomi.com
#
# Description:
#   This is a Bash script that is called from a Cron job once per day.
#   This script should be called around 30 minutes past midnight.
#   It will compress (zip) all the reports generated for the previous
#   day and send the zip file to the email address specified in the
#   configuration file.
#
# Running:
#   The following arguments have to be given in the exact order:
#     1. Path to and name of the configuration file
#     2. Operational System (1:MacOS, 2:Linux)
#     3. Type of execution (1:Normal, 2:Recovery)
#     4. Debug level (0:None, 1:Basic, 2:Verbose)
#

##
## MACROS
__LOG_FILE="email_log";
__ERROR_FILE="email_error";
__TEMP_ERROR_FILE="email_error.temp";

##
## ARGUMENTS PARSING
if [ $# -ne 4 ]; then
  echo "*** ERROR: problem on the number of arguments.";
  echo "";
  echo "    Usage:";
  echo "    ./send_email.sh <CONF> <OS> <RUN TYPE> <DEBUG>";
  echo "      <CONF>      path/to/config.file";
  echo "      <OS>        OS type (1:Mac OS, 2:Linux)";
  echo "      <RUN TYPE>  Type of execution (1:Normal, 2:Recovery)";
  echo "      <DEBUG>     Debug level (0:None, 1:Basic, 2:Verbose)";
  echo "";
  echo "    Arguments have to follow this specific order.";
  echo "    Check README file for more information.";
  echo "    Exiting...";
  exit;
fi;

# Check configuration file
if [ ! -f $1 ]; then
  echo "*** ERROR: cannot open configuration file.";
  echo "    $1 not found.";
  echo "    Exiting...";
  exit;
else
  # Loads configuration
  source $1;
fi;

# Check OS type
if [ $2 -eq 1 ]; then # 1 for Mac OS
  _os="MacOS";
elif [ $2 -eq 2 ]; then # 2 for Linux
  _os="Linux";
else
  echo "*** ERROR: invalid OS option.";
  echo "    Use 1:Mac OS or 2:Linux";
  echo "    Exiting...";
  exit;
fi;

# Check type of execution
if [ $3 -eq 1 ]; then # 1 for Normal
  _run_type="Normal";
elif [ $3 -eq 2 ]; then # 2 for Recovery
  _run_type="Recovery";
else
  echo "*** ERROR: invalid run type option.";
  echo "    Use 1:Normal or 2:Recovery";
  echo "    Exiting...";
  exit;
fi;

# Check Debug level option
if [ $4 -eq 0 ]; then # 0 for none
  _debug_level=0
elif [ $4 -eq 1 ]; then # 1 for basic
  _debug_level=1
elif [ $4 -eq 2 ]; then # 2 for verbose
  _debug_level=2
else
  echo "*** ERROR: invalid debug option.";
  echo "    Use 0:None, 1:Basic or 2:Verbose";
  echo "    Exiting...";
  exit;
fi;


if [ $_debug_level -eq 1 ]; then
  echo "[ Debug level set to BASIC ]";
elif [ $_debug_level -eq 2 ]; then
  echo "[ Debug level set to VERBOSE ]";
fi;


##
## CHECK RUN TYPE
if [ "$_run_type" == "Normal" ]; then

	echo "< Run type: Normal >"

	if [ "$_os" == "MacOS" ]; then

		# Get current time
		_call_time_ts=`date +%s`
		_call_time_hr=`date -r $_call_time_ts "+%Y-%m-%d %H:%M:%S"`

		# Reduce the current date by one day
		_rep_date_ts=`date -j -f '%s' -v-1d $_call_time_ts +%s` # Epoch format
		_rep_date_hr=`date -j -f '%s' -v-1d $_call_time_ts +%Y-%m-%d` # Human-readable format
		_rep_date_fn=`date -j -f '%s' -v-1d $_call_time_ts +%m%d%y` # XML report file name format

	elif [ "$_os" == "Linux" ]; then

		# Get current time
		_call_time_ts=`date +%s`
		_call_time_hr=`date -d @$_call_time_ts "+%Y-%m-%d %H:%M:%S"`
		_call_time_str=`date -d @$_call_time_ts` # Needed for date calculations

		# Reduce the current date by one day
		_rep_date_ts=`date -d "$_call_time_str-1 day" +%s` # Epoch format
		_rep_date_hr=`date -d "$_call_time_str-1 day" +%Y-%m-%d` # Human-readable format
		_rep_date_fn=`date -d "$_call_time_str-1 day" +%m%d%y` # XML report file name format

	fi

	# Print debug message
	echo "=============================="
	echo "Call time: $_call_time_hr"
	echo "Report date: $_rep_date_hr"
	echo "=============================="

	# First check whether there are files to be sent for the given date
	# NOTE: It is not considered an error if no files are found
	#		The script will simply not send any report
	if [[ ! $(ls -A ${_output_path_report}*_${_rep_date_fn}.*.xml) ]]; then
		echo "*** NOTE: No files found for $_rep_date_hr"
	    echo "    Exiting..."
		exit
	fi

	# Compress all the report files for the given day (zip all three report types together)
	echo "- Compressing report files from $_rep_date_hr"
	_file_to_email="reports_${_rep_date_fn}.zip"
	zip -q -j -r $_file_to_email ${_output_path_report}*_${_rep_date_fn}.*.xml

	# Count zipped files
	_tot_files=`ls -l $_output_path_report*_${_rep_date_fn}.*.xml | wc -l | awk '{print $1;}'`
	echo "- $_tot_files zipped files and ready to be sent"

	# Send the zip file by email to the informed email in the configuration file
	echo "- Sending zip file $_file_to_email"

	# Call Python script to send the email
	python2.7 email_reports.py \
			-src $_rep_email_from_addr \
			-dst $_rep_email_to_addr \
	        -server $_rep_email_server \
		    -port $_rep_email_port \
			-passwd $_rep_email_pw \
			-filename $_file_to_email \
			-log ${_output_path_email_log} \
			-error ${_output_path_email_log} \
			-retry $_rep_email_retry \
			-date $_rep_date_fn \
			-subject False \
			-message False

	# Remove the zip file
	rm -f $_file_to_email
	echo "- Removing zipped file $_file_to_email"

	## WRITE LOG
	if [ $_debug_level -eq 2 ]; then
		echo "< Writing log >"
	fi

	# Check if success file exists
	if [ -e ${_output_path_email_log}.email_success ]; then

		if [ $_debug_level -eq 2 ]; then
			echo "- Sending of reports from $_rep_date_hr SUCCEEDED"
			echo "- Writing into email log $_output_path_email_log$__LOG_FILE"
		fi

		echo $_rep_date_fn >> ${_output_path_email_log}${__LOG_FILE}
		rm -f ${_output_path_email_log}.email_success

	# Else, check if failure file exists
	elif [ -e ${_output_path_email_log}.email_fail ]; then
		if [ $_debug_level -eq 2 ]; then
			echo "- Sending of reports from $_rep_date_hr FAILED"
			echo "- Writing into email error log $_output_path_email_log$__ERROR_FILE"
		fi

		echo $_rep_date_fn >> ${_output_path_email_log}${__ERROR_FILE}
		rm -f ${_output_path_email_log}.email_fail
	fi


# ENTERS RECOVERY MODE
else

  # RECOVERY mode, which is run to resend reports from any previous failed attempt.
  # The failed attempts will be registered in email_error log file, its location is given in the configutation file.

  if [ $_debug_level -eq 2 ]; then
    echo "< Run type: recovery >";
    echo "  - Check error log file for reports to send";
  fi;

  if [ -e ${_output_path_email_log}${__ERROR_FILE} ]; then

    # Process line by line of the file
    while read -r line; do

      _report_name=$line

      if [ $_debug_level -eq 2 ]; then
        echo "  - Resending reports from $_report_name";
      fi;

      # Create zip file
      if [ $_debug_level -eq 2 ]; then
        echo "    - Creating zip file";
      fi;
      _file_to_email="reports_${_report_name}.zip";
      zip -q -j -r $_file_to_email ${_output_path_report}*_${_report_name}.*.xml;
      # Note the line above use the standard output folder for reports, as specified in the configuration file

      # Send email
      if [ $_debug_level -eq 2 ]; then
        echo "    - Sending email with file $_file_to_email";
      fi;
      python2.7 email_reports.py \
              -src $_rep_email_from_addr \
              -dst $_rep_email_to_addr \
              -server $_rep_email_server \
              -port $_rep_email_port \
              -passwd $_rep_email_pw \
              -filename $_file_to_email \
              -log ${_output_path_email_log} \
              -error ${_output_path_email_log} \
              -retry $_rep_email_retry \
              -date $_report_name \
              -subject False \
			  -message False

      # Remove zip file
      rm -f $_file_to_email;

      # Save temp log
      if [ $_debug_level -eq 2 ]; then
        echo "    - Writing log";
      fi;

      # Check if success file exists
      if [ -e ${_output_path_email_log}.email_success ]; then
        if [ $_debug_level -eq 2 ]; then
          echo "    - Sending of reports from $_report_name was a SUCCESS";
          echo "    - Writing into email log";
        fi;
        echo $_report_fdate >> ${_output_path_email_log}${__LOG_FILE};
        rm -f ${_output_path_email_log}.email_success;
      # Else, check if failure file exists
      elif [ -e ${_output_path_email_log}.email_fail ]; then
        if [ $_debug_level -eq 2 ]; then
          echo "    - Sending of reports from $_report_name FAILED";
          echo "    - Writing into email error log";
        fi;
        echo $_report_fdate >> ${_output_path_email_log}${__TEMP_ERROR_FILE};
        rm -f ${_output_path_email_log}.email_fail;
      fi;

    done < ${_output_path_email_log}${__ERROR_FILE};

    # Move the temp error file content to the error file
    if [ $_debug_level -eq 2 ]; then
      echo "    - Updating error log file";
    fi;
    if [ -e ${_output_path_email_log}${__TEMP_ERROR_FILE} ]; then
      mv ${_output_path_email_log}${__TEMP_ERROR_FILE} ${_output_path_email_log}${__ERROR_FILE};
    else
      rm -f ${_output_path_email_log}${__ERROR_FILE};
    fi;

  else
    if [ $_debug_level -eq 2 ]; then
      echo "  - Nothing to recover";
    fi;
  fi;


fi;

