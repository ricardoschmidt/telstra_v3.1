#!/bin/bash
#
# Author:
#   Ricardo de O. Schmidt
#     NAVOMI.com Cloud Solutions
#
# Date:
#   October 4, 2017
#
# Contact:
#   ricardo.schmidt@navomi.com
#
# Description:
#   This is a Bash script that, based on information in a configuration file and
#   input via command line, calls the Python script 15_min_reports.py to
#   generate 15-min report files for a given period, starting no longer than 24
#   hours in the past. The 24-hour limit is due to data availability of
#   LivePerson Operational Realtime API.
#
#   This script executes a series of steps:
#     1.  Makes sure all arguments and pertinent information is found in the
#         configuration file.
#     2.  Calls the 15_min_reports.py Python script as many times as needed to
#         generate the report files for the given date/time interval.
#     3.  Stores the report files within the folder given in the command line.
#
# Input arguments (all mandatory and in order):
#   1.  Path/to/CONFIG_FILE containing configuration information.
#       Default file is config.navomi
#   2.  S/FTP option for getting the reports to the customer; options are:
#         1: PUSH
#         2: PULL
#   3.  Operational System in which this script will run; options are:
#         1: Mac OS
#         2: Linux
#   4.  Debug option; options are:
#         0: None
#         1: Basic
#         2: Verbose
#   5.  Path/to/OUTPUT_REPORT_FILE/, where reports will be stored.
#       Default is ./rerun_reports/
#   6.  Start time of the interval for report generation; that is, time of the
#       first report to be generated.
#       Format for start time is MMDDYY.hhmm (UTC time)
#   7.  End time of the interval for report generation; that is, time of the
#       last report to be generated.
#       NOTE: for a single report, start time equals end time.
#       Format for end time is MMDDYY.hhmm (UTC time)
#
# Running:
#   For running instructions please refer to the README file.
#

###
### ARGUMENTS PARSING
###

# First check if they are ther7 if [ $# -ne 5 ]; then
if [ $# -ne 7 ]; then
  echo "";
  echo "    Usage:";
  echo "    ./postday.sh <CONF> <FTP> <OS> <DEBUG> <OUTPUT PATH> <INTRADAY XML PATH> <EMAIL>";
  echo "      <CONF>              Path/to/config.file";
  echo "      <FTP>               FTP option (1:Push, 2:Pull)";
  echo "      <OS>                OS type (1:Mac OS, 2:Linux)";
  echo "      <DEBUG>             Debug level (0:None, 1:Basic, 2:Verbose)";
  echo "      <OUTPUT PATH>       Output directory to store reports (has to be different than the one in config.file";
  echo "      <INTRADAY XML PATH> Directory from where to retrieve the intraday XML report files.";
  echo "      <EMAIL>             Reports have to be sent via email or not. (True:To send, False:To not send.";
  echo "";
  echo "    Arguments have to follow this specific order.";
  echo "    Check README file for more information.";
  echo "    Exiting...";
  exit;
fi;

# Check configuration file
if [ ! -f $1 ]; then
  echo "*** ERROR: cannot open configuration file.";
  echo "    $1 not found.";
  echo "    Exiting...";
  exit;
else
  # Loads configuration
  source $1;
fi;

# Check FTP option
if [ $2 -eq 1 ]; then # 1 for PUSH
  _ftp_option="PUSH";
elif [ $2 -eq 2 ]; then # 2 for PULL
  _ftp_option="PULL";
else
  echo "*** ERROR: invalid FTP option.";
  echo "    Use 1:Push or 2:Pull";
  echo "    Exiting...";
  exit;
fi;

# Check OS type
if [ $3 -eq 1 ]; then # 1 for Mac OS
  _os="MacOS";
elif [ $3 -eq 2 ]; then # 2 for Linux
  _os="Linux";
else
  echo "*** ERROR: invalid OS option.";
  echo "    Use 1:Mac OS or 2:Linux";
  echo "    Exiting...";
  exit;
fi;

# Check Debug level option
if [ $4 -eq 0 ]; then # 0 for none
  _debug_level=0
elif [ $4 -eq 1 ]; then # 1 for basic
  _debug_level=1
elif [ $4 -eq 2 ]; then # 2 for verbose
  _debug_level=2
else
  echo "*** ERROR: invalid debug option.";
  echo "    Use 0:None, 1:Basic or 2:Verbose";
  echo "    Exiting...";
  exit;
fi;

# Check if output directory exists
if [ ! -d $5 ]; then
  echo "** Warning: outputh directory $5 not found.";
  echo "   Creating default ./postday_reports";
  mkdir postday_reports;
  _output_path_report="./postday_reports/";
else
  _output_path_report=$5;
fi;

# Check if output directory exists
if [ ! -d $6 ]; then
	echo "** Warning: input directory $6 for fetching the intraday XML report file not found.";
	echo "   Exiting...";
	exit;
else
	_input_xml_path=$6;
fi;

# Check email sender option
if [ $7 == "True" ]; then # True: reports will be sent via email
  _reports_by_email="True";
elif [ $7 == "False" ]; then # False: reports won't be sent via email
  _reports_by_email="False";
else
  echo "*** ERROR: invalid value to EMAIL send option.";
  echo "    Use True:To send or False:To not send";
  echo "    Exiting...";
  exit;
fi;

## Check informed time, different command date sintax depending on OS
#if [ $_os == "MacOS" ]; then  # Mac OS
#  # Convert start and end times to epoch
#  _start_ts=`date -j -f "%m%d%y.%H%M%S" ${6}00 +%s`;
#  _end_ts=`date -j -f "%m%d%y.%H%M%S" ${7}00 +%s`;
#elif [ $_os == "Linux" ]; then # Linux
#  # Convert start and end times to epoch
#  _start_nice=`echo $6 | awk '{print substr($1,1,2)"/"substr($1,3,2)"/"substr($1,5,2)" "substr($1,8,2)":"substr($1,10,2)":00";}'`;
#  _start_ts=`date -d "$_start_nice" +%s`;
#  _end_nice=`echo $7 | awk '{print substr($1,1,2)"/"substr($1,3,2)"/"substr($1,5,2)" "substr($1,8,2)":"substr($1,10,2)":00";}'`;
#  _end_ts=`date -d "$_end_nice" +%s`;
#fi;

if [ $_os == "MacOS" ]; then
	_now_ts=`date +%s`
	_date_to_report_ts=`date -j -f '%s' -v -2d $_now_ts +%s`
	_day_to_report=`date -j -f '%s' -v -2d $_now_ts +%m%d%y`
	_start_time_to_report_dt=`echo "$_day_to_report.000000"`
	_end_time_to_report_dt=`echo "$_day_to_report.234500"`
	_start_ts=`date -j -f "%m%d%y.%H%M%S" $_start_time_to_report_dt +%s`
	_end_ts=`date -j -f "%m%d%y.%H%M%S" $_end_time_to_report_dt +%s`
elif [ $_os == "Linux" ]; then
	_now_str=`date`
	_start_hr=`date -d "$_now_str-2 days" "+%m/%d/%y 00:00:00"`
	_end_hr=`date -d "$_now_str-2 days" "+%m/%d/%y 23:45:00"`
	_start_ts=`date -d "$_start_hr" +%s`
	_end_ts=`date -d "$_end_hr" +%s`
fi


# Check if end time is bigger than start time
if [ $_start_ts -gt $_end_ts ]; then
  echo "*** ERROR: start time has to be smaller than end time.";
  echo "    Exiting...";
  exit;
fi;

# Check if informed start or end time are in the future
if [ $_start_ts -gt $_current_ts ] || [ $_end_ts -gt $_current_ts ]; then
  echo "*** ERROR: start and end time CANNOT be in the future.";
  echo "    Exiting...";
  exit;
fi;


# Check the applog output path
if [ -z $_output_path_applog_postday ]; then
    _output_path_applog_postday="./app_logs_postday/";
    if [ $_debug_level -eq 2 ]; then
        echo "    .. Output path for app log files not informed, set to default...";
    fi;
fi;
if [ ! -d $_output_path_applog_postday ]; then
    mkdir $_output_path_applog_postday;
    if [ $_debug_level -eq 2 ]; then
        echo "    .. Output folder for app log files created...";
    fi;
fi;


###
### Find out how many reports it has to generate
###

_diff_start_end=$(expr $_end_ts - $_start_ts);
_tot_rep=$(expr $_diff_start_end / 60 / 15 + 1);
if [ $_debug_level -eq 2 ]; then
	echo "==========================================================================="
	echo "Staring the generation of $_tot_rep postday (refreshed) reports"
fi;


# Start counter
echo "==========================================================================="
_rep_generated=0;
_report_generated_count=0
_report_fail_count=0
_report_fail_list=""

while [ $_report_generated_count -ne $_tot_rep ]; do


	if [ $_os == "MacOS" ]; then

		# Generation period
		_start_dt=`date -r $_start_ts +%m%d%y.%H%M`
		_start_hr=`date -r $_start_ts "+%Y-%m-%d.%H:%M:%S"`
		_end_dt=`date -r $_end_ts +%m%d%y.%H%M`
		_end_hr=`date -r $_end_ts "+%Y-%m-%d.%H:%M:%S"`


		# Calculate how many seconds we are ahead of call time
		_seconds_to_sum=`echo $_report_generated_count | awk '{print $1*900;}'`

		# Calculate report time
		_report_time_ts=`date -j -f '%s' -v +${_seconds_to_sum}S $_start_ts +%s`
		_report_time_dt=`date -j -f '%s' -v +${_seconds_to_sum}S $_start_ts +%m%d%y.%H%M`
		_report_time_hr=`date -j -f '%s' -v +${_seconds_to_sum}S $_start_ts "+%Y-%m-%d.%H:%M:%S"`

		# Compute report name
		_report_name=`date -r $_report_time_ts "+%m%d%y.%H%M"`

		# Compute generation day
		_report_day=`date -r $_report_time_ts "+%m%d%y"`

		# Compute OR API buckets
		_or_api_bucket_1_ts=`date -j -f '%s' -v +5M $_report_time_ts +%s`
		_or_api_bucket_1_dt=`date -r $_or_api_bucket_1_ts +%m%d%y.%H%M`
		_or_api_bucket_1_hr=`date -r $_or_api_bucket_1_ts "+%Y-%m-%d.%H:%M:%S"`
		_or_api_bucket_2_ts=`date -j -f '%s' -v +10M $_report_time_ts +%s`
		_or_api_bucket_2_dt=`date -r $_or_api_bucket_2_ts +%m%d%y.%H%M`
		_or_api_bucket_2_hr=`date -r $_or_api_bucket_2_ts "+%Y-%m-%d.%H:%M:%S"`
		_or_api_bucket_3_ts=`date -j -f '%s' -v +15M $_report_time_ts +%s`
		_or_api_bucket_3_dt=`date -r $_or_api_bucket_3_ts +%m%d%y.%H%M`
		_or_api_bucket_3_hr=`date -r $_or_api_bucket_3_ts "+%Y-%m-%d.%H:%M:%S"`

		# Compute the OR API timeframe
		_now_ts=`date +%s`
		_or_api_timeframe=`echo $_now_ts $_report_time_ts | awk '{_t=int(($1-$2)/60); _tr=int(_t/5)*5; print _tr;}'`

		# Compute EH API interval
		_eh_api_from_ts=$_report_time_ts
		_eh_api_from_dt=$_report_time_dt
		_eh_api_from_hr=$_report_time_hr
		_eh_api_to_ts=`date -j -f '%s' -v +899S $_eh_api_from_ts +%s`
		_eh_api_to_dt=`date -r $_eh_api_to_ts +%m%d%y.%H%M`
		_eh_api_to_hr=`date -r $_eh_api_to_ts "+%Y-%m-%d.%H:%M:%S"`

	elif [ $_os == "Linux" ]; then

		# Generation period
		_start_dt=`date -d @$_start_ts +%m%d%y.%H%M`
		_start_hr=`date -d @$_start_ts "+%Y-%m-%d.%H:%M:%S"`
		_start_str=`date -d @$_start_ts`
		_end_dt=`date -d @$_end_ts +%m%d%y.%H%M`
		_end_hr=`date -d @$_end_ts "+%Y-%m-%d.%H:%M:%S"`

		# Calculate how many seconds we are ahead of call time
		_seconds_to_sum=`echo $_report_generated_count | awk '{print $1*900;}'`

		# Calculate report time
		_report_time_ts=`date -d "$_start_str+${_seconds_to_sum} seconds" +%s`
		_report_time_dt=`date -d "$_start_str+${_seconds_to_sum} seconds" +%m%d%y.%H%M`
		_report_time_hr=`date -d "$_start_str+${_seconds_to_sum} seconds" "+%Y-%m-%d.%H:%M:%S"`
		_report_time_str=`date -d @$_report_time_ts`

		# Compute report name
		_report_name=`date -d @$_report_time_ts "+%m%d%y.%H%M"`

		# Compute generation day
		_report_day=`date -d @$_report_time_ts "+%m%d%y"`

		# Compute OR API buckets
		_or_api_bucket_1_ts=`date -d "$_report_time_str+5 minutes" +%s`
		_or_api_bucket_1_dt=`date -d @$_or_api_bucket_1_ts +%m%d%y.%H%M`
		_or_api_bucket_1_hr=`date -d @$_or_api_bucket_1_ts "+%Y-%m-%d.%H:%M:%S"`
		_or_api_bucket_2_ts=`date -d "$_report_time_str+10 minutes" +%s`
		_or_api_bucket_2_dt=`date -d @$_or_api_bucket_2_ts +%m%d%y.%H%M`
		_or_api_bucket_2_hr=`date -d @$_or_api_bucket_2_ts "+%Y-%m-%d.%H:%M:%S"`
		_or_api_bucket_3_ts=`date -d "$_report_time_str+15 minutes" +%s`
		_or_api_bucket_3_dt=`date -d @$_or_api_bucket_3_ts +%m%d%y.%H%M`
		_or_api_bucket_3_hr=`date -d @$_or_api_bucket_3_ts "+%Y-%m-%d.%H:%M:%S"`

		# Compute the OR API timeframe (rounded to 5)
		_now_ts=`date +%s`
		_or_api_timeframe=`echo $_now_ts $_report_time_ts | awk '{_t=int(($1-$2)/60); _tr=int(_t/5)*5; print _tr;}'`

		# Compute EH API interval
		_eh_api_from_ts=$_report_time_ts
		_eh_api_from_dt=$_report_time_dt
		_eh_api_from_hr=$_report_time_hr
		_eh_api_to_ts=`date -d "$_report_time_str+899 seconds" +%s`
		_eh_api_to_dt=`date -d @$_eh_api_to_ts +%m%d%y.%H%M`
		_eh_api_to_hr=`date -d @$_eh_api_to_ts "+%Y-%m-%d.%H:%M:%S"`

	fi;

	# Print debug message informing times to be used in the generation of reports
	if [ $_debug_level -eq 2 ]; then
		#echo "==========================================================================="
		echo "Generation period: $_start_hr [$_start_dt] ($_start_ts)"
		echo "                 : $_end_hr [$_end_dt] ($_end_ts)"
		echo "Report time      : $_report_time_hr [$_report_time_dt] ($_report_time_ts)"
		echo "Report name      : *_$_report_name.xml"
		#echo "OR API buckets   : $_or_api_bucket_1_hr [$_or_api_bucket_1_dt] ($_or_api_bucket_1_ts)"
		#echo "                   $_or_api_bucket_2_hr [$_or_api_bucket_2_dt] ($_or_api_bucket_2_ts)"
		#echo "                   $_or_api_bucket_3_hr [$_or_api_bucket_3_dt] ($_or_api_bucket_3_ts)"
		#echo "OR API timeframe : $_or_api_timeframe"
		echo "EH API interval  : $_eh_api_from_hr [$_eh_api_from_dt] ($_eh_api_from_ts)"
		echo "                   $_eh_api_to_hr [$_eh_api_to_dt] ($_eh_api_to_ts)"
		echo "==========================================================================="
	fi;

	# Check if the intraday XML report file can be found
	_xml_as_report_name=`echo "${_input_xml_path}AS_${_report_name}.xml"`
	_xml_aq_report_name=`echo "${_input_xml_path}AQ_${_report_name}.xml"`
	_xml_q_report_name=`echo "${_input_xml_path}Q_${_report_name}.xml"`
	if [ ! -f ${_xml_as_report_name} ] || [ ! -f $_xml_aq_report_name ] || [ ! -f $_xml_q_report_name ]; then
		echo "*** ERROR: cannot find the intraday XML report file"
		echo "    One or multiple XML reports for $_report_name not found"
		echo "    Exiting the entire postday generation..."

		# Append missing file name to the array
        ((_report_fail_count++))
        _report_fail_list="$_report_fail_list,$_report_name"


	else
		echo "< All XML reports for $_report_name have been found! >"

		# Call the report generation for the given report
        # source proxies.txt
        python2.7 generate_reports.py \
            -a $_lp_api_account \
            -k $_lp_api_key \
            -s $_lp_api_secret \
            -t $_lp_api_token \
            -T $_lp_api_token_secret \
            -m $_lp_api_signature_method \
            -o $_output_path_report \
            -l $_output_path_log \
            -al $_output_path_applog_postday \
            -f $_or_api_timeframe \
            -b1ts $_or_api_bucket_1_ts \
            -b2ts $_or_api_bucket_2_ts \
            -b3ts $_or_api_bucket_3_ts \
            -b1hr $_or_api_bucket_1_hr \
            -b2hr $_or_api_bucket_2_hr \
            -b3hr $_or_api_bucket_3_hr \
            -i $_api_arg_interval \
            -v $_api_arg_version \
            -A $_api_arg_agent_ids \
            -S $_api_arg_skill_ids \
            -e $_input_agent_employee \
            -d $_or_api_bucket_1_ts \
            -F $_or_api_bucket_3_ts \
            -V $_debug_level \
            -n $_report_name \
            -j $_output_path_json \
            -es $_email_server \
            -ep $_email_port \
            -ew $_email_pw \
            -ef $_email_from_addr \
            -et $_email_to_addr \
            -ej $_email_subject \
            -ehFrom $_eh_api_from_ts \
            -ehTo $_eh_api_to_ts \
            -cf $_input_concurrency_factor \
            -dcf $_default_concurrency_factor \
            -cts $_report_time_ts \
            -postday True \
            -inxml $_input_xml_path

        echo "=== Copying AS intraday report to the postday folder ==="
        cp ${_xml_as_report_name} ${_output_path_report}

        echo "==========================================================================="
	fi;

	# Increment report counter to be used for generation and number of seconds control
    let _report_generated_count+=1

done;


# Check if all postday report files have been correctly generated
_count_as_reports=`ls ${_output_path_report}AS_${_report_day}.*.xml | wc -l | awk '{print $1;}'`
_count_aq_reports=`ls ${_output_path_report}AQ_${_report_day}.*.xml | wc -l | awk '{print $1;}'`
_count_q_reports=`ls ${_output_path_report}Q_${_report_day}.*.xml | wc -l | awk '{print $1;}'`
_count_all_reports=`ls ${_output_path_report}*_${_report_day}.*.xml | wc -l | awk '{print $1;}'`

#if [ $_count_all_reports -eq 288 ]; then

echo "< All ${_count_all_reports} postday reports have been successfully generated >"
echo "=== Zipping all ${_count_all_reports} postday report files ==="

# Create zip file name
_file_to_email=`echo "reports_${_report_day}_refreshed.zip"`

# Zip all 288 files
zip -q -j -r $_file_to_email ${_output_path_report}*_${_report_day}.*.xml

#  Sending out the email with the postday reports only be called in case <EMAIL> equals true.
if [ $_reports_by_email == "True" ]; then
    echo "=== Send the postday report zip file to email ==="

    python2.7 email_reports.py \
            -src $_rep_email_from_addr \
            -dst $_rep_email_to_addr \
            -server $_rep_email_server \
            -port $_rep_email_port \
            -passwd $_rep_email_pw \
            -filename $_file_to_email \
            -log ${_output_path_email_log} \
            -error ${_output_path_email_log} \
            -retry $_rep_email_retry \
            -date $_report_day \
            -subject False \
            -message False

    rm -f $file_to_email
fi

# send e-mail with list of missing reports
if [ $_report_fail_count -gt 0 ];
then
    echo "=== Send the list of missing intraday reports - postday reports NOT generated to email ==="

    python2.7 email_reports.py \
            -src $_rep_email_from_addr \
            -dst $_rep_email_to_addr \
            -server $_rep_email_server \
            -port $_rep_email_port \
            -passwd $_rep_email_pw \
            -filename $_file_to_email \
            -log ${_output_path_email_log} \
            -error ${_output_path_email_log} \
            -retry $_rep_email_retry \
            -date $_report_day \
            -subject "True" \
            -message $_report_fail_list

    # Print debug message informing files counters and list don't found files
    echo ""
    echo "==========================================================================="
    echo " Missing files  : $_report_fail_list"
    echo "==========================================================================="

fi

echo "=== Generation completed. Nothing else to do! ==="
echo



