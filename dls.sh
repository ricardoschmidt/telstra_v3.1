#!/usr/bin/env bash
#
# Author:
# 	Luciano Buhler
#     NAVOMI.com Cloud Solutions
#
#
# Date:
# 	April 23, 2018
#
# Contact:
#   luciano.buhler@navomi.com
#
# Description:
#   This is a Bash script to Daylight Saving Time (DLS) test. Case necessary, will be regerated missing reports
#   DLS Starts have 23 hours and must be have 276 reports
#   DLS Ends have 25 hours and must be have 288 reports (12 will be replaced)

#------------- / SECTION 1 - Variables / -------------
# arrays of missing files
declare -a _arr_missing_files
declare -a _arr_DLS_reports=()
declare -a _arr_DLS_missing_reports=()
declare -a _arr_DLS_start_reports=()
declare -a _arr_DLS_start_missing_reports=()
declare -a _arr_DLS_end_reports=()
declare -a _arr_DLS_end_missing_reports=()
declare -a _arr_regerated_files=()
declare -a _arr_found_reports=()
declare -a _arr_removed_files=()

# files counters
_count_DLS_reports=0
_count_DLS_end_reports=0
_count_DLS_start_reports=0
_count_removed_reports=0
_report_generated_count=0
_count_regerated_reports=0
_count_found_reports=0

# files missing counters
_count_DLS_missing_reports=0
_count_DLS_end_missing_reports=0
_count_DLS_start_missing_reports=0
_count_missing_reports=0

# general variables
_missed_hour=()
_expected_reports=288

# boolean vars to tests if is DLS
_DLS_start=0
_DLS_end=0


#----------------- / END SECTION 1 / -----------------

#------------- / SECTION 2 - Functions / -------------

#----------------- / END SECTION 2 / -----------------


#------------- / SECTION 3 - Arguments Parsing / -------------

###
### ARGUMENTS PARSING
###

# First check if they are there
if [ $# -ne 4 ]; then
  echo "*** ERROR: arguments missing.";
  echo "";
  echo "    Usage:";
  echo "    ./dls.sh <CONF> <OS> <DEBUG> <INTRADAY XML PATH>";
  echo "      <CONF>  path/to/config.file";
  echo "      <OS>                OS type (1:Mac OS, 2:Linux)";
  echo "      <DEBUG> Debug level (0:None, 1:Basic, 2:Verbose)";
  echo "      <INTRADAY XML PATH> Directory from where to retrieve the intraday XML report files. Standard <reports/>";
  echo "";
  echo "    Arguments have to follow this specific order.";
  echo "    Check README file for more information.";
  echo "    Exiting...";
  exit;
fi;

# Check configuration file
if [ ! -f $1 ]; then
  echo "*** ERROR: cannot open configuration file.";
  echo "    $1 not found.";
  echo "    Exiting...";
  exit;
else
  # Loads configuration
  source $1;
fi;

# Check OS type
if [ $2 -eq 1 ]; then # 1 for Mac OS
  _os="MacOS";
elif [ $2 -eq 2 ]; then # 2 for Linux
  _os="Linux";
else
  echo "*** ERROR: invalid OS option.";
  echo "    Use 1:Mac OS or 2:Linux";
  echo "    Exiting...";
  exit;
fi;

# Check Debug level option
if [ $3 -eq 0 ]; then # 0 for none
  _debug_level=0
elif [ $3 -eq 1 ]; then # 1 for basic
  _debug_level=1
elif [ $3 -eq 2 ]; then # 2 for verbose
  _debug_level=2
else
  echo "*** ERROR: invalid debug option.";
  echo "    Use 0:None, 1:Basic or 2:Verbose";
  echo "    Exiting...";
  exit;
fi;

# Check if output directory exists
if [ ! -d $4 ]; then
	echo "** Warning: input directory $4 for fetching the intraday XML report file not found.";
	echo "   Exiting...";
	exit;
else
	_input_xml_path=$4;
fi;


#----------------- / END SECTION 3 / -----------------

#------------- / SECTION 4 - DLS Control / -------------
# timedatectl

# the command timedatectl is only able in centOS >= 7.0
#_timedatectl=`timedatectl`
#_timedatectl=$(timedatectl | sed -n 's/Time zone: \(.*\)/\1/p') # parsing and get "Time Zone" info
#echo $_timedatectl

# centOS < 7.0
# get Time Zone informations
# $ TZ='Australia/Melbourne' date

# B plan - bash commands and returns
#$ date -%Z #return AEST (Eastern Standard Time) or AEDT (Eastern Daylight Saving Time)
# END Daylight Saving
#$ date --date=2018/03/31 -%Z #returns AEDT
#$ date --date=2018/04/01 -%Z #returns AEDT
#$ date --date=2018/04/02 -%Z #returns AEST

# START Daylight Saving
#$ date --date=2018/10/06 -%Z #returns AEST
#$ date --date=2018/10/07 -%Z #returns AEST
#$ date --date=2018/10/08 -%Z #returns AEDT

# show the date on informed TIME ZONE
#$ TZ='Australia/Melbourne' date

_today_dls=`TZ='Australia/Melbourne' date +%Z`
_yesterday_dls=`TZ='Australia/Melbourne' date --date="1 day ago" +%Z`
_tomorrow_dls=`TZ='Australia/Melbourne' date --date="1 day" +%Z`
_today_report=`date +%m%d%y`

echo ""
echo "==========================================================================="
echo "==        Starting the Daylight Saving Time (DLS) Control Script         =="
echo "==========================================================================="

# TODO remove two next lines
#_today_dls="AEST"
#_tomorrow_dls="ADST"

# test if today is DLS ended (Australia is the first Sunday of April): 25 hours
if [ $_today_dls == "AEST" ] && [ $_yesterday_dls == "AEDT" ];
then
    echo " Warning: Today Daylight Saving Ended! Today have one hour the least."
    _DLS_start=0
    _DLS_end=1
    _expected_reports=288

# test if today is DLS starts (Australia is the first Sunday of October): 23 hours
elif [ $_today_dls == "AEDT" ] && [ $_yesterday_dls == "AEST" ];
then
    echo " Warning: Today Daylight Saving Started! Today have one hour longer."
    echo "==========================================================================="
    echo "==              Testing reports of the DLS Starts day                    =="
    echo "==========================================================================="
    _DLS_start=1
    _DLS_end=0
    _missed_hour=("$_today_report.0200" "$_today_report.0215" "$_today_report.0230" "$_today_report.0245")
    _expected_reports=276

else # normal days: 24 hours
    echo "  Today is not the DLS Starts nor the DLS Ends."
    echo "  Today is a normal day with 24 hours and this test is finished."
    echo "==========================================================================="
    echo ""
    _DLS_start=0
    _DLS_end=0
    exit
fi;

#----------------- / END SECTION 4 / -----------------

#------------- / SECTION 5 - Files Control / -------------

_now_str=`date`
_current_ts=`date +%s`
#_start_hr=`date -d "1 day ago" "+%m/%d/%y 00:00:00"`   # to run one day after
_start_hr=`date "+%m/%d/%y 00:00:00"`                   # to run intraday
#_end_hr=`date -d "1 day ago" "+%m/%d/%y 23:45:00"`     # to run one day after
_end_hr=`date "+%m/%d/%y 23:45:00"`                     # to run intraday
_start_ts=`date -d "$_start_hr" +%s`
_end_ts=`date -d "$_end_hr" +%s`

# Check if informed start or end time are in the future
if [ $_start_ts -gt $_current_ts ] || [ $_end_ts -gt $_current_ts ]; then
  echo "*** ERROR: start and end time CANNOT be in the future.";
  echo "    Exiting...";
  exit;
fi;

###
### Find out how many reports it has to generate
###

_diff_start_end=$(expr $_end_ts - $_start_ts);
_tot_rep=$(expr $_diff_start_end / 60 / 15 + 1);
#_tot_rep=96

if [ $_debug_level -eq 2 ]; then
	echo "==========================================================================="
	echo "Starting the confer of $_tot_rep reports"
fi;


# Start counter
echo "==========================================================================="

while [ $_report_generated_count -ne $_tot_rep ]; do

    # Generation period
    _start_dt=`date -d @$_start_ts +%m%d%y.%H%M`
    _start_hr=`date -d @$_start_ts "+%Y-%m-%d.%H:%M:%S"`
    _start_str=`date -d @$_start_ts`
    _end_dt=`date -d @$_end_ts +%m%d%y.%H%M`
    _end_hr=`date -d @$_end_ts "+%Y-%m-%d.%H:%M:%S"`
    _start_str_aux=`date -d @$_start_ts "+%Y-%m-%d %H:%M"`

    # Calculate how many seconds we are ahead of call time
    _seconds_to_sum=`echo $_report_generated_count | awk '{print $1*900;}'`

    _report_time_ts=`date -d "$_start_str_aux $_seconds_to_sum seconds" +%s`
    _report_time_dt=`date -d "$_start_str_aux $_seconds_to_sum seconds" +%m%d%y.%H%M`
    _report_time_hr=`date -d "$_start_str_aux $_seconds_to_sum seconds" "+%Y-%m-%d.%H:%M:%S"`

    # Compute report name
    _report_name=`date -d @$_report_time_ts "+%m%d%y.%H%M"`

  	# Print debug message informing times to be used in the generation of reports
	if [ $_debug_level -eq 2 ]; then
		#echo "==========================================================================="
		echo "Generation period: $_start_hr [$_start_dt] ($_start_ts)"
		echo "                 : $_end_hr [$_end_dt] ($_end_ts)"
		echo "Report time      : $_report_time_hr [$_report_time_dt] ($_report_time_ts)"
		echo "Report name      : *_$_report_name.xml"
		echo "==========================================================================="
		echo ""
	fi;

	# Check if the intraday XML report file can be found
	_xml_as_report_name=`echo "${_input_xml_path}AS_${_report_name}.xml"`
	_xml_aq_report_name=`echo "${_input_xml_path}AQ_${_report_name}.xml"`
	_xml_q_report_name=`echo "${_input_xml_path}Q_${_report_name}.xml"`

    # Today is DLS starts: 23 hours
    if [ $_DLS_start == 1 ] && [ $_DLS_end == 0 ];
    then
        if [ ! -f ${_xml_as_report_name} ] || [ ! -f $_xml_aq_report_name ] || [ ! -f $_xml_q_report_name ]; then
		    # test if the reports is one than missing hour, then won't be regerate
            if [[ " ${_missed_hour[@]} " =~ " ${_report_name} " ]]; then
                # whatever you want to do when arr contains value
                echo " Missing report $_report_name is one than lost DLS hour"

            else # when the missing report isn't in the missing hour, it will be regerate

                echo " Starting the generation of $_report_name (refreshed) reports"
                echo " Regerating report..."
                echo " "

                # regerate the missing report
                /bin/bash rerun.sh ./config.navomi 2 2 2 ./rerun_reports/ "$_report_name" "$_report_name"
                _arr_regerated_files[_count_regerated_reports]=$_report_name
                ((_count_regerated_reports++))
            fi

            # Append file name to the array
            _arr_missing_files[_count_missing_reports]=$_report_name
            ((_count_missing_reports++))

        else # if three reports to tested hour is found
            # append the _report_name to array of exists files
            #_arr_DLS_start_reports[_count_DLS_start_reports]=$_report_name

            # test if the reports is one than missing hour and exists, them must be removed
            if [[ " ${_missed_hour[@]} " =~ " ${_report_name} " ]]; then


                echo `rm "${_input_xml_path}AS_${_file_rm}.xml"`
                echo `rm "${_input_xml_path}AQ_${_file_rm}.xml"`
                echo `rm "${_input_xml_path}Q_${_file_rm}.xml"`

                echo " Removing the unxpected $_report_name report"
                echo " Removing report..."
                echo " "

                _arr_removed_files[_count_removed_reports]=$_report_name
                ((_count_removed_reports++))
            fi

            # Refresh reports found
            ((_count_found_reports++))
        fi

    # Today is the DLS ends: 25 hours
    elif [ $_DLS_start == 0 ] && [ $_DLS_end == 1 ];
    then
        echo "Testing if there are missing reports in DLS Ends day."
        if [ ! -f ${_xml_as_report_name} ] || [ ! -f $_xml_aq_report_name ] || [ ! -f $_xml_q_report_name ]; then
            echo " One or multiple reports for $_report_name not found"
            echo " Starting the generation ..."

            # Append file name to the DLS ends array
            _arr_missing_files[_count_missing_reports]=$_report_name
            ((_count_missing_reports++))

            # regerate the missing reports
            /bin/bash rerun.sh ./config.navomi 2 2 2 ./rerun_reports/ "$_report_name" "$_report_name"
             _arr_regerated_files[_count_regerated_reports]=$_report_name
            ((_count_regerated_reports++))

        else
            # if the report not is alredy in the array, append the report name to array
            if [[ " ${_arr_found_reports[@]} " =~ " ${_report_name} " ]]; then
                echo "The $_report_name already was considered."
            else
                echo "The $_report_name reports was found."
                _arr_found_reports[_count_found_reports]=$_report_name
                # Refresh reports found
                ((_count_found_reports++))
            fi
        fi
        echo "==========================================================================="

    fi

    # refresh the counter to while loop control
    let _report_generated_count+=1

done


# Some server already consider the DLS starts day with 23 hours,
# then the missing hour cannot be tested in normal way.
# Then this loop will test if exists generated reports to missing hour and remove them.
# Today is DLS starts: 23 hours
if [ $_DLS_start == 1 ] && [ $_DLS_end == 0 ];
then
    for _file in "${_missed_hour[@]}"
    do
        # Check if the intraday XML report file can be found
        _xml_as_report_name=`echo "${_input_xml_path}AS_${_file}.xml"`
        _xml_aq_report_name=`echo "${_input_xml_path}AQ_${_file}.xml"`
        _xml_q_report_name=`echo "${_input_xml_path}Q_${_file}.xml"`

        # Create and show report name to missing files
        if [ -e ${_xml_as_report_name} ] || [ -e $_xml_aq_report_name ] || [ -e $_xml_q_report_name ]; then
            echo " Removing the unxpected $_file report"

            if ! rm "$_xml_as_report_name"
            then
                echo "Problems to removing $_file"
            fi

            if ! rm "$_xml_aq_report_name"
            then
                echo "Problems to removing $_file"
            fi

            if ! rm "$_xml_q_report_name"
            then
                echo "Problems to removing $_file"
            fi

            _arr_removed_files[_count_removed_reports]=$_file
            ((_count_removed_reports++))
        fi
    done
fi


# Print debug message informing files counters and list don't found files
if [ $_debug_level -eq 2 ]; then
    echo ""
    echo "==========================================================================="
    echo "All Files"
    #echo " Expected: $_expected_reports" # $_tot_rep
    echo " Found          : $((_count_found_reports * 3))"
    echo " Missing        : $((_count_missing_reports * 3))"
    echo " Missing files  : ${_arr_missing_files[*]}"
    echo " Regerated      : $((_count_regerated_reports * 3))"
    echo " Regerated files: ${_arr_regerated_files[*]}"
    echo " Removed        : $((_count_removed_reports * 3))"
    echo " Removed files  : ${_arr_removed_files[*]}"
    echo "==========================================================================="
fi;

